# OnlineDeclareAnalyzer


## Requirements
- Java 6 or higher
- [LP Solver for Java](http://lpsolve.sourceforge.net/5.5/Java/README.html)

## Steps

1. Run ProM with UITopia 
2. Select and run Operational Support Service 2
3. Select and run Online Declare Analyzer 
4. Run OnlineDeclareAnalyzerClient.java and start monitoring
5. Run LogStreamer.java
6. Select log and model from ./examples .. and stream the log


## Demo

1. [Early detection of conflicts using ILP](https://www.youtube.com/watch?v=cZFVUaVjRpQ)
2. [Example from hospital logs](https://www.youtube.com/watch?v=-X9b77cTB-U)
