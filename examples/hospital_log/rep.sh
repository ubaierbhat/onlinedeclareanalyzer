sed -i.bak 's/aanname\ laboratoriumonderzoek/aanname_laboratoriumonderzoek/' hospital_log.xes
sed -i.bak 's/albumine/albumine/' hospital_log.xes
sed -i.bak 's/alkalische\ fosfatase\ \-kinetisch\-/alkalische_fosfatase__kinetisch_/' hospital_log.xes
sed -i.bak 's/ordertarief/ordertarief/' hospital_log.xes
sed -i.bak 's/vervolgconsult\ poliklinisch/vervolgconsult_poliklinisch/' hospital_log.xes
sed -i.bak 's/administratief\ tarief\ \ \ \ \ \ \ \-\ eerste\ pol/administratief_tarief___eerste_pol/' hospital_log.xes
sed -i.bak 's/bacteriologisch\ onderzoek\ met\ kweek\ \-nie/bacteriologisch_onderzoek_met_kweek__nie/' hospital_log.xes
sed -i.bak 's/cytologisch\ onderzoek\ \-\ ectocervix\ \-/cytologisch_onderzoek___ectocervix__/' hospital_log.xes
sed -i.bak 's/histologisch\ onderzoek\ \-\ biopten\ nno/histologisch_onderzoek___biopten_nno/' hospital_log.xes
sed -i.bak 's/telefonisch\ consult/telefonisch_consult/' hospital_log.xes
sed -i.bak 's/General\ Lab\ Clinical\ Chemistry/General_Lab_Clinical_Chemistry/' hospital_log.xes
sed -i.bak 's/Section\ /Section_/' hospital_log.xes
sed -i.bak 's/Medical\ Microbiology/Medical_Microbiology/' hospital_log.xes
sed -i.bak 's/Specialism\ code/Specialism_code/' hospital_log.xes
sed -i.bak 's/Producer/ code/Producer_code/' hospital_log.xes
sed -i.bak 's/Diagnosis\ code/Diagnosis_code/' hospital_log.xes

