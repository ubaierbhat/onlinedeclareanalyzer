package org.processmining.declarestreamer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XMxmlGZIPParser;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.*;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;
import org.processmining.plugins.declareminer.visualizing.Parameter;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import com.fluxicon.slickerbox.colors.SlickerColors;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;
import org.deckfour.xes.factory.XFactory;

import info.clearthought.layout.TableLayout;
import it.unibo.ai.rec.engine.FluentsConverter;
import it.unibo.ai.rec.visualization.FluentChartContainer;
import it.unibo.ai.rec.visualization.FluentChartFactory;

public class LogStreamer extends JFrame {


    protected static final int PORT = 4444;

    private int value;
    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss:S";
    protected FluentsConverter converter;
    private FluentChartFactory factory;
    private static final long serialVersionUID = 1561407447457027863L;
    private JTextField windSize;
    private JPanel contentPane;
    private AssignmentViewBroker broker = null;
    private String brokerFilePath;
    public static final String CASE_COMPLETE = "case_complete";
    private static final int MONITOR_TAB = 0;
    private static final int HEALTH_TAB = 1;
    private static final int DIAGNOSTICS_TAB = 2;

    private Color backgroundColor = SlickerColors.COLOR_BG_4;
    private Color chartBackgroundColor = new Color(232, 232, 232);
    private JRadioButton twentyfour;
    private JRadioButton sixty;
    private JRadioButton twelve;
    private JRadioButton six;
    private JRadioButton three;
    private JButton search;
    private String shipType = "REF MODEL: no model selected";
    private int selectedTab;
    private boolean showV;
    private boolean showA;
    private boolean searching;

    private JCheckBox check;
    private Map<String, Object> analysis;
    boolean primo = true;
    FluentChartContainer chartPanel;

    private HashMap<String, Double> weights;
    private JTextField searchField;
    private Action startAction;

    private JScrollPane outputContainer;
    private JScrollPane instancesContainer;

    private JPanel roundedPanelHealth;
    private JPanel roundedPanelDiag;
    private JPanel completeListPanel;
    private JPanel filterPanel;
    private JPanel searchPanel;

    JButton loadMap;
    JButton loadLog;
    JButton startReplay;

    private JList diagnosticsList;

    protected XLog log = null;
    protected JList instancesList;

    private OSXMLConverter osxmlConverter = new OSXMLConverter();

    @SuppressWarnings("serial")
    private void initActions() {
        startAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnabled(false);
            }

        };
        startAction.setEnabled(true);
    }

    private JScrollPane createSlickerScrollPane() {

        JScrollPane scrollpane = new JScrollPane();
        scrollpane.setOpaque(false);
        scrollpane.getViewport().setOpaque(false);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());
        scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(), new Color(0, 0, 0, 0),
                new Color(140, 140, 140), new Color(80, 80, 80));
        scrollpane.getVerticalScrollBar().setOpaque(false);

        SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(), new Color(0, 0, 0, 0),
                new Color(140, 140, 140), new Color(80, 80, 80));
        scrollpane.getHorizontalScrollBar().setOpaque(false);
        return scrollpane;
    }

    private LogStreamer(String confPath) throws JDOMException, IOException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        initActions();
        weights = new HashMap<String, Double>();
        instancesList = new JList();
        instancesContainer = createSlickerScrollPane();
        instancesContainer.setSize(200, 1200);
        instancesContainer.setPreferredSize(new Dimension(200, 1200));
        instancesContainer.setMaximumSize(new Dimension(200, 1200));
        instancesContainer.setMinimumSize(new Dimension(200, 1200));

        final SlickerFactory factory = SlickerFactory.instance();

        JPanel buttonPanel = factory.createRoundedPanel();

        loadMap = factory.createButton("LOAD MAP");
        loadMap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadDeclareModel();
            }
        });

        loadLog = factory.createButton("LOAD LOG");
        loadLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadEventsLog();
            }
        });

        startReplay = factory.createButton("START REPLAY");
        startReplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startStreaming();
            }
        });

        JPanel optionPanel = factory.createRoundedPanel();
        JPanel radioPanel = factory.createRoundedPanel();
        radioPanel.setBorder(new TitledBorder("Speed"));
        optionPanel.setLayout(new BorderLayout());
        check = factory.createCheckBox("Complete traces in the log", true);
        sixty = factory.createRadioButton("60 ev. per min.");
        twentyfour = factory.createRadioButton("24 ev. per min.");
        twelve = factory.createRadioButton("12 ev. per min.");
        six = factory.createRadioButton("6 ev. per min.");
        three = factory.createRadioButton("3 ev. per min.");
        ButtonGroup bg = new ButtonGroup();
        bg.add(sixty);
        bg.add(twentyfour);
        bg.add(twelve);
        bg.add(six);
        bg.add(three);
        twelve.setSelected(true);
        radioPanel.setLayout(new TableLayout(new double[][]{{300}, {20, 20, 20, 20, 20}}));
        radioPanel.add(sixty, "0,0");
        radioPanel.add(twentyfour, "0,1");
        radioPanel.add(twelve, "0,2");
        radioPanel.add(six, "0,3");
        radioPanel.add(three, "0,4");
        optionPanel.add(check, BorderLayout.EAST);
        optionPanel.add(radioPanel, BorderLayout.WEST);
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(optionPanel, BorderLayout.NORTH);
        buttonPanel.add(loadLog, BorderLayout.WEST);
        buttonPanel.add(loadMap, BorderLayout.CENTER);
        buttonPanel.add(startReplay, BorderLayout.EAST);
        contentPane = factory.createRoundedPanel();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(buttonPanel, BorderLayout.SOUTH);
        contentPane.add(instancesContainer, BorderLayout.CENTER);
        contentPane.setSize(200, 800);
        contentPane.setPreferredSize(new Dimension(200, 800));
        contentPane.setMaximumSize(new Dimension(200, 800));
        contentPane.setMinimumSize(new Dimension(200, 800));
        contentPane.setBackground(backgroundColor);
        setContentPane(contentPane);
        setSize(600, 700);
        setPreferredSize(new Dimension(600, 700));
        setMaximumSize(new Dimension(600, 700));
        setMinimumSize(new Dimension(600, 700));
        setName("Log Streamer");
        setResizable(false);
        setTitle("Log Streamer");

    }

    public String getStringFromDoc(org.w3c.dom.Document doc) {
        DOMImplementationLS domImplementation = (DOMImplementationLS) doc.getImplementation();
        LSSerializer lsSerializer = domImplementation.createLSSerializer();
        return lsSerializer.writeToString(doc);
    }

    public static void main(String[] args) {
        try {
            if (args.length > 0) {
                new LogStreamer(args[0]).setVisible(true);
            } else {
                new LogStreamer("conf.xml").setVisible(true);
            }
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void loadEventsLog() {
        JFileChooser jfc = new JFileChooser();
        jfc.showOpenDialog(contentPane);
        if (jfc.getSelectedFile() != null) {
            String inputLogFileName = jfc.getSelectedFile().getAbsolutePath();

            if (inputLogFileName.toLowerCase().contains("mxml.gz")) {
                XMxmlGZIPParser parser = new XMxmlGZIPParser();
                if (parser.canParse(new File(inputLogFileName))) {
                    try {
                        log = parser.parse(new File(inputLogFileName)).get(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (inputLogFileName.toLowerCase().contains("mxml")) {
                XMxmlParser parser = new XMxmlParser();
                if (parser.canParse(new File(inputLogFileName))) {
                    try {
                        log = parser.parse(new File(inputLogFileName)).get(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (inputLogFileName.toLowerCase().contains("xes.gz")) {
                XesXmlGZIPParser parser = new XesXmlGZIPParser();
                if (parser.canParse(new File(inputLogFileName))) {
                    try {
                        log = parser.parse(new File(inputLogFileName)).get(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (inputLogFileName.toLowerCase().contains("xes")) {
                XesXmlParser parser = new XesXmlParser();
                if (parser.canParse(new File(inputLogFileName))) {
                    try {
                        log = parser.parse(new File(inputLogFileName)).get(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        if (log == null) {
            return;
        }
    }

    private void loadDeclareModel() {
        JFileChooser jfc = new JFileChooser();
        jfc.showOpenDialog(contentPane);
        if (jfc.getSelectedFile() != null) {
            broker = XMLBrokerFactory.newAssignmentBroker(jfc.getSelectedFile().getAbsolutePath());
            brokerFilePath = jfc.getSelectedFile().getAbsolutePath();
            final Vector<String> list = new Vector<String>();
            AssignmentModel assmod = broker.readAssignment();
            double longest = 0.;
            for (ConstraintDefinition cd : assmod.getConstraintDefinitions()) {
                final String name = cd.getName();
                final String caption = cd.getCaption();
                final String condition = cd.getCondition().toString();
                final String constraint = name + "(" + caption.substring(caption.indexOf("[")) + ", </br>" + condition + ")";
                list.add(constraint);
                weights.put(constraint, 1.);
                if (constraint.length() > longest) {
                    longest = constraint.length();
                }
            }
            double[] size = new double[list.size() + 1];
            for (int i = 0; i < size.length; i++) {
                size[i] = 20;
            }
            JLabel idLabel;
            JComboBox combo = null;
            SlickerFactory sf = SlickerFactory.instance();
            JPanel panel = sf.createRoundedPanel();
            panel.setLayout(new TableLayout(new double[][]{{6.6 * longest, 50}, size}));
            int i = 0;
            for (String constr : list) {
                final String labelText = String.format("<html>%s<html>", constr);
                idLabel = sf.createLabel(labelText);
                combo = sf.createComboBox(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
                final int j = i;
                // panel.add(bef, BorderLayout.NORTH);
                combo.addItemListener(new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent arg0) {
                        weights.put(list.get(j), new Double(arg0.getItem().toString()));

                    }
                });
                panel.add(idLabel, "0," + i);
                panel.add(combo, "1," + i);
                i++;
            }

            windSize = new JTextField("5");
            panel.add(windSize, "0," + i);
            instancesContainer.setViewportView(panel);
        }
    }

    private void startStreaming() {
        if (log == null) {
            JOptionPane.showMessageDialog(contentPane, "LOG MISSING!");
            return;
        }
        if (broker == null) {
            JOptionPane.showMessageDialog(contentPane, "MAP MISSING!");
            return;
        }
        String host = "localhost";
        System.out.println(" \n startStreaming() \n");
        try {

            // Select frequency
            long times = 0;

            if (sixty.isSelected()) {
                times = 1000;
            } else if (twentyfour.isSelected()) {
                times = 2500;
            } else if (twelve.isSelected()) {
                times = 5000;
            } else if (six.isSelected()) {
                times = 10000;
            } else {
                times = 20000;
            }


            // Prepare model

            HashMap<Long, Vector<String>> events = new HashMap<Long, Vector<String>>();
            Socket socket = new Socket(host, PORT);
            socket.isConnected();
            PrintWriter writeOnTheSocket = new PrintWriter(socket.getOutputStream(), true);
            SAXBuilder sb = new SAXBuilder();
            DOMOutputter outputter = new DOMOutputter();
            org.jdom.Document xesDocument = sb.build(new File(brokerFilePath));
            org.w3c.dom.Document w3cdoc = outputter.output(xesDocument);

            // Transmit model
            writeOnTheSocket.println(getStringFromDoc(w3cdoc));
            writeOnTheSocket.flush();

            // Add weights
            for (Double d : weights.values()) {
                writeOnTheSocket.println(d);
                writeOnTheSocket.flush();
            }
            writeOnTheSocket.println("END_WEIGHTS");
            writeOnTheSocket.flush();
            writeOnTheSocket.println(windSize.getText());
            writeOnTheSocket.flush();
            Vector<Long> timestamps = new Vector<Long>();


            // Start processing log
            for (XTrace t : log) {
                // if we need to add a random time before the trace is executed, this is the moment
                long traceTimeIncrement = 0;

                // some general trace statistics
                int traceLength = t.size();
                int eventIndex = 0;


                for (XEvent e : t) {
                    XTraceImpl t1 = new XTraceImpl(t.getAttributes());
                    XAttributeTimestampImpl timestamp = (XAttributeTimestampImpl) e.getAttributes().get("time:timestamp");
                    ((XAttributeTimestampImpl) e.getAttributes().get("time:timestamp")).setValueMillis(timestamp.getValueMillis() + traceTimeIncrement + eventIndex);

                    // event tagging
                    boolean tagBeginningEnd = true;
                    if (tagBeginningEnd) {
                        if (eventIndex == 0) {
                            e.getAttributes().put("stream:lifecycle:trace-transition", new XAttributeLiteralImpl("stream:lifecycle:trace-transition", "start"));
                        } else if (eventIndex == traceLength - 1) {
                            e.getAttributes().put("stream:lifecycle:trace-transition", new XAttributeLiteralImpl("stream:lifecycle:trace-transition", "complete"));
                        }
                    }

                    // add trace attributes to event
                    String eventName = XConceptExtension.instance().extractName(e);
                    e.getAttributes().putAll(t.getAttributes());
                    XConceptExtension.instance().assignName(e,eventName);
                    // Add event to trace
                    t1.add(e);

                    System.out.println("Writing to socket Event : " + e.getAttributes().get("concept:name"));
                    String packet = osxmlConverter.toXML(t1).replace('\n', ' ');
                    // Transmit trace
                    writeOnTheSocket.println(packet);
                    writeOnTheSocket.flush();
                    eventIndex++;
                    Thread.sleep(times);
                }

                XTraceImpl lastTrace = new XTraceImpl(t.getAttributes());
                // Add last trace.
                XFactory xFactory = XFactoryRegistry.instance().currentDefault();
                XEvent le = xFactory.createEvent();
                //le.setAttributes(t.get(t.size() - 1).getAttributes());
                XConceptExtension concept = XConceptExtension.instance();
                concept.assignName(le, "complete");
                XLifecycleExtension lc = XLifecycleExtension.instance();
                lc.assignTransition(le, "complete");
                XTimeExtension timeExtension = XTimeExtension.instance();
                timeExtension.assignTimestamp(le, timeExtension.extractTimestamp(t.get(t.size() - 1)).getTime() + 1);
                lastTrace.add(le);
                System.out.println("Writing to socket Event : " + le.getAttributes().get("concept:name"));
                String packet = osxmlConverter.toXML(lastTrace).replace('\n', ' ');
                // Transmit trace
                writeOnTheSocket.println(packet);
                writeOnTheSocket.flush();

            }

            socket.shutdownOutput();


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

//            startReplay.setEnabled(true);
//            loadLog.setEnabled(true);
//            loadMap.setEnabled(true);
        }
    }

}
