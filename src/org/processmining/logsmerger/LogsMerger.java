package org.processmining.logsmerger;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.stream.utils.BasicPluginConfiguration;

/**
 * Class that contains a log merger plugin
 * 
 * @author Andrea Burattin
 */
public class LogsMerger {
	
	@Plugin(
		name = "Logs Merger",
		parameterLabels = { "The first log", "The second log" },
		returnLabels = { "The final merged log" },
		returnTypes = { XLog.class },
		userAccessible = true
	)
	@UITopiaVariant(
		author = BasicPluginConfiguration.AUTHOR,
		email = BasicPluginConfiguration.EMAIL,
		affiliation = BasicPluginConfiguration.AFFILIATION
	)
	public static XLog merge(UIPluginContext context, XLog firstLog, XLog secondLog) {
		
		XLogInfo firstInfo = XLogInfoFactory.createLogInfo(firstLog);
		XLogInfo secondInfo = XLogInfoFactory.createLogInfo(secondLog);
		
		long toAddLog1 = 0;
		long toAddLog2 = 0;
		
		MergerConfiguration configAppend = new MergerConfiguration(firstInfo, secondInfo);
		InteractionResult r = context.showConfiguration("Log merger setup", configAppend);
		
		if (r.equals(InteractionResult.CONTINUE)) {
			
			// prepare the name of the new log
			String newLogName = configAppend.getNewLogName();
			context.getFutureResult(0).setLabel(newLogName);
			
			// check reuse of the same case id
			Boolean reuseSameCaseId = configAppend.getReuseTheSameCaseId();
			
			// get the time shift
			toAddLog1 = configAppend.getTimeOffsetLog1();
			toAddLog2 = configAppend.getTimeOffsetLog2();
			
			// build the new log, starting from the union of the attribute sets
			XAttributeMap logAttributes = (XAttributeMap) firstLog.getAttributes().clone();
			logAttributes.putAll((XAttributeMap) secondLog.getAttributes().clone());
			XAttributeLiteral logName = ((XAttributeLiteral)logAttributes.get("concept:name"));
			logName.setValue(newLogName);
			logAttributes.put("concept:name", logName);
			XLog finalLog = new XLogImpl(logAttributes);
			
			// in order to have a unique case id, we have to recompute it from
			// scratch
			int caseId = 0;
			
			// add the elements of the first log
			for (XTrace t : firstLog) {
				XTrace newT = new XTraceImpl((XAttributeMap) t.getAttributes().clone());
				for (XEvent e : t) {
					e = (XEvent) e.clone();
					XAttributeMap m = e.getAttributes();
					XAttributeTimestamp time = (XAttributeTimestamp) m.get("time:timestamp").clone();
					time.setValueMillis(time.getValueMillis() + toAddLog1);
					m.put("time:timestamp", time);
					e.setAttributes(m);
					newT.add(e);
				}
				XAttributeMap m = newT.getAttributes();
				XAttributeLiteral c = (XAttributeLiteral) m.get("concept:name");
				c.setValue("case_id_" + (caseId++));
				m.put("concept:name", c);
				finalLog.add(newT);
			}
			
			// now we have to check if the same case id is supposed to be reused
			if (reuseSameCaseId) {
				caseId = 0;
			}
			
			// add the elements of the second log
			for (XTrace t : secondLog) {
				
				// get the current process instance (it depends if we have to
				// recycle the case id or not)
				XTrace newT;
				if (reuseSameCaseId) {
					newT = finalLog.get(caseId);
				} else {
					newT = new XTraceImpl((XAttributeMap) t.getAttributes().clone());
					XAttributeMap m = newT.getAttributes();
					XAttributeLiteral c = (XAttributeLiteral) m.get("concept:name");
					c.setValue("case_id_" + (caseId));
					m.put("concept:name", c);
				}
				
				// add all the new events to the process instance
				for (XEvent e : t) {
					e = (XEvent) e.clone();
					XAttributeMap m = e.getAttributes();
					XAttributeTimestamp time = (XAttributeTimestamp) m.get("time:timestamp").clone();
					time.setValueMillis(time.getValueMillis() + toAddLog2);
					m.put("time:timestamp", time);
					e.setAttributes(m);
					newT.add(e);
				}
				
				if (!reuseSameCaseId) {
					finalLog.add(newT);
				}
				
				caseId++;
			}
			
			return finalLog;
			
		} else {
			
			return null;
			
		}
	}

}
