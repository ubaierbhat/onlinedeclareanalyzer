package org.processmining.logsmerger;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.deckfour.xes.info.XLogInfo;
import org.processmining.stream.utils.GUIUtils;
import org.processmining.streamer.gui.LogTimeSpanVisualizer;

import com.fluxicon.slickerbox.components.RoundedPanel;
import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * Configuration panel for the log merger
 * 
 * @author Andrea Burattin
 */
public class MergerConfiguration extends JPanel {

	private static final long serialVersionUID = -7208932241207488773L;
	private XLogInfo log1;
	private XLogInfo log2;
	private String newDefaultName;
	private JTextField newLogName;
	private JCheckBox reuseSameCaseId;
	private LogTimeSpanVisualizer logTimeSpan;
	
	/**
	 * Basic constructor
	 * 
	 * @param log1 the first log
	 * @param log2 the second log
	 */
	public MergerConfiguration(XLogInfo log1, XLogInfo log2) {
		this.log1 = log1;
		this.log2 = log2;
		
		newDefaultName = "(" + log1.getLog().getAttributes().get("concept:name") + ", " + log2.getLog().getAttributes().get("concept:name") + ")";
		
		initComponents();
	}
	
	/**
	 * This method returns the offset to be summed to all the events of the
	 * first log. The value can be positive or negative
	 * 
	 * @return the time offset for the first log
	 */
	public long getTimeOffsetLog1() {
		return logTimeSpan.getTimeOffsetLog1();
	}
	
	/**
	 * This method returns the offset to be summed to all the events of the
	 * second log. The value can be positive or negative
	 * 
	 * @return the time offset for the second log
	 */
	public long getTimeOffsetLog2() {
		return logTimeSpan.getTimeOffsetLog2();
	}
	
	/**
	 * This method returns the name of the new process. If the user specified a
	 * non empty name, it is returned, otherwise the default one is returned
	 * The default name is "<tt>(first-process-name, second-process-name)</tt>". 
	 * 
	 * @return the final process name
	 */
	public String getNewLogName() {
		return (newLogName.getText().trim().isEmpty())? newDefaultName : newLogName.getText().trim();
	}
	
	/**
	 * This method returns a boolean value that indicates is it is necessary to
	 * reuse the same case id on the two processes
	 * 
	 * @return true if the same case id will be reused, false otherwise
	 */
	public Boolean getReuseTheSameCaseId() {
		return reuseSameCaseId.isSelected();
	}
	
	/*
	 * Graphical components initializer
	 */
	private void initComponents() {
		
		logTimeSpan = new LogTimeSpanVisualizer(log1, log2);
		newLogName = GUIUtils.prepareTextField(newDefaultName);
		reuseSameCaseId = SlickerFactory.instance().createCheckBox("Reuse the same case id in both processes", true);
		
		JPanel logNameContainer = new RoundedPanel(10);
		logNameContainer.setBackground(Color.DARK_GRAY);
		logNameContainer.setLayout(new BorderLayout());
		logNameContainer.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		logNameContainer.add(newLogName, BorderLayout.CENTER);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 10, 0);
		add(GUIUtils.prepareLabel("New log name:"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = 0;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 10, 0);
		add(logNameContainer, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 1;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 10, 0);
		add(reuseSameCaseId, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1; c.weighty = 1;
		add(logTimeSpan, c);
	}

}
