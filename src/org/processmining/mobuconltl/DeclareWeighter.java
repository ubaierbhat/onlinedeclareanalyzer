/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.mobuconltl;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.Progress;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.plugins.declareminer.visualizing.DeclareMap;

@Plugin(name = "Declare Weighter", parameterLabels = { "Declare Map" }, returnLabels = { "Weighted Declare Model" }, returnTypes = { WeightedDeclareMap.class }, userAccessible = true)
public class DeclareWeighter {
	Progress prog;

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F.M. Maggi", email = "F.M.Maggi@tue.nl")
	@PluginVariant(requiredParameterLabels = { 0 }, variantLabel = "Declare Weighter")
	public WeightedDeclareMap analyse(UIPluginContext context, DeclareMap model) {
		WeighterGUI ui = new WeighterGUI(model);
		InteractionResult r = context.showWizard("Configure the Declare Weighter", true, true, ui.createGui());
		if (r == InteractionResult.FINISHED) {
			prog = context.getProgress();
			prog.setMinimum(0);
			prog.setMaximum(112);
			prog.setIndeterminate(false);
			prog.setValue(3);
			double[] weights = new double[ui.getSettings().length];
			for (int i = 0; i < weights.length; i++) {
				weights[i] = (new Double((String) ui.getSettings()[i])).doubleValue();
			}
			WeightedDeclareMap outcome = new WeightedDeclareMap();
			outcome.setModel(model);
			// outcome.setComponents(model.getComponents());
			// outcome.setCoverage(model.getCoverage());
			//  outcome.setXmlDoc(model.getXmlDoc());
			outcome.setWeights(weights);
			prog.setValue(112);
			return outcome;
		}
		context.getFutureResult(0).cancel(true);
		return null;
	}

}
