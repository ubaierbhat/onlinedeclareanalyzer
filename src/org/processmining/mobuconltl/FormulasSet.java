package org.processmining.mobuconltl;

import java.util.Vector;

import org.processmining.plugins.declareminer.visualizing.AssignmentModel;


public class FormulasSet {

	private AssignmentModel model;
	private Vector weights;
	private String[] constraints;
	private String[] forms;
	private String formula;

	public AssignmentModel getModel() {
		return model;
	}

	public void setModel(AssignmentModel model) {
		this.model = model;
	}

	public Vector getWeights() {
		return weights;
	}

	public void setWeights(Vector weights) {
		this.weights = weights;
	}

	public String[] getConstraints() {
		return constraints;
	}

	public void setConstraints(String[] constraints) {
		this.constraints = constraints;
	}

	public String[] getForms() {
		return forms;
	}

	public void setForms(String[] forms) {
		this.forms = forms;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

}
