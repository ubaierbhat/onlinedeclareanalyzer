/***********************************************************
 * This software is part of the ProM package * http://www.processmining.org/ * *
 * Copyright (c) 2003-2006 TU/e Eindhoven * and is licensed under the * Common
 * Public License, Version 1.0 * by Eindhoven University of Technology *
 * Department of Information Systems * http://is.tm.tue.nl * *
 **********************************************************/

package org.processmining.mobuconltl;


import java.io.PrintWriter;
import java.net.Socket;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.Progress;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;

@Plugin(name = "Log Replayer", parameterLabels = { "Log",  "Declare Map"}, returnLabels = { }, returnTypes = { }, userAccessible = true)
public class LogReplayer {
	Progress prog;

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F.M. Maggi", email = "F.M.Maggi@tue.nl")
	@PluginVariant(requiredParameterLabels = { 0, 1 }, variantLabel = "Log Replayer")
	public void analyse(UIPluginContext context, XLog log, WeightedDeclareMap model) {
		String host = "localhost";
		try{
			Socket socket = new Socket (host,4444);
			socket.isConnected();
			PrintWriter writeOnTheSocket = new PrintWriter(socket.getOutputStream(),true);
			writeOnTheSocket.println(model.getModel().getBroker().getXML());
			writeOnTheSocket.flush();
	
			prog = context.getProgress();
			prog.setMaximum(112);
			prog.setIndeterminate(false);
			prog.setValue(1);

			double[] weights = model.getWeights();
			for(int i = 0; i<weights.length; i++){
				writeOnTheSocket.println(weights[i]);
				writeOnTheSocket.flush();
			}
			writeOnTheSocket.println("END_WEIGHTS");
			writeOnTheSocket.flush();
			writeOnTheSocket.println(5);
			writeOnTheSocket.flush();
		
			for(XTrace trace : log){
				for(XEvent event: trace){
					prog.inc();
			
					String line = "<?xml version=\"1.0\"?><Entry><ProcessID>0</ProcessID><ProcessInstanceID>"+XConceptExtension.instance().extractName(trace)+"</ProcessInstanceID><ModelID>Municipalities</ModelID><WorkflowModelElement>"+XConceptExtension.instance().extractName(event)+"</WorkflowModelElement><Timestamp>"+XTimeExtension.instance().extractTimestamp(event).getTime()+"</Timestamp><EventType>complete</EventType></Entry>";
			
					System.out.println(line);
					writeOnTheSocket.println(line);
					writeOnTheSocket.flush();
					
										Thread.sleep(1000);
				
				}
			}
		}catch (Exception e){e.printStackTrace();} }
}

