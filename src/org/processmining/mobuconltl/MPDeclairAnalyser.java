package org.processmining.mobuconltl;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.processmining.operationalsupport.session.Session;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;

public class MPDeclairAnalyser {

    final static int LISTENING_PORT = 1202;

    public static void main(String args[]) {

        ServerSocket echoServer = null;
        String line;
        DataInputStream is;
        PrintStream os;
        Socket clientSocket = null;


        try {
            echoServer = new ServerSocket(LISTENING_PORT);
            log("Server Started");
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            clientSocket = echoServer.accept();
            is = new DataInputStream(clientSocket.getInputStream());
            os = new PrintStream(clientSocket.getOutputStream());

            Hashtable replays = new Hashtable();
            //session.put("replays", replays);

            while (true) {
                //line = is.readLine();
                //System.out.println(line);


                DocumentBuilderFactory dbFactory
                        = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

                    //String line = null;
                    StringBuffer buffer = new StringBuffer();
                    while ((line = is.readLine()) != null) {  // 'in' is a BufferedReader on socket input stream
                        buffer.append(line);
                        if (line.indexOf("</model>") > -1) { // message close tag found
                            processXml(buffer); // send buffer for processing
                            // [maybe in.close()]
                            break;
                        }
                    }
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                }



            }
        } catch (
                IOException e
                )

        {
            System.out.println(e);
        }

    }

    private static void processXml(StringBuffer buffer) {
        log("processXml");
        try {

            // Create XES document

            SAXBuilder sb = new SAXBuilder();
            DOMOutputter outputter = new DOMOutputter();
            String input = buffer.toString();
            log("Input = " + input);
            Document xesDocument = sb.build(new StringReader(input));
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            String xmlString = xmlOutput.outputString(xesDocument);

            // View Pretty Print
            System.out.println(xmlString);

            // Code from MoBuConLTL write model to document

            File modelFile = File.createTempFile("model", ".xml");
            modelFile.deleteOnExit();
            xmlOutput.output(xesDocument, new FileWriter(modelFile));

            AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(modelFile.getAbsolutePath());

            AssignmentModel amodel = broker.readAssignment();
            FormulasSet fs = new FormulasSet();
            fs.setModel(amodel);
            String[] constraints = new String[amodel.constraintDefinitionsCount()];
            String[] forms = new String[amodel.constraintDefinitionsCount()];





        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void log(String str) {
        System.out.println(str);
    }
}