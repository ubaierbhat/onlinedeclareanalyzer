package org.processmining.mobuconltl;

import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.XMLOutputter;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.ltl2automaton.plugins.automaton.State;
import org.processmining.ltl2automaton.plugins.automaton.Transition;
import org.processmining.ltl2automaton.plugins.formula.DefaultParser;
import org.processmining.ltl2automaton.plugins.formula.Formula;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeLeaf;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeNode;
import org.processmining.ltl2automaton.plugins.formula.conjunction.DefaultTreeFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.GroupedTreeConjunction;
import org.processmining.ltl2automaton.plugins.formula.conjunction.TreeFactory;
import org.processmining.operationalsupport.provider.AbstractProvider;
import org.processmining.operationalsupport.provider.Provider;
import org.processmining.operationalsupport.server.OSService;
import org.processmining.operationalsupport.session.Session;
import org.processmining.plugins.declareminer.ExecutableAutomaton;
import org.processmining.plugins.declareminer.PossibleNodes;
import org.processmining.plugins.declareminer.enumtypes.DeclareTemplate;
import org.processmining.plugins.declareminer.templates.LTLFormula;
import org.processmining.plugins.declareminer.visualizing.ActivityDefinition;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;
import org.processmining.plugins.declareminer.visualizing.Parameter;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;

/**
 * 
 * @author Fabrizio Maria Maggi
 * 
 */

@Plugin(name = "Mobucon LTL", parameterLabels = { "Operational Support Service", "Declare Model",
"Weighted Declare Model" }, returnLabels = { "Mobucon LTL" }, returnTypes = { MoBuConLTL.class }, userAccessible = true)
public class MoBuConLTL extends AbstractProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3042748916288208677L;
	private static DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:S");

	//private File integrate = new File("C:/Users/Fabrizio/Desktop/integrate.rdf");
	//PrintWriter pw = new PrintWriter(new FileWriter(integrate));
	//private WeightedDeclareModel wmodel;
	//private String[] constraints;
	////private String[] forms;
	//private String formula;
	//private final Hashtable formulaTable;

	//private final Hashtable hash = new Hashtable();

	//private Xes2RecTraceTranslator traceTranslator;
	//private RecEngine recProxy;

	/*
	 * IMPLEMENTARE METODI ACCEPT SIMPLE E GET NAME
	 * 
	 * public <R, L> R simple(final Session session, final XLog emptylog, final
	 * String langauge, final L query, final boolean false true se hai fatto)
	 */

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F.M. Maggi", email = "F.M.Maggi@tue.nl", uiLabel = "Mobucon LTL", pack = "MobuconLTL")
	@PluginVariant(variantLabel = "Mobucon LTL", requiredParameterLabels = { 0 })
	public static Provider registerServiceProviderAUI(final UIPluginContext context, OSService service) {
		return registerServiceProviderA(context, service);
		//In the final version, it will use a translator plug-in Climb->Event Calculus, and then simply call the other variant!

	}

	//	@PluginVariant(variantLabel = "Default settings", requiredParameterLabels = { 0,1 })
	public static Provider registerServiceProviderA(final PluginContext context, OSService service) {

		try {
			MoBuConLTL provider = new MoBuConLTL(service);
			context.getFutureResult(0).setLabel(context.getFutureResult(0).getLabel() + " on port " + service);
			return provider;
		} catch (Exception e) {
			context.log(e);
		}
		return null;
	}

	public MoBuConLTL(OSService owner) throws Exception {
		super(owner);
	}

	protected String convert(XTrace trace, int pos) {
		XExtendedEvent ev = XExtendedEvent.wrap(trace.get(pos));
		return "" + ev.getTimestamp().getTime();
	}

	public String getName() {
		return "Mobucon LTL";
	}

	public boolean accept(final Session session, final List<String> modelLanguages, final List<String> queryLanguages,
			Object model) {
		if (modelLanguages.equals(DeclareLanguage.MIME_TYPE)) {
			return false;
		}
		if (!matchesLanguage(queryLanguages, DeclareMonitorQuery.MIME_TYPE)) {
			return false;
		}
		Hashtable replays = new Hashtable();
		session.put("replays", replays);
		try {
			SAXBuilder sb = new SAXBuilder();
			DOMOutputter outputter = new DOMOutputter();
			Document xesDocument = sb.build(new StringReader((String)model));
			XMLOutputter xmlOutput = new XMLOutputter();

			// display nice nice
			//	xmlOutput.setFormat(Format.getPrettyFormat());
			File modelFile = File.createTempFile("model", ".xml");
			modelFile.deleteOnExit();
			xmlOutput.output(xesDocument, new FileWriter(modelFile));


			AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(modelFile.getAbsolutePath());


			AssignmentModel amodel = broker.readAssignment();
			FormulasSet fs = new FormulasSet();
			fs.setModel(amodel);
			String[] constraints = new String[amodel.constraintDefinitionsCount()];
			String[] forms = new String[amodel.constraintDefinitionsCount()];
			int i = 0;
			String formula = "(";
			for (ConstraintDefinition cd : amodel.getConstraintDefinitions()) {
				constraints[i] = cd.getName();

				int count = 1;
				constraints[i] = constraints[i] + "(";
				for (Parameter p : cd.getParameters()) {
					if (count < cd.parameterCount()) {
						int countB = 1;
						constraints[i] = constraints[i] + "[";
						if(cd.getBranches(p).iterator().hasNext()){
							for (ActivityDefinition b : cd.getBranches(p)) {
								if (countB < cd.branchesCount(p)) {
									constraints[i] = constraints[i] + b.getName() + ",";
								} else {
									constraints[i] = constraints[i] + b.getName() + "],";
								}
								countB++;
							}
						}else{
							if (countB < cd.branchesCount(p)) {
								constraints[i] = constraints[i] + "empty"+ ",";
							} else {
								constraints[i] = constraints[i] + "empty" + "],";
							}
							countB++;
						}
					} else {
						int countB = 1;
						constraints[i] = constraints[i] + "[";
						if(cd.getBranches(p).iterator().hasNext()){
							for (ActivityDefinition b : cd.getBranches(p)) {
								if (countB < cd.branchesCount(p)) {
									constraints[i] = constraints[i] + b.getName() + ",";
								} else {
									constraints[i] = constraints[i] + b.getName() + "])";
								}
								countB++;
							}
						}else{
							if (countB < cd.branchesCount(p)) {
								constraints[i] = constraints[i] + "empty" + ",";
							} else {
								constraints[i] = constraints[i] + "empty" + "])";
							}
							countB++;	
						}
					}
					count++;
				}
				// DeclareModelGenerator dmg = new DeclareModelGenerator();
				String currentF = LTLFormula.getFormulaByTemplate(getTemplate(cd));

				for (Parameter p : cd.getParameters()) {
					int countB = 1;
					String actualParameter = "(\"";
					for (ActivityDefinition b : cd.getBranches(p)) {
						if (countB < cd.branchesCount(p)) {
							actualParameter = actualParameter + b.getName() + "\"||\"";
						} else {
							actualParameter = actualParameter + b.getName() + "\")";
						}
						countB++;
					}
					currentF = currentF.replace("\"" + p.getName() + "\"", actualParameter.replaceAll(" ", "_"));
				}
				currentF = currentF.replace("/\\ event==COMPLETE", "");
				currentF = currentF.replace("/\\ event==complete", "");
				currentF = currentF.replace("activity==", "");
				//currentF = currentF.replace("_O", "X");
				//currentF = currentF.replace("U_", "U");

				forms[i] = currentF;
				if (i != (forms.length - 1)) {
					formula = formula + currentF + ")&&(";
				} else {
					formula = formula + currentF + ")";
				}
				i++;
			}
			fs.setConstraints(constraints);
			fs.setForms(forms);
			fs.setFormula(formula);
			session.put("formulasSet", fs);
			Automaton[] pAut = new Automaton[forms.length];
			ExecutableAutomaton[] pExecAut = new ExecutableAutomaton[forms.length];
			for (int j = 0; j < forms.length; j++) {
				List<Formula> formulaeParsed = new ArrayList<Formula>();
				formulaeParsed.add(new DefaultParser(forms[j]).parse());
				TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
				ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
						.getFactory(treeFactory);
				GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
				pAut[j] = conjunction.getAutomaton().op.reduce();
				pExecAut[j] = new ExecutableAutomaton(pAut[j]);
			}
			List<Formula> formulaeParsed = new ArrayList<Formula>();
			formulaeParsed.add(new DefaultParser(fs.getFormula()).parse());
			TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
			ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
					.getFactory(treeFactory);
			GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
			Automaton aut = conjunction.getAutomaton().op.reduce();
			ExecutableAutomaton execAut = new ExecutableAutomaton(aut);
			execAut.ini();
			for (int j = 0; j < pAut.length; j++) {
				pExecAut[j].ini();
			}
			String[][] matrix = new String[constraints.length][1];
			StateObject currentState = null;
			currentState = new StateObject();
			currentState.setpAut(pAut);
			currentState.setpExecAut(pExecAut);
			currentState.setAut(aut);
			currentState.setExecAut(execAut);
			currentState.setMatrix(matrix);
			currentState.setEventPos(-1);
			currentState.setHealth(1);
			currentState.setHealthWindow(new Vector());
			session.put("stateObject", currentState);
			
			modelFile.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	public <R, L> R simple(final Session session, final XLog availableItems, final String langauge, final L query,
			final boolean done) throws Exception {

		if ((query == null) || !(query instanceof String)) {
			return null;
		}
		if (!langauge.equals(DeclareMonitorQuery.MIME_TYPE)) {
			return null;
		}
		boolean coloured = true;
		Hashtable output = null;
		XTrace lastTrace;
		XTrace trace = session.getCompleteTrace();
		lastTrace = session.getLastTrace();
		if (lastTrace.size() == 0) {
			lastTrace = session.getCompleteTrace();
		}
		//Map<String, Object> a = new HashMap();
		Hashtable replays = (Hashtable) session.get("replays");
		String systemID = (String) query;
		XTrace replay = null;
		if (replays.containsKey(systemID)) {
			replay = (XTrace) replays.get(systemID);
		} else {
			replay = new XTraceImpl(new XAttributeMapImpl());
		}
		FormulasSet fs = (FormulasSet) session.get("formulasSet");
		String[] constraints = fs.getConstraints();
		String positive = "";
		String negative = "";
		StateObject currentState = (StateObject) session.get("stateObject");
		ExecutableAutomaton[] pExecAut = null;
		ExecutableAutomaton execAut = null;
		Automaton[] pAut = null;
		Automaton aut = null;
		String[][] matrix = null;
		Vector weights = (Vector) session.getObject("weights");
		double health = -1;
		Vector healthWindow = null;
		double eventAffection = 0;
		int startingIndex = session.getCompleteTrace().size() - 5;
		if (startingIndex < 0) {
			startingIndex = 0;
		}
		int evPos;
		healthWindow = currentState.getHealthWindow();
		if (healthWindow.size() >= session.getInt("timeWindow")) {
			healthWindow.remove(0);
		}
		pExecAut = currentState.getpExecAut();
		execAut = currentState.getExecAut();
		pAut = currentState.getpAut();
		aut = currentState.getAut();
		evPos = currentState.getEventPos() + 1;
		currentState.setEventPos(evPos);
		matrix = new String[constraints.length][evPos + 1];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < (matrix[0].length - 1); j++) {
				matrix[i][j] = currentState.getMatrix()[i][j];
			}
		}
		Iterator events = lastTrace.iterator();
		while (events.hasNext()) {
			XEvent ev = null;
			ev = (XEvent) events.next();
			XEvent completeEvent = new XEventImpl();
			XExtendedEvent eva = XExtendedEvent.wrap(ev);
			XTimeExtension.instance().assignTimestamp(completeEvent, eva.getTimestamp());
			XConceptExtension.instance().assignName(completeEvent, eva.getName());
			boolean violated = true;
			String label = "";
			//	String globdiag = "";
			//String result = "[";
			label = ((XAttributeLiteral) ev.getAttributes().get("concept:name")).getValue();
			PossibleNodes current = null;
			current = execAut.currentState();
			if (!done) {
				if(current!=null&& !(current.get(0)==null)){
					for (Transition out : current.output()) {
						if (out.parses(label)) {
							violated = false;
							break;
						}
					}
				}
				if (violated) {
					if(current!=null&& !(current.get(0)==null)){
						for (Transition out : current.output()) {
							if ((out.isPositive())) {
								if (positive.equals("")) {
									//globdiag = out.getPositiveLabel();
									positive = out.getPositiveLabel();
								} else {
									//globdiag = globdiag + " or " + out.getPositiveLabel();
									positive = positive + "," + out.getPositiveLabel();
								}
							}
							if ((out.isNegative())) {
								boolean first = true;
								for (String nl : out.getNegativeLabels()) {
									if (negative.equals("")) {
										//globdiag = "(!" + nl;
										negative = nl;
									} else {
										if (first) {
											//	globdiag = globdiag + " or (!" + nl;
											negative = nl;
										} else {
											//	globdiag = globdiag + " and !" + nl;
											negative = negative + "," + nl;
										}
									}
									first = false;
								}
								//globdiag = globdiag + ")";
							}
						}
					}
					//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
				} else {
					execAut.next(label);
					replay.add(completeEvent);
					replays.put(systemID, replay);
				}
				current = execAut.currentState();
				if (!violated) {
					for (int i = 0; i < pAut.length; i++) {
						boolean violatedIth = true;
						current = pExecAut[i].currentState();
						if(current!=null&& !(current.get(0)==null)){
							for (Transition out : current.output()) {
								if (out.parses(label)) {
									violatedIth = false;
									break;
								}
							}
						}
						if (violatedIth) {
							eventAffection = eventAffection + new Double((String) weights.get(i)).doubleValue();
							/*
							 * if (!pinpointViol) { singlediag = ""; }
							 * singlediag = singlediag + constraints[i] + ", ";
							 * pinpointViol = true;
							 */
						} else {
							pExecAut[i].next(label);
						}
						current = pExecAut[i].currentState();
						if (!violatedIth) {
							if (current.isAccepting()) {
								matrix[i][evPos] = "poss.sat";

								for (State state : current) {
									if (state.isAccepting()) {
										for (Transition t : state.getOutput()) {
											if (t.isAll() && t.getTarget().equals(state)) {
												matrix[i][evPos] = "sat";
											}
										}
									}
								}

							} else {
								matrix[i][evPos] = "poss.viol";
							}
						} else {
							matrix[i][evPos] = "viol";
							//pinpointViol = true;
						}

					}
				} else {
					for (int i = 0; i < matrix.length; i++) {
						if(evPos>0){
							matrix[i][evPos] = matrix[i][evPos - 1];
						}else{
							matrix[i][evPos] = "poss.sat";
						}
						//						matrix[i][evPos] = "skipped";
					}
				}
				new Vector();
				String[] forms = fs.getForms();
				Vector conflictingSets = new Vector();
				if (coloured) {
					replay = (XTrace) replays.get(systemID);
					if (violated) {
						int g = 1;
						boolean conflViolated = false;
						while ((g <= forms.length) && conflictingSets.isEmpty()) {
							String[][] conflCandidates = DispositionsGenerator.generateDisp(forms, g);
							int i = 0;
							while (i < conflCandidates.length) {
								conflViolated = false;
								String formula = "(";
								for (int k = 0; k < conflCandidates[0].length; k++) {
									if (k != (conflCandidates[0].length - 1)) {
										formula = formula + conflCandidates[i][k] + ")&&(";
									} else {
										formula = formula + conflCandidates[i][k] + ")";
										;
									}
								}
								List<Formula> formulaeParsed = new ArrayList<Formula>();
								formulaeParsed.add(new DefaultParser(formula).parse());
								TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory
										.getInstance();
								ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
										.getFactory(treeFactory);
								GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
								aut = conjunction.getAutomaton().op.reduce();
								execAut = new ExecutableAutomaton(aut);
								execAut.ini();
								boolean used = false;
								if(replay==null){
									replay = new XTraceImpl(new XAttributeMapImpl());
								}
								Iterator moevents = replay.iterator();
								while (moevents.hasNext() || !used) {
									XEvent eve = null;
									if (!moevents.hasNext()) {
										used = true;
										eve = completeEvent;
									} else {
										eve = (XEvent) moevents.next();
									}
									label = "";
									//				globdiag = "";
									conflViolated = true;
									label = ((XAttributeLiteral) eve.getAttributes().get("concept:name")).getValue();
									current = null;
									current = execAut.currentState();
									if(current!= null&&!(current.get(0)==null)){
										for (Transition out : current.output()) {
											if (out.parses(label)) {
												conflViolated = false;
												break;
											}
										}
									}
									//			System.out.println(str);
									if (conflViolated) {
										//for (Transition out : current.output()) {
										//	if ((out.isPositive())) {
										//			if (positive.equals("")) {
										//	globdiag = out.getPositiveLabel();
										//			positive = out.getPositiveLabel();
										//		} else {
										//	globdiag = globdiag + " or " + out.getPositiveLabel();
										//			positive = positive + "," + out.getPositiveLabel();
										//		}
										//		}
										//		if ((out.isNegative())) {
										//				boolean first = true;
										//				for (String nl : out.getNegativeLabels()) {
										//					if (negative.equals("")) {
										//	globdiag = "(!" + nl;
										//						negative = nl;
										//					} else {
										//						if (first) {
										//	globdiag = globdiag + " or (!" + nl;
										//						negative = nl;
										//						} else {
										//	globdiag = globdiag + " and !" + nl;
										//						negative = negative + "," + nl;
										//					}
										//				}
										//				first = false;
										//			}
										//	globdiag = globdiag + ")";
										//	}
										//	}
										//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
									} else {
										execAut.next(label);
									}
								}
								if (conflViolated) {
									conflictingSets.add(conflCandidates[i]);
								}
								i++;
							}
							g++;
						}

						Vector finConfl = new Vector();
						for (int l = 0; l < conflictingSets.size(); l++) {
							finConfl.add(conflictingSets.get(l));
						}

						for (int i = 0; i < conflictingSets.size(); i++) {
							String[] currentConfl = (String[]) conflictingSets.get(i);
							boolean minimalConfl = true;
							int j = 0;
							if (currentConfl.length > 1) {
								while ((j < currentConfl.length) && minimalConfl) {
									String formula = "(";
									Vector formVect = new Vector();
									for (int l = 0; l < currentConfl.length; l++) {
										if (!currentConfl[l].equals(currentConfl[j])) {
											formVect.add(currentConfl[l]);
										}
									}
									for (int k = 0; k < formVect.size(); k++) {
										if (k != (formVect.size() - 1)) {
											formula = formula + formVect.get(k) + ")&&(";
										} else {
											formula = formula + formVect.get(k) + ")";
											;
										}
									}
									List<Formula> formulaeParsed = new ArrayList<Formula>();
									formulaeParsed.add(new DefaultParser(formula).parse());
									TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory
											.getInstance();
									ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
											.getFactory(treeFactory);
									GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
									aut = conjunction.getAutomaton().op.reduce();
									execAut = new ExecutableAutomaton(aut);
									execAut.ini();
									Iterator moevents = replay.iterator();
									boolean used = false;
									while (moevents.hasNext() || !used) {
										XEvent eve = null;
										if (!moevents.hasNext()) {
											used = true;
											eve = completeEvent;
										} else {
											eve = (XEvent) moevents.next();
										}
										label = "";
										//	globdiag = "";
										conflViolated = true;
										label = ((XAttributeLiteral) eve.getAttributes().get("concept:name"))
												.getValue();
										current = null;
										current = execAut.currentState();
										if(current!=null&& !(current.get(0)==null)){
											for (Transition out : current.output()) {
												if (out.parses(label)) {
													conflViolated = false;
													break;
												}
											}
										}
										//	System.out.println(str);
										if (conflViolated) {
											if (!moevents.hasNext()) {
												minimalConfl = false;
											}
											//	for (Transition out : current.output()) {
											//		if ((out.isPositive())) {
											//			if (globdiag.equals("")) {
											//				globdiag = out.getPositiveLabel();
											//				positive = out.getPositiveLabel();
											//			} else {
											//				globdiag = globdiag + " or " + out.getPositiveLabel();
											//				positive = positive + "," + out.getPositiveLabel();
											//			}
											//		}
											//		if ((out.isNegative())) {
											//			boolean first = true;
											//			for (String nl : out.getNegativeLabels()) {
											//				if (globdiag.equals("")) {
											//					globdiag = "(!" + nl;
											//					negative = nl;
											//				} else {
											//					if (first) {
											//						globdiag = globdiag + " or (!" + nl;
											//						negative = nl;
											//					} else {
											//						globdiag = globdiag + " and !" + nl;
											//						negative = negative + "," + nl;
											//					}
											//				}
											//				first = false;
											//			}
											//			globdiag = globdiag + ")";
											//		}
											//	}
											//	globdiag = "for this vessel type " + label + " was detected it was expected " + globdiag;
										} else {
											execAut.next(label);
										}
									}
									j++;
								}
							}
							if (!minimalConfl) {
								finConfl.remove(currentConfl);
							}
						}

						if (!finConfl.isEmpty()) {
							for (int i = 0; i < finConfl.size(); i++) {
								String[] currentConfl = (String[]) finConfl.get(i);
								for (int j = 0; j < currentConfl.length; j++) {
									for (int l = 0; l < forms.length; l++) {
										if (currentConfl[j].equals(forms[l])) {
											if (currentConfl.length == 1) {
												matrix[l][evPos] = "viol";
												eventAffection = eventAffection
														+ new Double((String) weights.get(i)).doubleValue();
											} else {
												matrix[l][evPos] = "conflict";
											}
										}
									}
								}
							}
						}
					}
				}
				/////////////////////////////// aaaaaaaaaaaaaaa
				//	if (true) {//(!pinpointViol && violated) || !violated || (pinpointViol && (!(label.equals("complete") && violated)))) {
				String result = "[";
				int intCounterMin;
				int intCounterMax;
				String INF = "inf";
				for (int i = 0; i < matrix.length; i++) {
					intCounterMin = 0;
					intCounterMax = 0;
					String oldStatus = matrix[i][0];
					for (int j = 0; j < matrix[0].length; j++) {
						if (matrix[i][j] == null) {
							matrix[i][j] = oldStatus;
						}
						if ((j == (matrix[0].length - 1)) && (i == (matrix.length - 1))) {
							if (!matrix[i][j].equals(oldStatus)) {
								result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
										+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax) + "]),";
								oldStatus = matrix[i][j];
								intCounterMin = intCounterMax;
							}
							result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j] + "),["
									+ convert(trace, intCounterMin) + "," + INF + "])]";
							intCounterMin = intCounterMax;
							intCounterMax++;
						} else {
							if (j == (matrix[0].length - 1)) {
								if (matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
											+ "),[" + convert(trace, intCounterMin) + "," + INF + "]),";
								} else {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "])," + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
													+ "),[" + convert(trace, intCounterMax) + "," + INF + "]),";
								}
							} else {
								if (!matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "]),";
									oldStatus = matrix[i][j];
									intCounterMin = intCounterMax;
								}
							}
						}
						intCounterMax++;

					}
				}

				if (violated) {
					eventAffection = eventAffection + 1;
				}
				//Analysis h = new Analysis("Health of the system");
				healthWindow.add(eventAffection);
				double temp = 0;
				for (int h = 0; h < healthWindow.size(); h++) {
					temp = temp + ((Double) healthWindow.get(h)).doubleValue();
				}
				double fact = 0;
				for (int l = 0; l < weights.size(); l++) {
					fact = fact + new Double((String) weights.get(l)).doubleValue();
				}
				health = 1 - (temp / ((fact) * healthWindow.size()));
				XExtendedEvent extEv = new XExtendedEvent(ev);
				extEv.getTimestamp().getTime();
				String fluents = result;
				result = result.subSequence(1, result.length() - 1).toString(); //remove "[" and "]"
				Scanner scanner = new Scanner(result);
				scanner.useDelimiter("mholds_for");

				while (scanner.hasNext()) {
					String mviStr = scanner.next();
					mviStr = mviStr.subSequence(1, mviStr.length() - 1).toString(); //remove "(" and ")"
					int removeTail = scanner.hasNext() ? 2 : 1;
					mviStr = mviStr.substring(0, mviStr.length() - removeTail); //remove "]," or "]"
					StringTokenizer tokenizer = new StringTokenizer(mviStr, "[", true);

					String fluent = "";
					String token;
					while (tokenizer.hasMoreTokens()) {
						token = tokenizer.nextToken();
						if (tokenizer.hasMoreTokens()) {
							fluent += token;
						} else {
							fluent = fluent.substring(0, fluent.length() - 2); //remove ,[
							//	token.split(",")[0];
							//	token.split(",")[1];
						}
					}
					//a.put("isStopped", false);
					//if (premstopped) {
					//	a.put("prematureCompletion", premdiag);
					//}
					/*
					 * if (!premstopped && (pinpointViol && violated)) {
					 * a.put("single", singlediag); } if (!premstopped &&
					 * (pinpointViol && violated)) { a.put("global", globdiag);
					 * a.put("isStopped", true); }
					 */
					//if(violated){
					//	a.put("global", globdiag);
				}
				output = new Hashtable();
				if (violated) {
					output.put("global", "violation");
				}
				output.put("health", health);
				output.put("positive", positive);
				output.put("negative", negative);
				output.put("fluents", fluents);
				//a.put("fluent", fluent);
				//a.put("start", startTime);
				//	if (!endTime.equals("_")) {
				//		a.put("end", endTime);
				//	}

				/*
				 * } else {
				 * 
				 * //Analysis a = new Analysis("Business Constraints Status");
				 * if (label.equals("complete") && violated) {
				 * a.put("isStopped", true); } else { a.put("global",
				 * "GLOBAL VESSEL TYPE VIOLATION! " + globdiag); }
				 * a.put("health", health); a.put("positive", positive);
				 * a.put("negative", negative); //response.addAnalysis(a); }
				 */
				currentState.setpAut(pAut);
				currentState.setpExecAut(pExecAut);
				currentState.setAut(aut);
				currentState.setExecAut(execAut);
				currentState.setMatrix(matrix);
				currentState.setEventPos(evPos);
				currentState.setHealth(health);
				currentState.setHealthWindow(healthWindow);
				session.put("stateObject", currentState);
			} else {

				for (int i = 0; i < matrix.length; i++) {
					if (matrix[i][evPos - 1].equals("poss.viol")) {
						matrix[i][evPos] = "viol";
					} else if (matrix[i][evPos - 1].equals("poss.sat")) {
						matrix[i][evPos] = "sat";
					} else {
						matrix[i][evPos] = matrix[i][evPos - 1];
					}
				}
				boolean premstopped = false;
				String premdiag = "";
				for (int i = 0; i < pAut.length; i++) {
					current = pExecAut[i].currentState();
					if (current.isAccepting()) {
						matrix[i][evPos] = matrix[i][evPos - 1];
					} else {
						premstopped = true;
						matrix[i][evPos] = "viol";
						eventAffection = eventAffection + new Double((String) weights.get(i)).doubleValue();
						if (premdiag.equals("")) {
							premdiag = constraints[i];
						} else {
							premdiag = premdiag + ", " + constraints[i];
						}
					}
				}
				String result = "[";
				if (premstopped) {
					premdiag = "Case completed with " + premdiag + " still pending!";
				}
				int intCounterMin;
				int intCounterMax;
				String INF = "inf";
				for (int i = 0; i < matrix.length; i++) {
					intCounterMin = 0;
					intCounterMax = 0;
					String oldStatus = matrix[i][0];
					for (int j = 0; j < matrix[0].length; j++) {
						if (matrix[i][j] == null) {
							matrix[i][j] = oldStatus;
						}
						if ((j == (matrix[0].length - 1)) && (i == (matrix.length - 1))) {
							if (!matrix[i][j].equals(oldStatus)) {
								result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
										+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax) + "]),";
								oldStatus = matrix[i][j];
								intCounterMin = intCounterMax;
							}
							result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j] + "),["
									+ convert(trace, intCounterMin) + "," + INF + "])]";
							intCounterMin = intCounterMax;
							intCounterMax++;
						} else {
							if (j == (matrix[0].length - 1)) {
								if (matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
											+ "),[" + convert(trace, intCounterMin) + "," + INF + "]),";
								} else {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "])," + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
													+ "),[" + convert(trace, intCounterMax) + "," + INF + "]),";
								}
							} else {
								if (!matrix[i][j].equals(oldStatus)) {
									result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
											+ convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
											+ "]),";
									oldStatus = matrix[i][j];
									intCounterMin = intCounterMax;
								}
							}
						}
						intCounterMax++;
					}
				}

				//Analysis h = new Analysis("Health of the system");
				healthWindow.add(eventAffection);
				double temp = 0;
				for (int h = 0; h < healthWindow.size(); h++) {
					temp = temp + ((Double) healthWindow.get(h)).doubleValue();
				}
				double fact = 0;
				for (int l = 0; l < weights.size(); l++) {
					fact = fact + new Double((String) weights.get(l)).doubleValue();
				}
				health = 1 - (temp / ((fact) * healthWindow.size()));
				String stringone = result;
				result = result.subSequence(1, result.length() - 1).toString(); //remove "[" and "]"
				Scanner scanner = new Scanner(result);
				scanner.useDelimiter("mholds_for");
				while (scanner.hasNext()) {
					String mviStr = scanner.next();
					mviStr = mviStr.subSequence(1, mviStr.length() - 1).toString(); //remove "(" and ")"
					int removeTail = scanner.hasNext() ? 2 : 1;
					mviStr = mviStr.substring(0, mviStr.length() - removeTail); //remove "]," or "]"
					StringTokenizer tokenizer = new StringTokenizer(mviStr, "[", true);

					String fluent = "";
					String token;
					while (tokenizer.hasMoreTokens()) {
						token = tokenizer.nextToken();
						if (tokenizer.hasMoreTokens()) {
							fluent += token;
						} else {
							fluent = fluent.substring(0, fluent.length() - 2); //remove ,[
							//token.split(",")[0];
							//token.split(",")[1];
						}
					}
					output = new Hashtable();
					if (premstopped) {
						output.put("prematureCompletion", premdiag);
					}
					output.put("health", health);
					output.put("positive", positive);
					output.put("negative", negative);
					output.put("fluents", stringone);
					//a.put("fluent", fluent);
					//a.put("start", startTime);
					//if (!endTime.equals("_")) {
					//	a.put("end", endTime);
					//}
				}
			}
		}
		System.out.println("Sending output : - ");
		System.out.println(output.get("fluents"));
		return (R) output;
	}

	public static DeclareTemplate getTemplate(ConstraintDefinition constraint){
		if(constraint.getName().toLowerCase().equals("absence")){
			return DeclareTemplate.Absence;
		}
		if(constraint.getName().toLowerCase().equals("absence2")){
			return DeclareTemplate.Absence2;
		}
		if(constraint.getName().toLowerCase().equals("absence3")){
			return DeclareTemplate.Absence3;
		}
		if(constraint.getName().toLowerCase().equals("alternate precedence")){
			return DeclareTemplate.Alternate_Precedence;
		}
		if(constraint.getName().toLowerCase().equals("alternate response")){
			return DeclareTemplate.Alternate_Response;
		}
		if(constraint.getName().toLowerCase().equals("alternate succession")){
			return DeclareTemplate.Alternate_Succession;
		}
		if(constraint.getName().toLowerCase().equals("chain precedence")){
			return DeclareTemplate.Chain_Precedence;
		}
		if(constraint.getName().toLowerCase().equals("chain response")){
			return DeclareTemplate.Chain_Response;
		}
		if(constraint.getName().toLowerCase().equals("chain succession")){
			return DeclareTemplate.Chain_Succession;
		}
		if(constraint.getName().toLowerCase().equals("precedence")){
			return DeclareTemplate.Precedence;
		}
		if(constraint.getName().toLowerCase().equals("response")){
			return DeclareTemplate.Response;
		}
		if(constraint.getName().toLowerCase().equals("succession")){
			return DeclareTemplate.Succession;
		}
		if(constraint.getName().toLowerCase().equals("responded existence")){
			return DeclareTemplate.Responded_Existence;
		}
		if(constraint.getName().toLowerCase().equals("co-existence")){
			return DeclareTemplate.CoExistence;
		}
		if(constraint.getName().toLowerCase().equals("exclusive choice")){
			return DeclareTemplate.Exclusive_Choice;
		}
		if(constraint.getName().toLowerCase().equals("choice")){
			return DeclareTemplate.Choice;
		}
		if(constraint.getName().toLowerCase().equals("existence")){
			return DeclareTemplate.Existence;
		}
		if(constraint.getName().toLowerCase().equals("existence2")){
			return DeclareTemplate.Existence2;
		}
		if(constraint.getName().toLowerCase().equals("existence3")){
			return DeclareTemplate.Existence3;
		}
		if(constraint.getName().toLowerCase().equals("exactly1")){
			return DeclareTemplate.Exactly1;
		}
		if(constraint.getName().toLowerCase().equals("exactly2")){
			return DeclareTemplate.Exactly2;
		}	
		if(constraint.getName().toLowerCase().equals("init")){
			return DeclareTemplate.Init;
		}
		if(constraint.getName().toLowerCase().equals("not chain succession")){
			return DeclareTemplate.Not_Chain_Succession;
		}
		if(constraint.getName().toLowerCase().equals("not succession")){
			return DeclareTemplate.Not_Succession;
		}	 
		if(constraint.getName().toLowerCase().equals("not co-existence")){
			return DeclareTemplate.Not_CoExistence;
		}
		return null;
	}
}