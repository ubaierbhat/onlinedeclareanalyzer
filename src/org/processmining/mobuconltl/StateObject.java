package org.processmining.mobuconltl;

import java.util.Vector;

import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.plugins.declareminer.ExecutableAutomaton;

public class StateObject {
	private Automaton aut;
	private Automaton[] pAut;
	private ExecutableAutomaton execAut;
	private ExecutableAutomaton[] pExecAut;
	private int eventPos;
	private String[][] matrix;
	private double health;
	private Vector healthWindow;

	public Vector getHealthWindow() {
		return healthWindow;
	}

	public void setHealthWindow(Vector healthWindow) {
		this.healthWindow = healthWindow;
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(double health) {
		this.health = health;
	}

	public int getEventPos() {
		return eventPos;
	}

	public void setEventPos(int eventPos) {
		this.eventPos = eventPos;
	}

	public String[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(String[][] matrix) {
		this.matrix = matrix;
	}

	public Automaton getAut() {
		return aut;
	}

	public void setAut(Automaton aut) {
		this.aut = aut;
	}

	public Automaton[] getpAut() {
		return pAut;
	}

	public void setpAut(Automaton[] pAut) {
		this.pAut = pAut;
	}

	public ExecutableAutomaton getExecAut() {
		return execAut;
	}

	public void setExecAut(ExecutableAutomaton execAut) {
		this.execAut = execAut;
	}

	public ExecutableAutomaton[] getpExecAut() {
		return pExecAut;
	}

	public void setpExecAut(ExecutableAutomaton[] pExecAut) {
		this.pExecAut = pExecAut;
	}

}
