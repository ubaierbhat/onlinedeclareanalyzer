package org.processmining.mobuconltl;

import org.processmining.plugins.declareminer.visualizing.DeclareMap;



public class WeightedDeclareMap {

	private DeclareMap model;

	public DeclareMap getModel() {
		return model;
	}

	public void setModel(DeclareMap model) {
		this.model = model;
	}

	private double[] weights;

	public double[] getWeights() {
		return weights;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

}
