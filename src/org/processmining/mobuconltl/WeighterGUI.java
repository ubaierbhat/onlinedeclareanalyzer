package org.processmining.mobuconltl;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;
import org.processmining.plugins.declareminer.visualizing.DeclareMap;

import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;

public class WeighterGUI extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5750448897838002124L;
	DeclareMap model;
	JTextField[] fields;

	public WeighterGUI(DeclareMap model) {
		this.model = model;
	}

	public JPanel createGui() {
		SlickerFactory sf = SlickerFactory.instance();
		JPanel mainPane = sf.createRoundedPanel();

		Iterable<ConstraintDefinition> constraintsList = model.getModel().getConstraintDefinitions();
		int k = 0;
		String[] constraints = new String[model.getModel().constraintDefinitionsCount()];
		for (ConstraintDefinition constr : constraintsList) {
			constraints[k] = constr.getName();
			k++;
		}
		JPanel constraintsPane = sf.createRoundedPanel();
		JPanel fieldsPane = sf.createRoundedPanel();

		double[] sizes = new double[constraints.length + 1];
		for (int i = 0; i < constraints.length + 1; i++) {
			sizes[i] = 30;
		}
		TableLayout l = new TableLayout(new double[][] { { 900 }, sizes });
		TableLayout l2 = new TableLayout(new double[][] { { 100 }, sizes });
		constraintsPane.setLayout(l);
		fieldsPane.setLayout(l2);
		for (int i = 0; i < constraints.length; i++) {
			JLabel label = sf.createLabel(constraints[i]);
			constraintsPane.add(label, "0," + i);
		}
		fields = new JTextField[constraints.length];
		for (int i = 0; i < constraints.length; i++) {
			fields[i] = new JTextField();
			fields[i].setText("1");
			fieldsPane.add(fields[i], "0," + i);
		}

		JScrollPane scrollpan = new JScrollPane(constraintsPane);
		scrollpan.setOpaque(false);
		scrollpan.getViewport().setOpaque(false);
		scrollpan.setBorder(BorderFactory.createEmptyBorder());
		scrollpan.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollpan.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		SlickerDecorator.instance().decorate(scrollpan.getVerticalScrollBar(), new Color(0, 0, 0, 0),
				new Color(140, 140, 140), new Color(80, 80, 80));
		scrollpan.getVerticalScrollBar().setOpaque(false);

		SlickerDecorator.instance().decorate(scrollpan.getHorizontalScrollBar(), new Color(0, 0, 0, 0),
				new Color(140, 140, 140), new Color(80, 80, 80));
		scrollpan.getHorizontalScrollBar().setOpaque(false);

		TableLayout l3 = new TableLayout(new double[][] { { 800, 100, 100 }, { TableLayoutConstants.FILL } });
		mainPane.setLayout(l3);
		//mainPane.setMaximumSize(new Dimension (900,2000));
		//mainPane.setMinimumSize(new Dimension (900,2000));
		//	mainPane.setPreferredSize(new Dimension (900,2000));
		//	mainPane.setSize(new Dimension (900,2000));
		mainPane.add("0,0", scrollpan);
		mainPane.add("1,0", fieldsPane);
		//JScrollPane scrollpane = new JScrollPane(mainPane);
		//		scrollpane.setOpaque(false);
		//	scrollpane.getViewport().setOpaque(false);
		//	scrollpane.setBorder(BorderFactory.createEmptyBorder());
		//	scrollpane
		//	.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//	scrollpane
		//	.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		//	SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
		//			new Color(0, 0, 0, 0), new Color(140, 140, 140),
		//			new Color(80, 80, 80));
		//	scrollpane.getVerticalScrollBar().setOpaque(false);

		//	SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
		//			new Color(0, 0, 0, 0), new Color(140, 140, 140),
		//			new Color(80, 80, 80));
		//	scrollpane.getHorizontalScrollBar().setOpaque(false);
		//	scrollpane.setMaximumSize(new Dimension (900,2000));
		//	scrollpane.setMinimumSize(new Dimension (900,2000));
		//	scrollpane.setPreferredSize(new Dimension (900,2000));
		//	scrollpane.setSize(new Dimension (900,2000));

		return mainPane;

	}

	public Object[] getSettings() {
		Object[] out = new Object[fields.length];
		for (int i = 0; i < fields.length; i++) {
			out[i] = fields[i].getText();
		}
		return out;
	}

}
