package org.processmining.odaclient;

import java.awt.Color;
import java.util.Properties;

public class ColorMap extends Properties {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Color getColor(String key) {
		String value = super.getProperty(key);
		if (value == null) {
			return null;
		}
		String rHex = value.substring(0, 2);
		String gHex = value.substring(2, 4);
		String bHex = value.substring(4, 6);
		return new Color(Integer.parseInt(rHex, 16), Integer.parseInt(gHex, 16), Integer.parseInt(bHex, 16));
	}
}
