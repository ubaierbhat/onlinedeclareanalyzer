package org.processmining.odaclient;

import it.unibo.ai.rec.model.Fluent;
import it.unibo.ai.rec.model.FluentState;
import it.unibo.ai.rec.model.FluentsModel;
import it.unibo.ai.rec.model.TrendMetric;

import java.util.Hashtable;


public class HealthCountingMetric implements
		TrendMetric {

	private static String violStateId = "viol";
	private static String satStateId = "sat";
	private double gradient;
	private boolean countAll;
	private Hashtable health;

	public HealthCountingMetric() {
		this(false);
	}
	
	public HealthCountingMetric(boolean countAll) {
		this(countAll, 1);
	}
	
	public HealthCountingMetric(int gradient) {
		this(false, gradient);
	}
	
	public HealthCountingMetric(boolean countAll, int gradient) {
		this.countAll = countAll;
		this.gradient = gradient;
	}
	
	public HealthCountingMetric(Hashtable health) {
		this.health = health;
	}
	
	private boolean inState(Fluent f, String stateId, long time) {
		FluentState state = f.getStates().get(stateId);
		return state != null && state.holdsAt2(time);
	}
	
	@Override
	public double getValue(FluentsModel monitoringState, long time) {
		if(time == -1){
			return 1.;
		}
			return (Double)health.get(time);
	}
	
	
	@Override
	public String getName() {
		return "system health";
	}

 
}
