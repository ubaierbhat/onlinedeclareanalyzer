package org.processmining.odaclient;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.fluxicon.slickerbox.factory.SlickerFactory;

public class ListRenderer implements ListCellRenderer {

	private final JLabel idLabel;
	private final JLabel violLabel;
	private final JPanel panel;

	public ListRenderer() {
		SlickerFactory sf = SlickerFactory.instance();
		panel = new JPanel();
		panel.setBackground(Color.black);
		idLabel = sf.createLabel("");
		idLabel.setForeground(Color.yellow);
		violLabel = sf.createLabel("WARNING!");
		//violLabel.setVisible(false);
		violLabel.setForeground(Color.red);
		panel.setLayout(new TableLayout(new double[][] { { 100, 70 }, { 20 } }));
		//panel.add(bef, BorderLayout.NORTH);
		panel.add(idLabel, "0,0");

		//panel.setBorder(new EmptyBorder(3,2,3,0));
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// TODO Auto-generated method stub
		InstanceInfo c = (InstanceInfo) value;
		idLabel.setText(c.getId());
		if (c.isViolated()) {
			panel.add(violLabel, "1,0");
		} else {
			panel.remove(violLabel);
		}
		if (isSelected) {
			panel.setBackground(Color.blue);
		} else {
			panel.setBackground(Color.black);
		}
		//panel.revalidate();
		return panel;
	}

}
