package org.processmining.odaclient;

import com.fluxicon.slickerbox.colors.SlickerColors;
import com.fluxicon.slickerbox.components.SlickerTabbedPane;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;
import it.unibo.ai.rec.common.TimeGranularity;
import it.unibo.ai.rec.engine.FluentsConverter;
import it.unibo.ai.rec.model.FluentsModel;
import it.unibo.ai.rec.model.NoGroupingStrategy;
import it.unibo.ai.rec.model.RecTrace;
import it.unibo.ai.rec.visualization.BasicDateEventOutputter;
import it.unibo.ai.rec.visualization.FluentChartContainer;
import it.unibo.ai.rec.visualization.FluentChartFactory;
import it.unibo.ai.rec.visualization.FluentChartStandardPanel;
import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.processmining.onlinedeclareanalyzer.data.DeclareLanguage;
import org.processmining.onlinedeclareanalyzer.data.DeclareMonitorQuery;
import org.processmining.operationalsupport.client.InvocationException;
import org.processmining.operationalsupport.client.SessionHandle;
import org.processmining.operationalsupport.messages.reply.ResponseSet;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

class OnlineDeclareAnalyzerClient extends JFrame implements ActionListener, MouseListener, KeyListener {


    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss:S";
    public static final String CASE_COMPLETE = "case_complete";
    private static final long serialVersionUID = 1561407447457027863L;
    private static final int MONITOR_TAB = 0;
    private static final int HEALTH_TAB = 1;
    private static final int DIAGNOSTICS_TAB = 2;
    private static int PORT = 4444;
    private static String HOST;

    static {
        try {
            // Get hostname
            HOST = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }
    }

    //	private String confPath;
    private final Hashtable handles;
    private FluentsConverter converter;
    boolean primo = true;
    FluentChartContainer chartPanel;

    private int value;
    private FluentChartFactory factory;
    private Hashtable healthHash = new Hashtable();
    private Color backgroundColor = SlickerColors.COLOR_BG_4;
    private Color chartBackgroundColor = new Color(232, 232, 232);
    private JRadioButton showViolated;
    private JRadioButton showAll;
    private JRadioButton searchRadioButton;
    private JButton search;
    private String shipType = "REF MODEL: no model selected";
    private int selectedTab;
    private boolean showV;
    private boolean showA;
    private boolean searching;
    private Map<String, Object> analysis;
    private JTextField searchField;
    private Action startAction;

    private JScrollPane outputContainer;
    private JScrollPane instancesContainer;

    private JPanel roundedPanelHealth;
    private JPanel roundedPanelDiag;
    private JPanel completeListPanel;
    private JPanel filterPanel;
    private JPanel searchPanel;

    private JList diagnosticsList;

    private SlickerTabbedPane tpInstances;
    private Hashtable violationsModels;
    private Hashtable healthGraphs;
    private Hashtable monitorGraphs;
    private Hashtable partialTraces;
    private Hashtable globalResponses;
    private Hashtable healths;
    private Hashtable types;
    private Vector instancesAll;
    private Vector instancesViol;
    private String selected = "";
    private Vector instances;
    private JList instancesList;
    private XTrace trace;
    HashMap<String, XTrace> traceHashMap;
    AssignmentModel assignmentModel = null;
    private static boolean modelReceived;
    OSXMLConverter osxmlConverter = new OSXMLConverter();

    private OnlineDeclareAnalyzerClient(String confPath) throws JDOMException, IOException {
        super("Online Declare Analyzer Client");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        try {
            factory = new FluentChartFactory(TimeGranularity.MILLIS, new BasicDateEventOutputter(
                    TimeGranularity.MILLIS, DATE_FORMAT), false, "stateColors.properties", "fluentColors.properties");
        } catch (Exception e) {
            e.printStackTrace();
        }


        HOST = "localhost";
        instancesAll = new Vector();
        instancesViol = new Vector();
        violationsModels = new Hashtable();
        healthGraphs = new Hashtable();
        handles = new Hashtable();
        monitorGraphs = new Hashtable();
        partialTraces = new Hashtable();
        globalResponses = new Hashtable();
        types = new Hashtable();
        healths = new Hashtable();
        healthHash = new Hashtable();


        initActions();


        instancesList = new JList();
        instancesList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int[] selectedIndices = instancesList.getSelectedIndices();
                if (selectedIndices.length > 0) {
                    if (!selected.equals(((InstanceInfo) instances.get(selectedIndices[0])).getId()))
                        instancesSelectionChanged();
                }
            }
        });
        instancesList.setCellRenderer(new ListRenderer());
        //instancesList.setModel(new ListModel(instances));
        outputContainer = createSlickerScrollPane();
        instancesList.setBackground(Color.black);
        instancesList.setForeground(Color.yellow);
        instancesList.addMouseListener(this);
        instancesContainer = createSlickerScrollPane(instancesList);
        instancesContainer.setSize(200, 800);
        instancesContainer.setPreferredSize(new Dimension(200, 800));
        instancesContainer.setMaximumSize(new Dimension(200, 800));
        instancesContainer.setMinimumSize(new Dimension(200, 800));
        instancesContainer.addMouseListener(this);
        SlickerFactory factory = SlickerFactory.instance();
        search = factory.createButton("search");
        search.addActionListener(this);
        search.setActionCommand("searching");
        searchField = new JTextField();
        searchField.addMouseListener(this);
        searchField.addKeyListener(this);
        searchField.setSize(new Dimension(100, 20));
        searchField.setMaximumSize(new Dimension(100, 20));
        searchField.setMinimumSize(new Dimension(100, 20));
        searchField.setPreferredSize(new Dimension(100, 20));
        searchPanel = factory.createRoundedPanel();
        searchPanel.setLayout(new BorderLayout());
        searchPanel.add(searchField, BorderLayout.WEST);
        //searchPanel.add(search, BorderLayout.EAST);
        search.setEnabled(false);
        searching = false;
        showA = true;
        showV = false;
        showAll = factory.createRadioButton("show All");
        showAll.addActionListener(this);
        showAll.setActionCommand("showAll");
        showAll.setSelected(true);
        showViolated = factory.createRadioButton("show warnings");
        showViolated.addActionListener(this);
        showViolated.setActionCommand("showViolated");
        searchRadioButton = factory.createRadioButton("search ID");
        searchRadioButton.addActionListener(this);
        searchRadioButton.setActionCommand("search");
        ButtonGroup bg = new ButtonGroup();
        bg.add(showAll);
        bg.add(showViolated);
        bg.add(searchRadioButton);
        JPanel contentPane = factory.createRoundedPanel();
        JPanel rbPanel = factory.createRoundedPanel();
        rbPanel.setLayout(new BorderLayout());
        rbPanel.add(showAll, BorderLayout.NORTH);
        rbPanel.add(showViolated, BorderLayout.CENTER);
        rbPanel.add(searchRadioButton, BorderLayout.SOUTH);
        filterPanel = factory.createRoundedPanel();
        filterPanel.setLayout(new BorderLayout());
        filterPanel.add(rbPanel, BorderLayout.NORTH);
        filterPanel.add(searchPanel, BorderLayout.SOUTH);

        completeListPanel = factory.createRoundedPanel();
        completeListPanel.setLayout(new BorderLayout());
        completeListPanel.add(instancesContainer, BorderLayout.CENTER);
        completeListPanel.add(filterPanel, BorderLayout.PAGE_END);


        contentPane.setBackground(backgroundColor);
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout());

        JToolBar toolBar = new JToolBar();
        toolBar.setOpaque(false);
        toolBar.setFloatable(false);

        ImageLozengeButton startButton = new ImageLozengeButton(ImageLoader
                .load("next_white_30x30.png"), "Start", new Color(0, 90, 0),
                new Color(0, 140, 0), 4);
        startButton.setLabelColor(Color.white);
        startButton.setAction(startAction);
        toolBar.add(startButton);

        toolBar.addSeparator();

        selectedTab = MONITOR_TAB;


        diagnosticsList = new JList();


        contentPane.add(toolBar, BorderLayout.PAGE_START);

        contentPane.add(outputContainer, BorderLayout.CENTER);
        contentPane.add(completeListPanel, BorderLayout.WEST);
        outputContainer.getViewport().add(Box.createRigidArea(new Dimension(800, 800)));

        setSize(800, 600);
        outputContainer.getHorizontalScrollBar().addMouseListener(this);

        roundedPanelHealth = SlickerFactory.instance().createRoundedPanel(50, chartBackgroundColor);


        roundedPanelHealth.setLayout(new BoxLayout(roundedPanelHealth, BoxLayout.LINE_AXIS));

        roundedPanelDiag = SlickerFactory.instance().createRoundedPanel();
        primo = true;

    }

    public static void main(String[] args) {
        try {
            if (args.length > 0) {
                new OnlineDeclareAnalyzerClient(args[0]).setVisible(true);
            } else {
                new OnlineDeclareAnalyzerClient("conf.xml").setVisible(true);
            }
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("serial")
    private void initActions() {
        startAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnabled(false);
                sendRequest();
            }

        };
        startAction.setEnabled(true);

    }

    private JScrollPane createSlickerScrollPane() {

        JScrollPane scrollpane = new JScrollPane();
        scrollpane.setOpaque(false);
        scrollpane.getViewport().setOpaque(false);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());


        scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getVerticalScrollBar().setOpaque(false);

        SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getHorizontalScrollBar().setOpaque(false);
        return scrollpane;
    }

    private JScrollPane createSlickerScrollPane(JList list) {


        JScrollPane scrollpane = new JScrollPane(list);
        scrollpane.setOpaque(false);
        scrollpane.getViewport().setOpaque(false);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());


        scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        SlickerDecorator.instance().decorate(scrollpane.getVerticalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getVerticalScrollBar().setOpaque(false);

        SlickerDecorator.instance().decorate(scrollpane.getHorizontalScrollBar(),
                new Color(0, 0, 0, 0), new Color(140, 140, 140),
                new Color(80, 80, 80));
        scrollpane.getHorizontalScrollBar().setOpaque(false);
        return scrollpane;
    }

    protected void instancesSelectionChanged() {
        int[] selectedIndices = instancesList.getSelectedIndices();
        if (selectedIndices.length > 0) {
            selected = ((InstanceInfo) instances.get(selectedIndices[0])).getId();
            shipType = "REF MODEL: " + (String) types.get(selected);
            instancesContainer.revalidate();
            instancesContainer.repaint();
            roundedPanelHealth.removeAll();
            if ((FluentChartStandardPanel) healthGraphs.get(selected) != null) {
                roundedPanelHealth.add((FluentChartStandardPanel) healthGraphs.get(selected));
            }
            if ((Vector) violationsModels.get(selected) != null) {
                diagnosticsList.setModel(new ViolationListModel((Vector) violationsModels.get(selected)));
            } else {
                diagnosticsList.setModel(new ViolationListModel(new Vector()));
            }
            tpInstances = new SlickerTabbedPane(shipType, new Color(240, 240, 240, 230), new Color(0, 0, 0, 230),
                    new Color(220, 220, 220, 150));
            tpInstances.addTab("Monitor", roundedPanelHealth);
            tpInstances.addTab("Diagnostics", roundedPanelDiag);
            tpInstances.addMouseListener(this);
            if (selectedTab == HEALTH_TAB) {
                tpInstances.selectTab("Health Trend");
            } else if (selectedTab == MONITOR_TAB) {
                tpInstances.selectTab("Monitor");
            } else {
                tpInstances.selectTab("Diagnostics");
            }
            outputContainer.getViewport().removeAll();
            outputContainer.getViewport().add(tpInstances);
            outputContainer.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            outputContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        }
    }

    private void sendRequest() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ProxyDaemon daemon;
        try {
            daemon = new ProxyDaemon(PORT, this);
            daemon.start();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void updateOutput(String piID, String eventName, String[] completeSetEvents) {
        System.out.println("updateOutput piID: " + piID + " eventName: "+ eventName  );
        try {
            XTrace trace = (XTrace) partialTraces.get(piID);
            boolean timestampPrinted = false;
            double healthValue;
            Vector healthvec;
            if (healths.containsKey(piID)) {
                healthvec = (Vector) healths.get(piID);
            } else {
                healthvec = new Vector();
            }
            healthvec.add((Math.rint((((Double) analysis.get("health"))) * Math.pow(10, 2)) / Math
                    .pow(10, 2)));
            healths.put(piID, healthvec);
            int count = 1;

            count++;
            Vector violations;


            if ((analysis.get("prematureCompletion") != null) || (analysis.get("single") != null) || (analysis.get("global") != null)) {
                if (analysis.get("prematureCompletion") != null) {
                    if (violationsModels.containsKey(piID)) {
                        violations = (Vector) violationsModels.get(piID);
                    } else {
                        violations = new Vector();
                    }
                    timestampPrinted = true;
                    violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
                    violations.add(analysis.get("prematureCompletion"));

                    violationsModels.put(piID, violations);

                }
                if (analysis.get("single") != null) {


                    if (violationsModels.containsKey(piID)) {
                        violations = (Vector) violationsModels.get(piID);
                    } else {
                        violations = new Vector();
                    }
                    timestampPrinted = true;

                    violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
                    violations.add(analysis.get("single"));

                    violationsModels.put(piID, violations);

                }

                if (analysis.get("global") != null) {
                    //	roundedPanelMon.setBackground(Color.red);
                    if (violationsModels.containsKey(piID)) {
                        violations = (Vector) violationsModels.get(piID);
                    } else {
                        violations = new Vector();
                    }
                    //globalviolLabel.setText((String)viol.get("global"));
                    if (timestampPrinted == false) {
                        violations.add("AT " + (XExtendedEvent.wrap(trace.get(trace.size() - 1))).getTimestamp());
                    }

                    String diags = "Observed   " + eventName;

                    String positive = (String) analysis.get("positive");
                    if (!positive.isEmpty()) {
                        if (positive != null) {
                            String[] positivesForSize = positive.split(",");
                            if (positivesForSize.length == 1) {
                                diags = diags + ",   while for this reference model expecting   " + positivesForSize[0];
                            } else {
                                String posList = positive.replace(",", ", ");
                                int indCo = posList.lastIndexOf(",");
                                String sub = posList.substring(indCo);
                                posList = posList.replaceFirst(sub, sub.replaceFirst(", ", ", or "));
                                diags = diags + ",   while for this reference model expecting   " + posList;
                            }
                        }
                    }

                    String[] negativeSet = null;
                    String negative = (String) analysis.get("negative");
                    if (!negative.isEmpty()) {
                        if (negative != null) {
                            negativeSet = negative.split(",");
                        }
                    }
                    String[] positiveSet = positive.split(",");
                    Vector ve = new Vector();
                    for (int i = 0; i < positiveSet.length; i++) {
                        ve.add(positiveSet[i]);
                    }
                    if (negativeSet != null) {
                        if (negativeSet.length >= 1) {
                            Vector vecCom = new Vector();
                            for (int g = 0; g < completeSetEvents.length; g++) {
                                vecCom.add(completeSetEvents[g]);
                            }
                            Vector vecNeg = new Vector();
                            for (int g = 0; g < negativeSet.length; g++) {
                                vecNeg.add(negativeSet[g]);
                            }
                            for (int g = 0; g < completeSetEvents.length; g++) {
                                if (vecNeg.contains(completeSetEvents[g])) {
                                    vecCom.remove(completeSetEvents[g]);
                                }
                            }
                            diags = diags + "   (or otherwise everything different from   ";
                            String last = null;
                            if (vecNeg.size() == 1) {
                                diags = diags + vecNeg.get(0) + ")";
                            } else {
                                for (int g = 0; g < vecNeg.size(); g++) {
                                    if (!ve.contains(vecNeg.get(g))) {
                                        diags = diags + vecNeg.get(g) + ", ";
                                        last = (String) vecNeg.get(g);
                                    }
                                }

                                diags = diags.replace(", " + last + ", ", " and " + last + ")");

                            }
                        }
                    }


                    violations.add(diags);
                    violationsModels.put(piID, violations);

                }
            }

            String fluent = (String) analysis.get("fluents");

            System.out.println(fluent);
            HealthCountingMetric metric = new HealthCountingMetric(healthHash);
            FluentsModel model = converter.toFluentsModel(fluent);
            Xes2RecTraceTranslator traceTranslator = new Xes2RecTraceTranslator(
                    TimeGranularity.MILLIS, Xes2RecTraceTranslator.TimestampStrategy.ABSOLUTE);
            RecTrace rtrace = traceTranslator.translate(trace);


            chartPanel = new FluentChartStandardPanel(factory, metric);
            chartPanel.update(rtrace, model);

            chartPanel.getChartPanel().setOpaque(true);
            chartPanel.getChartPanel().setBackground(Color.white);

            roundedPanelDiag.setLayout(new BorderLayout());

            if (primo) {
                roundedPanelHealth.add(chartPanel);
                primo = false;
            }
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                }
            });
            roundedPanelDiag.add(diagnosticsList, BorderLayout.NORTH);
            chartPanel.setAlignmentY(Component.TOP_ALIGNMENT);
            monitorGraphs.put(piID, chartPanel);
            healthGraphs.put(piID, chartPanel);
            if (selected.equals(piID)) {
                tpInstances = new SlickerTabbedPane(shipType, new Color(240, 240, 240, 230), new Color(0, 0, 0, 230),
                        new Color(220, 220, 220, 150));
                tpInstances.addTab("Monitor", roundedPanelHealth);

                tpInstances.addTab("Diagnostics", roundedPanelDiag);
                tpInstances.addMouseListener(this);
                if (selectedTab == HEALTH_TAB) {
                    tpInstances.selectTab("Health Trend");
                } else if (selectedTab == MONITOR_TAB) {
                    tpInstances.selectTab("Monitor");
                } else {
                    tpInstances.selectTab("Diagnostics");
                }
                outputContainer.getViewport().removeAll();
                outputContainer.getViewport().add(tpInstances);


                outputContainer.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                outputContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            }
            outputContainer.getHorizontalScrollBar().setValue(value);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();

        if (action.equals("showViolated")) {
            showV = true;
            showA = false;
            searching = false;

            int ind = -1;
            instances = new Vector();
            for (int i = 0; i < instancesViol.size(); i++) {
                InstanceInfo ii = new InstanceInfo();
                ii.setId((String) instancesViol.get(i));
                ii.setViolated(true);
                instances.add(ii);
                if (ii.getId().equals(selected)) {
                    ind = i;
                }
            }
            instancesList.setModel(new ListModel(instances));
            instancesList.setSelectedIndex(ind);
            search.setEnabled(false);
        } else if (action.equals("showAll")) {
            showV = false;
            showA = true;
            searching = false;
            instances = new Vector();
            int ind = -1;
            for (int i = 0; i < instancesAll.size(); i++) {
                InstanceInfo ii = new InstanceInfo();
                ii.setId((String) instancesAll.get(i));
                if (instancesViol.contains(instancesAll.get(i))) {
                    ii.setViolated(true);
                } else {
                    ii.setViolated(false);
                }
                instances.add(ii);
                if (ii.getId().equals(selected)) {
                    ind = i;
                }

            }
            instancesList.setModel(new ListModel(instances));
            instancesList.setSelectedIndex(ind);
            search.setEnabled(false);

        } else if (action.equals("search")) {
            //search.setEnabled(true);
            showV = false;
            showA = false;
            searching = true;

            int ind = -1;
            instances = new Vector();
            for (int i = 0; i < instancesAll.size(); i++) {
                if (((String) instancesAll.get(i)).startsWith(searchField.getText())) {
                    InstanceInfo ii = new InstanceInfo();
                    ii.setId((String) instancesAll.get(i));
                    if (instancesViol.contains(instancesAll.get(i))) {
                        ii.setViolated(true);
                    } else {
                        ii.setViolated(false);
                    }
                    instances.add(ii);
                    if (ii.getId().equals(selected)) {
                        ind = i;
                    }
                }
            }
            instancesList.setModel(new ListModel(instances));

            instancesList.setSelectedIndex(ind);


        } else if (action.equals("searching")) {
            showV = false;
            showA = false;
            searching = true;

        }
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        if (arg0.getSource() instanceof JTextField) {
            searchRadioButton.setSelected(true);
            showV = false;
            showA = false;
            searching = true;
        }

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        if (tpInstances != null) {
            if (tpInstances.getSelected() != null) {
                if (tpInstances.getSelected().equals(roundedPanelHealth)) {
                    selectedTab = HEALTH_TAB;
                    //	}else if (tpInstances.getSelected().equals(roundedPanelMon)){
                    //	selectedTab= MONITOR_TAB;
                } else {
                    selectedTab = DIAGNOSTICS_TAB;
                }
            }
        }

    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        if (tpInstances != null) {
            if (tpInstances.getSelected() != null) {
                if (tpInstances.getSelected().equals(roundedPanelHealth)) {
                    selectedTab = HEALTH_TAB;


                } else {
                    selectedTab = DIAGNOSTICS_TAB;
                }
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        if (tpInstances != null) {
            if (tpInstances.getSelected() != null) {
                if (tpInstances.getSelected().equals(roundedPanelHealth)) {
                    selectedTab = HEALTH_TAB;


                } else {
                    selectedTab = DIAGNOSTICS_TAB;
                }
            }
        }

    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        if (arg0.getSource() instanceof JScrollBar) {
            value = outputContainer.getHorizontalScrollBar().getValue();
        }
        if (tpInstances != null) {
            if (tpInstances.getSelected() != null) {
                if (tpInstances.getSelected().equals(roundedPanelHealth)) {
                    selectedTab = HEALTH_TAB;


                } else {
                    selectedTab = DIAGNOSTICS_TAB;
                }
            }
        }

    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        int ind = -1;
        instances = new Vector();
        for (int i = 0; i < instancesAll.size(); i++) {
            if (((String) instancesAll.get(i)).startsWith(searchField.getText())) {
                InstanceInfo ii = new InstanceInfo();
                ii.setId((String) instancesAll.get(i));
                if (instancesViol.contains(instancesAll.get(i))) {
                    ii.setViolated(true);
                } else {
                    ii.setViolated(false);
                }
                instances.add(ii);
                if (ii.getId().equals(selected)) {
                    ind = i;
                }
            }
        }
        instancesList.setModel(new ListModel(instances));
        instancesList.setSelectedIndex(ind);
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void finalize() {
        try {
            Set keys = handles.keySet();
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                ((SessionHandle<Object, Map<String, Object>, String, Object, Object>) handles.get(it.next())).close();
            }

            startAction.setEnabled(true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ProxySimulator extends Thread {

        @SuppressWarnings("unused")
        private final Socket socket;
        private final OutputStream outStream;
        private final InputStream inStream;

        public ProxySimulator(final Socket socket, final OutputStream outStream, final InputStream inStream) {
            super("Proxy Simulator");
            setDaemon(true);
            this.outStream = outStream;
            this.inStream = inStream;
            this.socket = socket;
            types = new Hashtable();
        }

        private void processXml(String input) {

            try {

                // Create XES document

                SAXBuilder sb = new SAXBuilder();
                DOMOutputter outputter = new DOMOutputter();
                //String input = buffer.toString();

                Document xesDocument = sb.build(new StringReader(input));
                XMLOutputter xmlOutput = new XMLOutputter();
                xmlOutput.setFormat(Format.getPrettyFormat());
                String xmlString = xmlOutput.outputString(xesDocument);

                // View Pretty Print
                System.out.println(xmlString);

                // Code from MoBuConLTL write model to document

                File modelFile = File.createTempFile("model", ".xml");
                modelFile.deleteOnExit();
                xmlOutput.output(xesDocument, new FileWriter(modelFile));

                AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(modelFile.getAbsolutePath());

                assignmentModel = broker.readAssignment();

                if (assignmentModel != null) {
                    modelReceived = true;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }


            System.out.println("----- Model Created ------");
        }

        @Override
        public void run() {
            System.out.println("run()");
            BufferedReader in = null;
            HashMap properties = new HashMap();
            String referenceXML = null;
            PrintWriter out = null;
            String[] completeSetEvents = null;
            Hashtable handles = new Hashtable();
            partialTraces = new Hashtable();
            traceHashMap =   new HashMap<>();
            in = new BufferedReader(new InputStreamReader(inStream));
            out = new PrintWriter(outStream, true);
            String letto = null;
            SessionHandle handle = null;

            File message = null;

            try {

                referenceXML = in.readLine();
                while (!referenceXML.contains("</model>")) {
                    referenceXML = referenceXML + in.readLine();
                }

                if (!modelReceived) {
                    processXml(referenceXML);
                }

                Vector weights = new Vector();
                letto = in.readLine();
                while (!letto.equals("END_WEIGHTS")) {
                    weights.add(letto);
                    letto = in.readLine();
                }
                int timeWindowSize = new Integer(in.readLine());

                properties.put("weights", weights);
                properties.put("timeWindow", timeWindowSize);

                completeSetEvents = new String[0];
                message = File.createTempFile("message", ".xml");
                message.deleteOnExit();
                PrintWriter printmessage = new PrintWriter(new FileWriter(message));
                letto = in.readLine();
                System.out.println(letto);

                while (!letto.contains("</org.deckfour.xes.model.impl.XTraceImpl>")) {
                    letto = in.readLine();
                    System.out.println(letto);
                    printmessage.println(letto);
                }
                printmessage.println(letto);
                trace = (XTrace) osxmlConverter.fromXML(letto);
                traceHashMap.put(letto, trace);
                printmessage.flush();

            } catch (IOException e1) {
                e1.printStackTrace();
            }


            while (message != null && trace != null) {
                try {

                    String modelID = assignmentModel.getName();
                    String processInstanceID = XConceptExtension.instance().extractName(trace);
                    types.put(processInstanceID, modelID);

                    if (!handles.containsKey(processInstanceID)) {
                        handle = SessionHandle.create("localhost", 1202, DeclareMonitorQuery.INSTANCE, properties);
                        handle.setModel(DeclareLanguage.INSTANCE, referenceXML);
                        handles.put(processInstanceID, handle);
                    } else {
                        handle = (SessionHandle) handles.get(processInstanceID);
                    }

                    outputContainer.getHorizontalScrollBar().setValue(value);

                    XTrace partialTrace;
                    System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                            "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");

                    String eventName = "";
                    long timestamp = 0;
                    String piID = "";

                    long old = -1;
                    XEvent completeEvent = new XEventImpl();

                    for (XEvent e : trace) {
                        XAttributeMap eventAttributeMap = e.getAttributes();
                        completeEvent = e;
                        //String event = XConceptExtension.instance().extractName(e);
                        eventName = XConceptExtension.instance().extractName(e).replaceAll(" ", "_");
                        //String transitionType = XLifecycleExtension.instance().extractTransition(e);
                        String ts = eventAttributeMap.get(XLifecycleExtension.KEY_TRANSITION).getAttributes().toString();
                        long current = XTimeExtension.instance().extractTimestamp(e).getTime();
                        if (current <= old) {
                            old = old + 1;
                        } else {
                            old = current;
                        }
                        timestamp = old;
                        piID = XConceptExtension.instance().extractName(trace);
                    }
                    if (partialTraces.containsKey(piID)) {
                        partialTrace = (XTrace) partialTraces.get(piID);
                    } else {
                        partialTrace = new XTraceImpl(new XAttributeMapImpl());
                        if (!instancesAll.contains(piID)) {
                            instancesAll.add(piID);
                        }
                    }

                    trace.get(0);

                    converter = new FluentsConverter(new NoGroupingStrategy());

                    boolean done = false;
                    if (eventName.equals("complete")) {
                        done = true;
                    }

                    //XTimeExtension.instance().assignTimestamp(completeEvent, timestamp);
                    //XConceptExtension.instance().assignName(completeEvent, eventName.toLowerCase());
                    handle.addEvent(completeEvent);
                    partialTrace.add(completeEvent);
                    partialTraces.put(piID, partialTrace);

                    // TODO: THIS IS A HACK BECAUSE WE DON'T GET THE ATTRIBUTES OF THE TRACE WITH THE TRACE
                    handle.addProperty("serializedTrace", letto);
                    XLog emptyLog = XFactoryRegistry.instance().currentDefault().createLog();
                    emptyLog.add(trace);

                    // Send trace
                    System.out.println("Start handle.simple, piID: "+  piID + " done: " + done );
                    ResponseSet<Map<String, Object>> result = handle.simple(piID, emptyLog, done);
                    for (String provider : result) {
                        for (Map<String, Object> r : result.getResponses(provider)) {
                            analysis = r;
                        }
                    }
                    boolean viol = false;
                    double health = ((Double) analysis.get("health")).doubleValue();
                    healthHash.put(timestamp, health);
                    if (health < 1) {
                        if (!instancesViol.contains(piID)) {
                            instancesViol.add(piID);
                        }
                    } else {
                        if (instancesViol.contains(piID)) {
                            instancesViol.remove(piID);
                        }
                    }
                    if (showA) {
                        instances = new Vector();
                        for (int i = 0; i < instancesAll.size(); i++) {
                            InstanceInfo ii = new InstanceInfo();
                            ii.setId((String) instancesAll.get(i));
                            if (instancesViol.contains(instancesAll.get(i))) {
                                ii.setViolated(true);
                            } else {
                                ii.setViolated(false);
                            }
                            instances.add(ii);
                        }
                    } else if (showV) {
                        instances = new Vector();
                        for (int i = 0; i < instancesViol.size(); i++) {
                            InstanceInfo ii = new InstanceInfo();
                            ii.setId((String) instancesViol.get(i));
                            ii.setViolated(true);
                            instances.add(ii);
                        }
                    } else if (searching) {
                        instances = new Vector();
                        for (int i = 0; i < instancesAll.size(); i++) {
                            if (((String) instancesAll.get(i)).startsWith(searchField.getText())) {
                                InstanceInfo ii = new InstanceInfo();
                                ii.setId((String) instancesAll.get(i));
                                if (instancesViol.contains(instancesAll.get(i))) {
                                    ii.setViolated(true);
                                } else {
                                    ii.setViolated(false);
                                }
                                instances.add(ii);
                            }
                        }
                    }
                    int[] selectedIndices = instancesList.getSelectedIndices();
                    int index = -1;
                    if (selectedIndices.length > 0) {
                        index = selectedIndices[0];
                    }
                    instancesList.clearSelection();
                    instancesList.setModel(new ListModel(instances));
                    instancesList.setSelectedIndex(index);
                    out.println(((Math.rint(health) * Math.pow(10, 2)) / Math.pow(10, 2)));
                    if (tpInstances != null) {
                        if (tpInstances.getSelected() != null) {
                            if (tpInstances.getSelected().equals(roundedPanelHealth)) {
                                selectedTab = HEALTH_TAB;
                            } else {
                                selectedTab = DIAGNOSTICS_TAB;
                            }
                        }
                    }
                    Vector vv = (Vector) violationsModels.get(piID);
                    String diags = "Observed   " + eventName;
                    int oldSize;
                    if (vv == null) {
                        oldSize = 0;
                    } else {
                        oldSize = vv.size();
                    }
                    updateOutput(piID, eventName, completeSetEvents);
                    outputContainer.getHorizontalScrollBar().setValue(value);
                    vv = (Vector) violationsModels.get(piID);
                    int newSize;
                    if (vv == null) {
                        newSize = 0;
                    } else {
                        newSize = vv.size();
                    }
                    if (oldSize == newSize) {
                        out.println("");
                    } else {
                        String positive = (String) analysis.get("positive");
                        if (!positive.isEmpty()) {
                            if (positive != null) {
                                String[] positivesForSize = positive.split(",");
                                if (positivesForSize.length == 1) {
                                    diags = diags + ",   while for this reference model expecting   "
                                            + positivesForSize[0];
                                } else {
                                    String posList = positive.replace(",", ", ");
                                    int indCo = posList.lastIndexOf(",");
                                    String sub = posList.substring(indCo);
                                    posList = posList
                                            .replaceFirst(sub, sub.replaceFirst(", ", ", or "));
                                    diags = diags + ",   while for this reference model expecting   "
                                            + posList;
                                }
                            }
                        }
                        String[] negativeSet = null;
                        String negative = (String) analysis.get("negative");
                        if (!negative.isEmpty()) {
                            if (negative != null) {
                                negativeSet = negative.split(",");
                            }
                        }
                        String[] positiveSet = positive.split(",");
                        Vector ve = new Vector();
                        for (int i = 0; i < positiveSet.length; i++) {
                            ve.add(positiveSet[i]);
                        }
                        if (negativeSet != null) {
                            if (negativeSet.length >= 1) {
                                Vector vecCom = new Vector();
                                for (int g = 0; g < completeSetEvents.length; g++) {
                                    vecCom.add(completeSetEvents[g]);
                                }
                                Vector vecNeg = new Vector();
                                for (int g = 0; g < negativeSet.length; g++) {
                                    vecNeg.add(negativeSet[g]);
                                }
                                for (int g = 0; g < completeSetEvents.length; g++) {
                                    if (vecNeg.contains(completeSetEvents[g])) {
                                        vecCom.remove(completeSetEvents[g]);
                                    }
                                }
                                diags = diags + "   (or otherwise everything different from   ";
                                String last = null;
                                if (vecNeg.size() == 1) {
                                    diags = diags + vecNeg.get(0) + ")";
                                } else {
                                    for (int g = 0; g < vecNeg.size(); g++) {
                                        if (!ve.contains(vecNeg.get(g))) {
                                            diags = diags + vecNeg.get(g) + ", ";
                                            last = (String) vecNeg.get(g);
                                        }
                                    }

                                    diags = diags.replace(", " + last + ", ", " and " + last + ")");

                                }
                            }
                        }

                        out.println(diags);

                    }
                    if (selected.equals(piID)) {
                        roundedPanelHealth.removeAll();
                        roundedPanelHealth.add((FluentChartStandardPanel) healthGraphs.get(selected));
                        diagnosticsList.setModel(new ViolationListModel((Vector) violationsModels.get(selected)));
                    }
                    outputContainer.getHorizontalScrollBar().setValue(value);

                } catch (Exception e) {
                    e.printStackTrace();

                }

                try {
                    message.delete();
                    message = File.createTempFile("message", ".xml");
                    message.deleteOnExit();
                    PrintWriter printmessage = new PrintWriter(new FileWriter(message));
                    letto = in.readLine();
                    if (letto == null || letto.isEmpty()) {
                        message = null;
                        trace = null;
                        System.out.println("Connection closed by log streamer");

                    } else {
                        while (!letto.contains("</org.deckfour.xes.model.impl.XTraceImpl>")) {
                            printmessage.println(letto);
                        }
                        printmessage.println(letto);
                        trace = (XTrace) osxmlConverter.fromXML(letto);
                        traceHashMap.put(letto, trace);
                        printmessage.flush();
                        printmessage.close();
                    }
                    outputContainer.getHorizontalScrollBar().setValue(value);
                } catch (IOException e) {
                    message = null;
                    trace = null;
                    handles.clear();
                    e.printStackTrace();
                }
            }
        }

        // We have to actually update the model values in the event thread
        // to make sure that they are consistent. This is a quick operation
        // becuase all we are doing is updating the references to the data arrays.


    }
}


