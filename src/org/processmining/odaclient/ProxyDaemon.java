/************************************************************************/
/* Access/CPN                                                           */
/* Copyright 2010-2011 AIS Group, Eindhoven University of Technology    */
/*                                                                      */
/* This library is free software; you can redistribute it and/or        */
/* modify it under the terms of the GNU Lesser General Public           */
/* License as published by the Free Software Foundation; either         */
/* version 2.1 of the License, or (at your option) any later version.   */
/*                                                                      */
/* This library is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    */
/* Lesser General Public License for more details.                      */
/*                                                                      */
/* You should have received a copy of the GNU Lesser General Public     */
/* License along with this library; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,           */
/* MA  02110-1301  USA                                                  */
/************************************************************************/
package org.processmining.odaclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import org.processmining.odaclient.OnlineDeclareAnalyzerClient.ProxySimulator;

import ltl2aut.formula.conjunction.BlockingQueue;


public class ProxyDaemon extends Thread {
	private ServerSocket s;

	private static ProxyDaemon defaultInstance;
	private static OnlineDeclareAnalyzerClient client;

	public ProxyDaemon(final int port, OnlineDeclareAnalyzerClient client) throws Exception {
		super("Proxy Simulator (port = " + port + ")");
		setDaemon(true);
		simulators = new BlockingQueue<ProxySimulator>();
		this.client = client;
		try {
			s = new ServerSocket(4444);
		} catch (final Exception e) {
			throw new Exception(
					"Could not start Proxy Daemon. Most likely CPN Tools is running. Try shutting down CPN Tools (including any cpnmld.* processes) and try again.");
		}
	}

	private final BlockingQueue<ProxySimulator> simulators;

	/**
	 * Get the next ProxySimulator connecting
	 * 
	 * @return
	 */
	public ProxySimulator getNext() {
		return simulators.get();
	}

	/**
	 * Clear the queue of ProxySimulators. Only ensures that at some point after calling the queue
	 * will be empty. Suggested use: clear the queue, display a message to the user to connect,
	 * getNext to (most likely) get the one the user connected.
	 */
	public void clear() {
		while (simulators.size() > 0) {
			simulators.get();
		}
	}
	
	

	@Override
	public void run() {
		
		while (true) {
			try {
				final Socket socket = s.accept();
				final InputStream inStream = socket.getInputStream();
				final OutputStream outStream = socket.getOutputStream();
				socket.setTcpNoDelay(true);
				socket.setReceiveBufferSize(1);
				ProxySimulator xydataset = client.new ProxySimulator(socket, outStream, inStream);
				xydataset.start();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
