package org.processmining.odaclient;

import java.util.Vector;

import javax.swing.AbstractListModel;

public class WeightsListModel extends AbstractListModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4965852306182681132L;
	protected Vector list;

	public WeightsListModel(Vector list) {
		this.list = list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	@Override
	public Object getElementAt(int index) {
		try {
			return list.get(index);
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getSize()
	 */
	@Override
	public int getSize() {
		return list.size();
	}

}
