package org.processmining.odaclient;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.fluxicon.slickerbox.factory.SlickerFactory;

public class WeightsRenderer implements ListCellRenderer {

	private final JLabel idLabel;
	private final JComboBox combo;
	private final JPanel panel;

	public WeightsRenderer(double longest) {
		SlickerFactory sf = SlickerFactory.instance();
		panel = new JPanel();
		//panel.setBackground(Color.black);
		idLabel = sf.createLabel("");
		combo = sf.createComboBox(new Integer[]{1,2,3,4,5,6,7,8,9,10});
		panel.setLayout(new TableLayout(new double[][] { { 6.6 *longest, 50 }, { 20 } }));
		//panel.add(bef, BorderLayout.NORTH);
		panel.add(idLabel, "0,0");
		panel.add(combo, "1,0");
		//panel.setBorder(new EmptyBorder(3,2,3,0));
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// TODO Auto-generated method stub
		
		idLabel.setText((String)value);
		
		//panel.revalidate();
		return panel;
	}

}
