package org.processmining.onlinedeclareanalyzer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.processmining.onlinedeclareanalyzer.data.DataSnapshotListener;
import org.processmining.onlinedeclareanalyzer.data.TraceReaderAndReplayer;
import org.processmining.onlinedeclareanalyzer.replayers.Absence2Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.Absence3Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.AbsenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.AlternatePrecedenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.AlternateResponseAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.ChainPrecedenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.ChainResponseAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.DeclareReplayer;
import org.processmining.onlinedeclareanalyzer.replayers.Exactly1Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.Exactly2Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.Existence2Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.Existence3Analyzer;
import org.processmining.onlinedeclareanalyzer.replayers.ExistenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.InitAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.NotChainPrecedenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.NotChainResponseAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.NotPrecedenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.NotResponseAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.PrecedenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.RespondedExistenceAnalyzer;
import org.processmining.onlinedeclareanalyzer.replayers.ResponseAnalyzer;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import org.processmining.plugins.declareminer.visualizing.ActivityDefinition;
import org.processmining.plugins.declareminer.visualizing.AssignmentModel;
import org.processmining.plugins.declareminer.visualizing.AssignmentViewBroker;
import org.processmining.plugins.declareminer.visualizing.ConstraintDefinition;
import org.processmining.plugins.declareminer.visualizing.Parameter;
import org.processmining.plugins.declareminer.visualizing.XMLBrokerFactory;

class OnlineDeclareAnalyser {

	final static int LISTENING_PORT = 4444;

	HashMap<String, DeclareReplayer> replayers = new HashMap<String, DeclareReplayer>();
	XAttributeMap eventAttributeMap;
	HashSet<String> templates = new HashSet<String>();

	List<List<String>> dispositionsresponse = new ArrayList<List<String>>();
	List<List<String>> dispositionsprecedence = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotresponse = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotprecedence = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotchainresponse = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotchainprecedence = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotrespondedexistence = new ArrayList<List<String>>();
	List<List<String>> dispositionssuccession = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotsuccession = new ArrayList<List<String>>();
	List<List<String>> dispositionschainresponse = new ArrayList<List<String>>();
	List<List<String>> dispositionschainprecedence = new ArrayList<List<String>>();
	List<List<String>> dispositionschainsuccession = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotchainsuccession = new ArrayList<List<String>>();
	List<List<String>> dispositionsalternateresponse = new ArrayList<List<String>>();
	List<List<String>> dispositionsalternateprecedence = new ArrayList<List<String>>();
	List<List<String>> dispositionsalternatesuccession = new ArrayList<List<String>>();
	List<List<String>> dispositionsrespondedexistence = new ArrayList<List<String>>();
	List<List<String>> dispositionscoexistence = new ArrayList<List<String>>();
	List<List<String>> dispositionsnotcoexistence = new ArrayList<List<String>>();
	List<List<String>> dispositionsexclusivechoice = new ArrayList<List<String>>();
	List<List<String>> dispositionschoice = new ArrayList<List<String>>();

	List<List<String>> invdispositionscoexistence = new ArrayList<List<String>>();
	List<List<String>> invdispositionsnotcoexistence = new ArrayList<List<String>>();
	List<List<String>> invdispositionsexclusivechoice = new ArrayList<List<String>>();
	List<List<String>> invdispositionschoice = new ArrayList<List<String>>();

	List<List<String>> dispositionsinit = new ArrayList<List<String>>();
	List<List<String>> dispositionsabsence = new ArrayList<List<String>>();
	List<List<String>> dispositionsabsence2 = new ArrayList<List<String>>();
	List<List<String>> dispositionsabsence3 = new ArrayList<List<String>>();
	List<List<String>> dispositionsexactly1 = new ArrayList<List<String>>();
	List<List<String>> dispositionsexactly2 = new ArrayList<List<String>>();
	List<List<String>> dispositionsexistence = new ArrayList<List<String>>();
	List<List<String>> dispositionsexistence2 = new ArrayList<List<String>>();
	List<List<String>> dispositionsexistence3 = new ArrayList<List<String>>();
	List<List<String>> dispositionsstronginit = new ArrayList<List<String>>();

	Set<String> candidateActivations = new HashSet<String>();

	AssignmentModel assignmentModel = null;
	boolean data = false;

	protected OSXMLConverter converter = new OSXMLConverter();

    AnalysisResult analysis;

	public static OnlineDeclareAnalyser instence;
	private static boolean modelReceived;

	public static void main(String args[]) {

		if (OnlineDeclareAnalyser.instence == null) {
			OnlineDeclareAnalyser.instence = new OnlineDeclareAnalyser();
		}
		OnlineDeclareAnalyser.instence.run();
	}

	private void run() {

		ServerSocket echoServer = null;
		String line;
		DataInputStream is;
		PrintStream os;
		PrintWriter writeOnTheSocket;
		Socket clientSocket = null;
		modelReceived = false;

		//OSXMLConverter converter;
		try {
			echoServer = new ServerSocket(LISTENING_PORT);
			log("Server Started");
		} catch (IOException e) {
			System.out.println(e);
		}

		try {
			clientSocket = echoServer.accept();
			is = new DataInputStream(clientSocket.getInputStream());
			os = new PrintStream(clientSocket.getOutputStream());
			writeOnTheSocket = new PrintWriter(clientSocket.getOutputStream(),true);

			Hashtable replays = new Hashtable();
            HashMap<String,XTrace> traceHashMap= new HashMap<>();

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while (true) {
				//line = is.readLine();
				//System.out.println(line);

				// Create model
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				try {
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

					//String line = null;
					StringBuffer buffer = new StringBuffer();
					while ((line = br.readLine()) != null && !modelReceived) { // 'in' is a BufferedReader on socket input stream
						buffer.append(line);
						if (line.indexOf("</model>") > -1) { // message close tag found
							processXml(buffer); // send buffer for processing							
							writeOnTheSocket.println("Model Received");
                            analysis = new AnalysisResult();
							break;
						}
					}
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
				
				// Reade traces
				try {
					String streamLine;
					XTrace t;
					while ( (streamLine = br.readLine()) != null) {
						if(!streamLine.isEmpty()){
                            t = (XTrace) converter.fromXML(streamLine);
                            System.out.println("Trace added  " + traceHashMap.size());
                            traceHashMap.put(streamLine,t);
                            processTrace(t);
                        }
					}
                    System.out.println("Size of traces map " + traceHashMap.size());

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (IOException e)

		{
			System.out.println(e);
		}
	}
	
	
	private void processTrace(XTrace t){
		System.out.println("processTrace()");



        System.out.println("STEP 2");
		System.out.println("=============================================");
        TraceReaderAndReplayer replay = null;
        DataSnapshotListener listener = null;
        if(data) {
            try {
                replay = new TraceReaderAndReplayer(t);
                listener = new DataSnapshotListener(replay.getDataTypes(), replay.getActivityLabels());
                replay.addReplayerListener(listener);
                replay.replayLog(candidateActivations);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        String traceId = XConceptExtension.instance().extractName(t);
        int i = 0;
        for (XEvent e : t) {
            String event = XConceptExtension.instance().extractName(e);
            String transitionType = XLifecycleExtension.instance().extractTransition(e);
            if (transitionType == null) {
                event += "-complete";
            } else {
                event += "-" + transitionType;
            }
            System.out.println("Before replayer.process event: " + event.toLowerCase() + " transitionType: " + transitionType + " traceid: " + traceId);
            for(DeclareReplayer replayer : replayers.values()){
                replayer.process(i, event.toLowerCase(), t, traceId, listener);
            }
            i++;
        }

        System.out.println("STEP 3");
        System.out.println("=============================================");

        XAttributeMap tracetAttributeMap = t.getAttributes();
        traceId = tracetAttributeMap.get(XConceptExtension.KEY_NAME).toString();
        Hashtable output = new Hashtable();
        for (ConstraintDefinition cd : assignmentModel.getConstraintDefinitions()) {
            ArrayList<String> disposs = new ArrayList<String>();
            for (Parameter p : cd.getParameters()) {
                for (ActivityDefinition b : cd.getBranches(p)) {
                    String bName = b.getName().toLowerCase();

                    if((!bName.contains("-assign")&&!bName.contains("-ate_abort")&&!bName.contains("-suspend")&&!bName.contains("-complete")&&!bName.contains("-autoskip")&&!bName.contains("-manualskip")&&!bName.contains("pi_abort")&&!bName.contains("-reassign")&&!bName.contains("-resume")&&!bName.contains("-schedule")&&!bName.contains("-start")&&!bName.contains("-unknown")&&!bName.contains("-withdraw"))&&(!bName.contains("<center>assign")&&!bName.contains("<center>ate_abort")&&!bName.contains("<center>suspend")&&!bName.contains("<center>complete")&&!bName.contains("<center>autoskip")&&!bName.contains("<center>manualskip")&&!bName.contains("<center>pi_abort")&&!bName.contains("<center>reassign")&&!bName.contains("<center>resume")&&!bName.contains("<center>schedule")&&!bName.contains("<center>start")&&!bName.contains("<center>unknown")&&!bName.contains("<center>withdraw"))){
                        disposs.add(bName + "-complete");
                    }else{
                        disposs.add(bName);
                    }
                }

            }
            String cdName = cd.getName().toLowerCase();
            String param0 = disposs.get(0);
            if(cdName.equals("existence")){
                ExistenceAnalyzer replayer = (ExistenceAnalyzer)replayers.get("existence");
                Set<Integer> violations = replayer.getViolations(traceId, param0);
                Set<Integer> fulfillments = replayer.getFulfillments(traceId, param0);
                Set<Integer> activations = new HashSet<Integer>();
                activations.addAll(fulfillments);
                activations.addAll(violations);
                analysis.addResult(t, cd, activations, violations, fulfillments);
            }
        }

        //System.out.println(analysis.toString());
//        output.put("health", analysis.getActivations());
//        output.put("positive", positive);
//        output.put("negative", negative);
//        output.put("fluents", stringone);
		
	}

	private void processXml(StringBuffer buffer) {
		log("processXml");
		try {

			// Create XES document

			SAXBuilder sb = new SAXBuilder();
			DOMOutputter outputter = new DOMOutputter();
			String input = buffer.toString();
			log("Input = " + input);
			Document xesDocument = sb.build(new StringReader(input));
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			String xmlString = xmlOutput.outputString(xesDocument);

			// View Pretty Print
			System.out.println(xmlString);

			// Code from MoBuConLTL write model to document

			File modelFile = File.createTempFile("model", ".xml");
			modelFile.deleteOnExit();
			xmlOutput.output(xesDocument, new FileWriter(modelFile));

			AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(modelFile.getAbsolutePath());

			assignmentModel = broker.readAssignment();
			
			if(assignmentModel != null ){
				modelReceived = true;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		prepareDataStructure();
		
		//log("----- Model Created ------");
		System.out.println("----- Model Created ------");
	}

	private void prepareDataStructure() {
		log("prepareDataStructure ");
		for (ConstraintDefinition cd : assignmentModel.getConstraintDefinitions()) {
			templates.add(cd.getName());
			String temp = cd.getName();
			ArrayList<String> disposs = new ArrayList<String>();
			for (Parameter p : cd.getParameters()) {
				for (ActivityDefinition b : cd.getBranches(p)) {
					if ((!b.getName().contains("-assign") && !b.getName().contains("-ate_abort")
							&& !b.getName().contains("-suspend") && !b.getName().contains("-complete")
							&& !b.getName().contains("-autoskip") && !b.getName().contains("-manualskip")
							&& !b.getName().contains("pi_abort") && !b.getName().contains("-reassign")
							&& !b.getName().contains("-resume") && !b.getName().contains("-schedule")
							&& !b.getName().contains("-start") && !b.getName().contains("-unknown")
							&& !b.getName().contains("-withdraw"))
							&& (!b.getName().contains("<center>assign") && !b.getName().contains("<center>ate_abort")
									&& !b.getName().contains("<center>suspend")
									&& !b.getName().contains("<center>complete")
									&& !b.getName().contains("<center>autoskip")
									&& !b.getName().contains("<center>manualskip")
									&& !b.getName().contains("<center>pi_abort")
									&& !b.getName().contains("<center>reassign")
									&& !b.getName().contains("<center>resume")
									&& !b.getName().contains("<center>schedule")
									&& !b.getName().contains("<center>start")
									&& !b.getName().contains("<center>unknown")
									&& !b.getName().contains("<center>withdraw"))) {
						disposs.add(b.getName().toLowerCase() + "-complete");
						if (cd.getCondition().toString().contains("[")) {
							candidateActivations.add(b.getName().toLowerCase() + "-complete");
							data = true;
						}
					} else {
						disposs.add(b.getName().toLowerCase());
						if (cd.getCondition().toString().contains("[")) {
							candidateActivations.add(b.getName().toLowerCase());
							data = true;
						}
					}
				}
			}

			String tempLower = temp.toLowerCase();
			if (tempLower.equals("response")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsresponse.add(disposs);
			} else if (tempLower.equals("not response")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotresponse.add(disposs);
			} else if (tempLower.equals("exclusive choice")) {
				disposs.add("responded existence");
				disposs.add(cd.getCondition().toString());
				ArrayList<String> inverse = new ArrayList<String>();
				inverse.add(disposs.get(1));
				inverse.add(disposs.get(0));
				inverse.add(disposs.get(2));
				inverse.add(disposs.get(3));
				dispositionsexclusivechoice.add(disposs);
				invdispositionsexclusivechoice.add(inverse);
			} else if (tempLower.equals("choice")) {
				disposs.add("responded existence");
				disposs.add(cd.getCondition().toString());
				ArrayList<String> inverse = new ArrayList<String>();
				inverse.add(disposs.get(1));
				inverse.add(disposs.get(0));
				inverse.add(disposs.get(2));
				inverse.add(disposs.get(3));
				dispositionschoice.add(disposs);
				invdispositionschoice.add(inverse);
			} else if (tempLower.equals("precedence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsprecedence.add(disposs);
			} else if (tempLower.equals("not precedence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotprecedence.add(disposs);
			} else if (tempLower.equals("init")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsinit.add(disposs);
			} else if (tempLower.equals("strong init")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsinit.add(disposs);
			} else if (tempLower.equals("absence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsabsence.add(disposs);
			} else if (tempLower.equals("absence2")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsabsence2.add(disposs);
			} else if (tempLower.equals("absence3")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsabsence3.add(disposs);
			} else if (tempLower.equals("existence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsexistence.add(disposs);
			} else if (tempLower.equals("existence2")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsexistence2.add(disposs);
			} else if (tempLower.equals("existence3")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsexistence3.add(disposs);
			} else if (tempLower.equals("exactly1")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsexactly1.add(disposs);
			} else if (tempLower.equals("exactly2")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsexactly2.add(disposs);
			} else if (tempLower.equals("responded existence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsrespondedexistence.add(disposs);
			} else if (tempLower.equals("not responded existence")) {
				disposs.add("responded existence");
				disposs.add(cd.getCondition().toString());
				dispositionsnotrespondedexistence.add(disposs);
			} else if (tempLower.equals("co-existence")) {
				disposs.add("responded existence");
				disposs.add(cd.getCondition().toString());
				ArrayList<String> inverse = new ArrayList<String>();
				inverse.add(disposs.get(1));
				inverse.add(disposs.get(0));
				inverse.add(disposs.get(2));
				inverse.add(disposs.get(3));
				dispositionscoexistence.add(disposs);
				invdispositionscoexistence.add(inverse);
			} else if (tempLower.equals("not co-existence")) {
				disposs.add("responded existence");
				disposs.add(cd.getCondition().toString());
				//.add(disposs);
				ArrayList<String> inverse = new ArrayList<String>();
				inverse.add(disposs.get(1));
				inverse.add(disposs.get(0));
				inverse.add(disposs.get(2));
				inverse.add(disposs.get(3));
				dispositionsnotcoexistence.add(disposs);
				invdispositionsnotcoexistence.add(inverse);
			} else if (tempLower.equals("succession")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionssuccession.add(disposs);
			} else if (tempLower.equals("not succession")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotsuccession.add(disposs);
			} else if (tempLower.equals("chain response")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionschainresponse.add(disposs);
			} else if (tempLower.equals("not chain response")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotchainresponse.add(disposs);
			} else if (tempLower.equals("chain precedence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionschainprecedence.add(disposs);
			} else if (tempLower.equals("not chain precedence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotchainprecedence.add(disposs);
			} else if (tempLower.equals("chain succession")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionschainsuccession.add(disposs);
			} else if (tempLower.equals("not chain succession")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsnotchainsuccession.add(disposs);
			} else if (tempLower.equals("alternate response")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsalternateresponse.add(disposs);
			} else if (tempLower.equals("alternate precedence")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsalternateprecedence.add(disposs);
			} else if (tempLower.equals("alternate succession")) {
				disposs.add(cd.getDisplay());
				disposs.add(cd.getCondition().toString());
				dispositionsalternatesuccession.add(disposs);
			}
			//}
		}

		for (String temp : templates) {
			String tempLower = temp.toLowerCase();
			if (tempLower.equals("response")) {
				ResponseAnalyzer replayer = new ResponseAnalyzer(dispositionsresponse);
				replayers.put("response", replayer);
			} else if (tempLower.equals("not response")) {
				NotResponseAnalyzer replayer = new NotResponseAnalyzer(dispositionsnotresponse);
				replayers.put("not response", replayer);
			} else if (tempLower.equals("precedence")) {
				PrecedenceAnalyzer replayer = new PrecedenceAnalyzer(dispositionsprecedence);
				replayers.put("precedence", replayer);
			} else if (tempLower.equals("not precedence")) {
				NotPrecedenceAnalyzer replayer = new NotPrecedenceAnalyzer(dispositionsnotprecedence);
				replayers.put("not precedence", replayer);
			} else if (tempLower.equals("exclusive choice")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsexclusivechoice);
				replayers.put("exclusive choice", replayer);
				RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionsexclusivechoice);
				replayers.put("inv exclusive choice", replayerinv);
			} else if (tempLower.equals("choice")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionschoice);
				replayers.put("choice", replayer);
				RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionschoice);
				replayers.put("inv choice", replayerinv);
			} else if (tempLower.equals("init")) {
				InitAnalyzer replayer = new InitAnalyzer(dispositionsinit);
				replayers.put("init", replayer);
			} else if (tempLower.equals("strong init")) {
				InitAnalyzer replayer = new InitAnalyzer(dispositionsstronginit);
				replayers.put("strong init", replayer);
			} else if (tempLower.equals("absence")) {
				AbsenceAnalyzer replayer = new AbsenceAnalyzer(dispositionsabsence);
				replayers.put("absence", replayer);
			} else if (tempLower.equals("absence2")) {
				Absence2Analyzer replayer = new Absence2Analyzer(dispositionsabsence2);
				replayers.put("absence2", replayer);
			} else if (tempLower.equals("absence3")) {
				Absence3Analyzer replayer = new Absence3Analyzer(dispositionsabsence3);
				replayers.put("absence3", replayer);
			} else if (tempLower.equals("existence")) {
				ExistenceAnalyzer replayer = new ExistenceAnalyzer(dispositionsexistence);
				replayers.put("existence", replayer);
			} else if (tempLower.equals("existence2")) {
				Existence2Analyzer replayer = new Existence2Analyzer(dispositionsexistence2);
				replayers.put("existence2", replayer);
			} else if (tempLower.equals("existence3")) {
				Existence3Analyzer replayer = new Existence3Analyzer(dispositionsexistence3);
				replayers.put("existence3", replayer);
			} else if (tempLower.equals("exactly1")) {
				Exactly1Analyzer replayer = new Exactly1Analyzer(dispositionsexactly1);
				replayers.put("exactly1", replayer);
			} else if (tempLower.equals("exactly2")) {
				Exactly2Analyzer replayer = new Exactly2Analyzer(dispositionsexactly2);
				replayers.put("exactly2", replayer);
			} else if (tempLower.equals("responded existence")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsrespondedexistence);
				replayers.put("responded existence", replayer);
			} else if (tempLower.equals("not responded existence")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsnotrespondedexistence);
				replayers.put("not responded existence", replayer);
			} else if (tempLower.equals("co-existence")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionscoexistence);
				replayers.put("co-existence", replayer);
				RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionscoexistence);
				replayers.put("inv co-existence", replayerinv);
			} else if (tempLower.equals("not co-existence")) {
				RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsnotcoexistence);
				replayers.put("not co-existence", replayer);
				RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionsnotcoexistence);
				replayers.put("inv not co-existence", replayerinv);
			} else if (tempLower.equals("succession")) {
				ResponseAnalyzer replayerResponse = new ResponseAnalyzer(dispositionssuccession);
				replayers.put("succession r", replayerResponse);
				PrecedenceAnalyzer replayerPrecedence = new PrecedenceAnalyzer(dispositionssuccession);
				replayers.put("succession p", replayerPrecedence);
			} else if (tempLower.equals("not succession")) {
				NotResponseAnalyzer replayerNotResponse = new NotResponseAnalyzer(dispositionsnotsuccession);
				replayers.put("not succession r", replayerNotResponse);
				NotPrecedenceAnalyzer replayerNotPrecedence = new NotPrecedenceAnalyzer(dispositionsnotsuccession);
				replayers.put("not succession p", replayerNotPrecedence);
			} else if (tempLower.equals("chain response")) {
				ChainResponseAnalyzer replayer = new ChainResponseAnalyzer(dispositionschainresponse);
				replayers.put("chain response", replayer);
			} else if (tempLower.equals("not chain response")) {
				NotChainResponseAnalyzer replayer = new NotChainResponseAnalyzer(dispositionsnotchainresponse);
				replayers.put("not chain response", replayer);
			} else if (tempLower.equals("chain precedence")) {
				ChainPrecedenceAnalyzer replayer = new ChainPrecedenceAnalyzer(dispositionschainprecedence);
				replayers.put("chain precedence", replayer);
			} else if (tempLower.equals("not chain precedence")) {
				NotChainPrecedenceAnalyzer replayer = new NotChainPrecedenceAnalyzer(dispositionsnotchainprecedence);
				replayers.put("not chain precedence", replayer);
			} else if (tempLower.equals("chain succession")) {
				ChainResponseAnalyzer replayerResponse = new ChainResponseAnalyzer(dispositionschainsuccession);
				replayers.put("chain succession r", replayerResponse);
				ChainPrecedenceAnalyzer replayerPrecedence = new ChainPrecedenceAnalyzer(dispositionschainsuccession);
				replayers.put("chain succession p", replayerPrecedence);
			} else if (tempLower.equals("alternate response")) {
				AlternateResponseAnalyzer replayer = new AlternateResponseAnalyzer(dispositionsalternateresponse);
				replayers.put("alternate response", replayer);
			} else if (tempLower.equals("alternate precedence")) {
				AlternatePrecedenceAnalyzer replayer = new AlternatePrecedenceAnalyzer(dispositionsalternateprecedence);
				replayers.put("alternate precedence", replayer);
			} else if (tempLower.equals("alternate succession")) {
				AlternateResponseAnalyzer replayerResponse = new AlternateResponseAnalyzer(
						dispositionsalternatesuccession);
				replayers.put("alternate succession r", replayerResponse);
				AlternatePrecedenceAnalyzer replayerPrecedence = new AlternatePrecedenceAnalyzer(
						dispositionsalternatesuccession);
				replayers.put("alternate succession p", replayerPrecedence);
			} else if (tempLower.equals("not chain succession")) {
				NotChainResponseAnalyzer replayerNotResponse = new NotChainResponseAnalyzer(
						dispositionsnotchainsuccession);
				replayers.put("not chain succession r", replayerNotResponse);
				NotChainPrecedenceAnalyzer replayerNotPrecedence = new NotChainPrecedenceAnalyzer(
						dispositionsnotchainsuccession);
				replayers.put("not chain succession p", replayerNotPrecedence);
			}
		}
	}

	public static void log(String str) {
		System.out.println(str);
	}
}