package org.processmining.onlinedeclareanalyzer.data;

/**
 * Created by Ubaier on 02/05/2016.
 */
public class ConstraintName {
    public static final String  RESPONSE = "response";
    public static final String  NOT_RESPONSE = "not response";
    public static final String  PRECEDENCE = "precedence";
    public static final String  NOT_PRECEDENCE = "not precedence";
    public static final String  INIT = "init";
    public static final String  STRONG_INIT = "strong init";
    public static final String  ABSENCE = "absence";
    public static final String  ABSENCE2 = "absence2";
    public static final String  ABSENCE3 = "absence3";
    public static final String  EXISTENCE = "existence";
    public static final String  EXISTENCE2 = "existence2";
    public static final String  EXISTENCE3 = "existence3";
    public static final String  EXACTLY1 = "exactly1";
    public static final String  EXACTLY2 = "exactly2";
    public static final String  RESPONDED_EXISTENCE = "responded existence";
    public static final String  NOT_RESPONDED_EXISTENCE = "not responded existence";
    public static final String  SUCCESSION = "succession";
    public static final String  SUCCESSION_RESPONSE = "succession r";
    public static final String  SUCCESSION_PRECEDENCE = "succession p";
    public static final String  NOT_SUCCESSION = "not succession";
    public static final String  NOT_SUCCESSION_RESPONSE = "not succession r";
    public static final String  NOT_SUCCESSION_PRECEDENCE = "not succession p";
    public static final String  CHAIN_RESPONSE = "chain response";
    public static final String  NOT_CHAIN_RESPONSE = "not chain response";
    public static final String  CHAIN_PRECEDENCE = "chain precedence";
    public static final String  NOT_CHAIN_PRECEDENCE = "not chain precedence";
    public static final String  NOT_CHAIN_SUCCESSION = "not chain succession";
    public static final String  NOT_CHAIN_SUCCESSION_RESPONSE = "not chain succession r";
    public static final String  NOT_CHAIN_SUCCESSION_PRECEDENCE = "not chain succession p";
    public static final String  CHAIN_SUCCESSION = "chain succession";
    public static final String  CHAIN_SUCCESSION_RESPONSE = "chain succession r";
    public static final String  CHAIN_SUCCESSION_PRECEDENCE = "chain succession p";
    public static final String  ALTERNATE_RESPONSE = "alternate response";
    public static final String  ALTERNATE_PRECEDENCE = "alternate precedence";
    public static final String  ALTERNATE_SUCCESSION = "alternate succession";
    public static final String  ALTERNATE_SUCCESSION_RESPONSE = "alternate succession r";
    public static final String  ALTERNATE_SUCCESSION_PRECEDENCE = "alternate succession p";
    public static final String  CO_EXISTENCE = "co-existence";
    public static final String  INV_CO_EXISTENCE = "inv co-existence";
    public static final String  CHOICE = "choice";
    public static final String  INV_CHOICE = "inv choice";
    public static final String  NOT_CO_EXISTENCE = "not co-existence";
    public static final String  INV_NOT_CO_EXISTENCE = "inv not co-existence";
    public static final String  EXCLUSIVE_CHOICE = "exclusive choice";
    public static final String  INV_EXCLUSIVE_CHOICE = "inv exclusive choice";


}
