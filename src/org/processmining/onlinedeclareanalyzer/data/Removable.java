package org.processmining.onlinedeclareanalyzer.data;

/**
 * Created by Ubaier on 10/05/2016.
 */
public class Removable {

    String activityName;
    int cdPosition;
    int eventPosition;

    public Removable(String activityName, int cdPosition, int eventPosition) {
        this.activityName = activityName;
        this.cdPosition = cdPosition;
        this.eventPosition = eventPosition;
    }

    public String getActivityName() {
        return activityName;
    }

    public int getCdPosition() {
        return cdPosition;
    }

    public int getEventPosition() {
        return eventPosition;
    }
}
