package org.processmining.onlinedeclareanalyzer.data;

/**
 * State Object
 * Created by Ubaier on 04/02/2016.
 */

import org.apache.commons.lang3.StringUtils;
import org.processmining.framework.util.Pair;
import org.processmining.onlinedeclareanalyzer.replayers.*;
import org.processmining.plugins.declare.visualizing.ActivityDefinition;
import org.processmining.plugins.declare.visualizing.AssignmentModel;
import org.processmining.plugins.declare.visualizing.ConstraintDefinition;
import org.processmining.plugins.declare.visualizing.Parameter;
import org.processmining.plugins.declareminer.ExecutableAutomaton;
import org.processmining.utils.EquationBuilder;

import java.util.*;

public class StateObject {

    private ExecutableAutomaton execAut;
    private ExecutableAutomaton[] pExecAut;
    private int eventPos;
    private String[][] matrix;
    private double health;
    private Vector<Double> healthWindow;
    private HashMap<String, DeclareReplayer> replayers = new HashMap<>();

    private final HashSet<String> templates = new HashSet<>();

    private final List<List<String>> dispositionsresponse = new ArrayList<>();
    private final List<List<String>> dispositionsprecedence = new ArrayList<>();
    private final List<List<String>> dispositionsnotresponse = new ArrayList<>();
    private final List<List<String>> dispositionsnotprecedence = new ArrayList<>();
    private final List<List<String>> dispositionsnotchainresponse = new ArrayList<>();
    private final List<List<String>> dispositionsnotchainprecedence = new ArrayList<>();
    private final List<List<String>> dispositionsnotrespondedexistence = new ArrayList<>();
    private final List<List<String>> dispositionssuccession = new ArrayList<>();
    private final List<List<String>> dispositionsnotsuccession = new ArrayList<>();
    private final List<List<String>> dispositionschainresponse = new ArrayList<>();
    private final List<List<String>> dispositionschainprecedence = new ArrayList<>();
    private final List<List<String>> dispositionschainsuccession = new ArrayList<>();
    private final List<List<String>> dispositionsnotchainsuccession = new ArrayList<>();
    private final List<List<String>> dispositionsalternateresponse = new ArrayList<>();
    private final List<List<String>> dispositionsalternateprecedence = new ArrayList<>();
    private final List<List<String>> dispositionsalternatesuccession = new ArrayList<>();
    private final List<List<String>> dispositionsrespondedexistence = new ArrayList<>();
    private final List<List<String>> dispositionscoexistence = new ArrayList<>();
    private final List<List<String>> dispositionsnotcoexistence = new ArrayList<>();
    private final List<List<String>> dispositionsexclusivechoice = new ArrayList<>();
    private final List<List<String>> dispositionschoice = new ArrayList<>();

    private final List<List<String>> invdispositionscoexistence = new ArrayList<>();
    private final List<List<String>> invdispositionsnotcoexistence = new ArrayList<>();
    private final List<List<String>> invdispositionsexclusivechoice = new ArrayList<>();
    private final List<List<String>> invdispositionschoice = new ArrayList<>();

    private final List<List<String>> dispositionsinit = new ArrayList<>();
    private final List<List<String>> dispositionsabsence = new ArrayList<>();
    private final List<List<String>> dispositionsabsence2 = new ArrayList<>();
    private final List<List<String>> dispositionsabsence3 = new ArrayList<>();
    private final List<List<String>> dispositionsexactly1 = new ArrayList<>();
    private final List<List<String>> dispositionsexactly2 = new ArrayList<>();
    private final List<List<String>> dispositionsexistence = new ArrayList<>();
    private final List<List<String>> dispositionsexistence2 = new ArrayList<>();
    private final List<List<String>> dispositionsexistence3 = new ArrayList<>();
    private final List<List<String>> dispositionsstronginit = new ArrayList<>();

    private final Set<String> candidateActivations = new HashSet<>();
    private final HashMap<String, HashSet<String>> occurredParametersByTemplate = new HashMap<>();

    private HashMap<String, HashMap<Integer, Pair<String,String>>> solverMap;
    private HashMap<String, HashMap<Integer, Pair<String,String>>> postActivation;
    private HashMap<String, HashMap<Integer, Pair<String,String>>> posActs;
    private HashMap<String, HashMap<Integer, Pair<String,String>>> acMap;
    private HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String,String>>>> activePositiveTemps;
    private HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String,String>>>> activeNegativeTemps;

    public HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> getActiveNegativeTemps() {
        return activeNegativeTemps;
    }

    public void setActiveNegativeTemps(HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activeNegativeTemps) {
        this.activeNegativeTemps = activeNegativeTemps;
    }

    public HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> getActivePositiveTemps() {
        return activePositiveTemps;
    }

    public void setActivePositiveTemps(HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activePositiveTemps) {
        this.activePositiveTemps = activePositiveTemps;
    }

    private boolean data = false;
    private OnlineTraceReaderAndReplayer readerReplayer;

    public Vector<Double> getHealthWindow() {
        return healthWindow;
    }

    public void setHealthWindow(Vector healthWindow) {
        this.healthWindow = healthWindow;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public int getEventPos() {
        return eventPos;
    }

    public void setEventPos(int eventPos) {
        this.eventPos = eventPos;
    }

    public String[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(String[][] matrix) {
        this.matrix = matrix;
    }

    public ExecutableAutomaton getExecAut() {
        return execAut;
    }

    public void setExecAut(ExecutableAutomaton execAut) {
        this.execAut = execAut;
    }

    public ExecutableAutomaton[] getpExecAut() {
        return pExecAut;
    }

    public void setpExecAut(ExecutableAutomaton[] pExecAut) {
        this.pExecAut = pExecAut;
    }

    public HashMap<String, DeclareReplayer> getReplayers() {
        return replayers;
    }

    public void setReplayers(HashMap<String, DeclareReplayer> replayers) {
        this.replayers = replayers;
    }

    public boolean hasData() {
        return data;
    }

    public Set<String> getCandidateActivations() {
        return candidateActivations;
    }

    public void prepareDataStructure(AssignmentModel assignmentModel) {
        System.out.println("prepareDataStructure ");

        this.replayers = new HashMap<>();

        for (ConstraintDefinition cd : assignmentModel.getConstraintDefinitions()) {
            boolean occurred = false;
            HashSet<String> occurredParameters = new HashSet<>();
            if (occurredParametersByTemplate.containsKey(cd.getDisplay())) {
                occurredParameters = occurredParametersByTemplate.get(cd.getDisplay());
            }

            templates.add(cd.getName());
            String temp = cd.getName();
            ArrayList<String> disposs = new ArrayList<>();
            for (Parameter p : cd.getParameters()) {
                for (ActivityDefinition b : cd.getBranches(p)) {
                    if ((!b.getName().contains("-assign") && !b.getName().contains("-ate_abort") && !b.getName().contains("-suspend") && !b.getName().contains("-complete") && !b.getName().contains("-autoskip") && !b.getName().contains("-manualskip") && !b.getName().contains("pi_abort") && !b.getName().contains("-reassign") && !b.getName().contains("-resume") && !b.getName().contains("-schedule") && !b.getName().contains("-start") && !b.getName().contains("-unknown") && !b.getName().contains("-withdraw")) && (!b.getName().contains("<center>assign") && !b.getName().contains("<center>ate_abort") && !b.getName().contains("<center>suspend") && !b.getName().contains("<center>complete") && !b.getName().contains("<center>autoskip") && !b.getName().contains("<center>manualskip") && !b.getName().contains("<center>pi_abort") && !b.getName().contains("<center>reassign") && !b.getName().contains("<center>resume") && !b.getName().contains("<center>schedule") && !b.getName().contains("<center>start") && !b.getName().contains("<center>unknown") && !b.getName().contains("<center>withdraw"))) {
                        disposs.add(b.getName().toLowerCase() + "-complete");
                        if (cd.getCondition().toString().contains("[")) {
                            candidateActivations.add(b.getName().toLowerCase() + "-complete");
                            if (occurredParameters.contains(b.getName().toLowerCase() + "-complete")) {
                                occurred = true;
                            }
                            occurredParameters.add(b.getName().toLowerCase() + "-complete");
                            data = true;
                        }
                    } else {
                        disposs.add(b.getName().toLowerCase());
                        if (cd.getCondition().toString().contains("[")) {
                            candidateActivations.add(b.getName().toLowerCase());
                            if (occurredParameters.contains(b.getName().toLowerCase())) {
                                occurred = true;
                            }
                            occurredParameters.add(b.getName().toLowerCase());
                            data = true;
                        }
                    }
                }
            }
            occurredParametersByTemplate.put(cd.getDisplay(), occurredParameters);
            String tempLower = temp.toLowerCase();
            switch (tempLower) {
                case ConstraintName.RESPONSE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsresponse.add(disposs);
                    break;
                case ConstraintName.NOT_RESPONSE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotresponse.add(disposs);
                    break;
                case ConstraintName.EXCLUSIVE_CHOICE: {
                    disposs.add(ConstraintName.RESPONDED_EXISTENCE);
                    disposs.add(cd.getCondition().toString());
                    ArrayList<String> inverse = new ArrayList<>();
                    inverse.add(disposs.get(1));
                    inverse.add(disposs.get(0));
                    inverse.add(disposs.get(2));
                    inverse.add(disposs.get(3));
                    dispositionsexclusivechoice.add(disposs);
                    invdispositionsexclusivechoice.add(inverse);
                    break;
                }
                case ConstraintName.CHOICE: {
                    disposs.add(ConstraintName.RESPONDED_EXISTENCE);
                    disposs.add(cd.getCondition().toString());
                    ArrayList<String> inverse = new ArrayList<>();
                    inverse.add(disposs.get(1));
                    inverse.add(disposs.get(0));
                    inverse.add(disposs.get(2));
                    inverse.add(disposs.get(3));
                    dispositionschoice.add(disposs);
                    invdispositionschoice.add(inverse);
                    break;
                }
                case ConstraintName.PRECEDENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsprecedence.add(disposs);
                    break;
                case ConstraintName.NOT_PRECEDENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotprecedence.add(disposs);
                    break;
                case ConstraintName.INIT:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsinit.add(disposs);
                    break;
                case ConstraintName.STRONG_INIT:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsinit.add(disposs);
                    break;
                case ConstraintName.ABSENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsabsence.add(disposs);
                    break;
                case ConstraintName.ABSENCE2:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsabsence2.add(disposs);
                    break;
                case ConstraintName.ABSENCE3:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsabsence3.add(disposs);
                    break;
                case ConstraintName.EXISTENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsexistence.add(disposs);
                    break;
                case ConstraintName.EXISTENCE2:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsexistence2.add(disposs);
                    break;
                case ConstraintName.EXISTENCE3:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsexistence3.add(disposs);
                    break;
                case ConstraintName.EXACTLY1:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsexactly1.add(disposs);
                    break;
                case ConstraintName.EXACTLY2:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsexactly2.add(disposs);
                    break;
                case ConstraintName.RESPONDED_EXISTENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsrespondedexistence.add(disposs);
                    break;
                case ConstraintName.NOT_RESPONDED_EXISTENCE:
                    disposs.add(ConstraintName.RESPONDED_EXISTENCE);
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotrespondedexistence.add(disposs);
                    break;
                case ConstraintName.CO_EXISTENCE: {
                    disposs.add(ConstraintName.RESPONDED_EXISTENCE);
                    disposs.add(cd.getCondition().toString());
                    ArrayList<String> inverse = new ArrayList<>();
                    inverse.add(disposs.get(1));
                    inverse.add(disposs.get(0));
                    inverse.add(disposs.get(2));
                    inverse.add(disposs.get(3));
                    dispositionscoexistence.add(disposs);
                    invdispositionscoexistence.add(inverse);
                    break;
                }
                case ConstraintName.NOT_CO_EXISTENCE: {
                    disposs.add(ConstraintName.RESPONDED_EXISTENCE);
                    disposs.add(cd.getCondition().toString());
                    //.add(disposs);
                    ArrayList<String> inverse = new ArrayList<>();
                    inverse.add(disposs.get(1));
                    inverse.add(disposs.get(0));
                    inverse.add(disposs.get(2));
                    inverse.add(disposs.get(3));
                    dispositionsnotcoexistence.add(disposs);
                    invdispositionsnotcoexistence.add(inverse);
                    break;
                }
                case ConstraintName.SUCCESSION:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionssuccession.add(disposs);
                    break;
                case ConstraintName.NOT_SUCCESSION:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotsuccession.add(disposs);
                    break;
                case ConstraintName.CHAIN_RESPONSE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionschainresponse.add(disposs);
                    break;
                case ConstraintName.NOT_CHAIN_RESPONSE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotchainresponse.add(disposs);
                    break;
                case ConstraintName.CHAIN_PRECEDENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionschainprecedence.add(disposs);
                    break;
                case ConstraintName.NOT_CHAIN_PRECEDENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotchainprecedence.add(disposs);
                    break;
                case ConstraintName.CHAIN_SUCCESSION:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionschainsuccession.add(disposs);
                    break;
                case ConstraintName.NOT_CHAIN_SUCCESSION:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsnotchainsuccession.add(disposs);
                    break;
                case ConstraintName.ALTERNATE_RESPONSE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsalternateresponse.add(disposs);
                    break;
                case ConstraintName.ALTERNATE_PRECEDENCE:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsalternateprecedence.add(disposs);
                    break;
                case ConstraintName.ALTERNATE_SUCCESSION:
                    disposs.add(cd.getDisplay());
                    disposs.add(cd.getCondition().toString());
                    dispositionsalternatesuccession.add(disposs);
                    break;
            }
            //}
        }

        for (String temp : templates) {
            String tempLower = temp.toLowerCase();
            switch (tempLower) {
                case ConstraintName.RESPONSE: {
                    ResponseAnalyzer replayer = new ResponseAnalyzer(dispositionsresponse);
                    replayers.put(ConstraintName.RESPONSE, replayer);
                    break;
                }
                case ConstraintName.NOT_RESPONSE: {
                    NotResponseAnalyzer replayer = new NotResponseAnalyzer(dispositionsnotresponse);
                    replayers.put(ConstraintName.NOT_RESPONSE, replayer);
                    break;
                }
                case ConstraintName.PRECEDENCE: {
                    PrecedenceAnalyzer replayer = new PrecedenceAnalyzer(dispositionsprecedence);
                    replayers.put(ConstraintName.PRECEDENCE, replayer);
                    break;
                }
                case ConstraintName.NOT_PRECEDENCE: {
                    NotPrecedenceAnalyzer replayer = new NotPrecedenceAnalyzer(dispositionsnotprecedence);
                    replayers.put(ConstraintName.NOT_PRECEDENCE, replayer);
                    break;
                }
                case ConstraintName.EXCLUSIVE_CHOICE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsexclusivechoice);
                    replayers.put(ConstraintName.EXCLUSIVE_CHOICE, replayer);
                    RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionsexclusivechoice);
                    replayers.put(ConstraintName.INV_EXCLUSIVE_CHOICE, replayerinv);
                    break;
                }
                case ConstraintName.CHOICE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionschoice);
                    replayers.put(ConstraintName.CHOICE, replayer);
                    RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionschoice);
                    replayers.put(ConstraintName.INV_CHOICE, replayerinv);
                    break;
                }
                case ConstraintName.INIT: {
                    InitAnalyzer replayer = new InitAnalyzer(dispositionsinit);
                    replayers.put(ConstraintName.INIT, replayer);
                    break;
                }
                case ConstraintName.STRONG_INIT: {
                    InitAnalyzer replayer = new InitAnalyzer(dispositionsstronginit);
                    replayers.put(ConstraintName.STRONG_INIT, replayer);
                    break;
                }
                case ConstraintName.ABSENCE: {
                    AbsenceAnalyzer replayer = new AbsenceAnalyzer(dispositionsabsence);
                    replayers.put(ConstraintName.ABSENCE, replayer);
                    break;
                }
                case ConstraintName.ABSENCE2: {
                    Absence2Analyzer replayer = new Absence2Analyzer(dispositionsabsence2);
                    replayers.put(ConstraintName.ABSENCE2, replayer);
                    break;
                }
                case ConstraintName.ABSENCE3: {
                    Absence3Analyzer replayer = new Absence3Analyzer(dispositionsabsence3);
                    replayers.put(ConstraintName.ABSENCE3, replayer);
                    break;
                }
                case ConstraintName.EXISTENCE: {
                    ExistenceAnalyzer replayer = new ExistenceAnalyzer(dispositionsexistence);
                    replayers.put(ConstraintName.EXISTENCE, replayer);
                    break;
                }
                case ConstraintName.EXISTENCE2: {
                    Existence2Analyzer replayer = new Existence2Analyzer(dispositionsexistence2);
                    replayers.put(ConstraintName.EXISTENCE2, replayer);
                    break;
                }
                case ConstraintName.EXISTENCE3: {
                    Existence3Analyzer replayer = new Existence3Analyzer(dispositionsexistence3);
                    replayers.put(ConstraintName.EXISTENCE3, replayer);
                    break;
                }
                case ConstraintName.EXACTLY1: {
                    Exactly1Analyzer replayer = new Exactly1Analyzer(dispositionsexactly1);
                    replayers.put(ConstraintName.EXACTLY1, replayer);
                    break;
                }
                case ConstraintName.EXACTLY2: {
                    Exactly2Analyzer replayer = new Exactly2Analyzer(dispositionsexactly2);
                    replayers.put(ConstraintName.EXACTLY2, replayer);
                    break;
                }
                case ConstraintName.RESPONDED_EXISTENCE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsrespondedexistence);
                    replayers.put(ConstraintName.RESPONDED_EXISTENCE, replayer);
                    break;
                }
                case ConstraintName.NOT_RESPONDED_EXISTENCE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsnotrespondedexistence);
                    replayers.put(ConstraintName.NOT_RESPONDED_EXISTENCE, replayer);
                    break;
                }
                case ConstraintName.CO_EXISTENCE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionscoexistence);
                    replayers.put(ConstraintName.CO_EXISTENCE, replayer);
                    RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionscoexistence);
                    replayers.put(ConstraintName.INV_CO_EXISTENCE, replayerinv);
                    break;
                }
                case ConstraintName.NOT_CO_EXISTENCE: {
                    RespondedExistenceAnalyzer replayer = new RespondedExistenceAnalyzer(dispositionsnotcoexistence);
                    replayers.put(ConstraintName.NOT_CO_EXISTENCE, replayer);
                    RespondedExistenceAnalyzer replayerinv = new RespondedExistenceAnalyzer(invdispositionsnotcoexistence);
                    replayers.put(ConstraintName.INV_NOT_CO_EXISTENCE, replayerinv);
                    break;
                }
                case ConstraintName.SUCCESSION: {
                    ResponseAnalyzer replayerResponse = new ResponseAnalyzer(dispositionssuccession);
                    replayers.put(ConstraintName.SUCCESSION_RESPONSE, replayerResponse);
                    PrecedenceAnalyzer replayerPrecedence = new PrecedenceAnalyzer(dispositionssuccession);
                    replayers.put(ConstraintName.SUCCESSION_PRECEDENCE, replayerPrecedence);
                    break;
                }
                case ConstraintName.NOT_SUCCESSION: {
                    NotResponseAnalyzer replayerNotResponse = new NotResponseAnalyzer(dispositionsnotsuccession);
                    replayers.put(ConstraintName.NOT_SUCCESSION_RESPONSE, replayerNotResponse);
                    NotPrecedenceAnalyzer replayerNotPrecedence = new NotPrecedenceAnalyzer(dispositionsnotsuccession);
                    replayers.put(ConstraintName.NOT_SUCCESSION_PRECEDENCE, replayerNotPrecedence);
                    break;
                }
                case ConstraintName.CHAIN_RESPONSE: {
                    ChainResponseAnalyzer replayer = new ChainResponseAnalyzer(dispositionschainresponse);
                    replayers.put(ConstraintName.CHAIN_RESPONSE, replayer);
                    break;
                }
                case ConstraintName.NOT_CHAIN_RESPONSE: {
                    NotChainResponseAnalyzer replayer = new NotChainResponseAnalyzer(dispositionsnotchainresponse);
                    replayers.put(ConstraintName.NOT_CHAIN_RESPONSE, replayer);
                    break;
                }
                case ConstraintName.CHAIN_PRECEDENCE: {
                    ChainPrecedenceAnalyzer replayer = new ChainPrecedenceAnalyzer(dispositionschainprecedence);
                    replayers.put(ConstraintName.CHAIN_PRECEDENCE, replayer);
                    break;
                }
                case ConstraintName.NOT_CHAIN_PRECEDENCE: {
                    NotChainPrecedenceAnalyzer replayer = new NotChainPrecedenceAnalyzer(dispositionsnotchainprecedence);
                    replayers.put(ConstraintName.NOT_CHAIN_PRECEDENCE, replayer);
                    break;
                }
                case ConstraintName.CHAIN_SUCCESSION: {
                    ChainResponseAnalyzer replayerResponse = new ChainResponseAnalyzer(dispositionschainsuccession);
                    replayers.put(ConstraintName.CHAIN_SUCCESSION_RESPONSE, replayerResponse);
                    ChainPrecedenceAnalyzer replayerPrecedence = new ChainPrecedenceAnalyzer(dispositionschainsuccession);
                    replayers.put(ConstraintName.CHAIN_SUCCESSION_PRECEDENCE, replayerPrecedence);
                    break;
                }
                case ConstraintName.ALTERNATE_RESPONSE: {
                    AlternateResponseAnalyzer replayer = new AlternateResponseAnalyzer(dispositionsalternateresponse);
                    replayers.put(ConstraintName.ALTERNATE_RESPONSE, replayer);
                    break;
                }
                case ConstraintName.ALTERNATE_PRECEDENCE: {
                    AlternatePrecedenceAnalyzer replayer = new AlternatePrecedenceAnalyzer(dispositionsalternateprecedence);
                    replayers.put(ConstraintName.ALTERNATE_PRECEDENCE, replayer);
                    break;
                }
                case ConstraintName.ALTERNATE_SUCCESSION: {
                    AlternateResponseAnalyzer replayerResponse = new AlternateResponseAnalyzer(dispositionsalternatesuccession);
                    replayers.put(ConstraintName.ALTERNATE_SUCCESSION_RESPONSE, replayerResponse);
                    AlternatePrecedenceAnalyzer replayerPrecedence = new AlternatePrecedenceAnalyzer(dispositionsalternatesuccession);
                    replayers.put(ConstraintName.ALTERNATE_SUCCESSION_PRECEDENCE, replayerPrecedence);
                    break;
                }
                case ConstraintName.NOT_CHAIN_SUCCESSION: {
                    NotChainResponseAnalyzer replayerNotResponse = new NotChainResponseAnalyzer(dispositionsnotchainsuccession);
                    replayers.put(ConstraintName.NOT_CHAIN_SUCCESSION_RESPONSE, replayerNotResponse);
                    NotChainPrecedenceAnalyzer replayerNotPrecedence = new NotChainPrecedenceAnalyzer(dispositionsnotchainsuccession);
                    replayers.put(ConstraintName.NOT_CHAIN_SUCCESSION_PRECEDENCE, replayerNotPrecedence);
                    break;
                }
            }
        }


    }

    public OnlineTraceReaderAndReplayer getReaderReplayer() {
        return readerReplayer;
    }

    public void setReaderReplayer(OnlineTraceReaderAndReplayer readerReplayer) {
        this.readerReplayer = readerReplayer;
    }

    public HashMap<String, HashMap<Integer, Pair<String,String>>> getSolverMap() {
        return this.solverMap;
    }


    public void buildLPSolver(AssignmentModel model, String[] consts) {
        System.out.println("buildLPSolver()");
        HashSet<String> activities = new HashSet<>();

        Iterator<ActivityDefinition> nameIterator = model.getConstrainedActivities();
        // Iterate over all elements in the list
        while (nameIterator.hasNext()) {
            // Get the next element from the list
            ActivityDefinition activity = nameIterator.next();
            //System.out.println(activity);
            if (activity != null) {
                activities.add(activity.getName());
            }
        }

        this.solverMap = new HashMap<>();
        this.postActivation = new HashMap<>();
        this.acMap = new HashMap<>();
        this.posActs = new HashMap<>();

        this.activePositiveTemps = new HashMap<>();
        this.activeNegativeTemps = new HashMap<>();

        System.out.println("Number of activities " + activities.size());
        for (String a : activities) {
            HashMap<Integer, String> h = new HashMap<>();
            // solverMap.put(a.toLowerCase() + "-complete", h);
            System.out.println(a.toLowerCase().replace(" ", "_") + "_complete");
        }

        int cdPos = 0;
        for (ConstraintDefinition cd : model.getConstraintDefinitions()) {
            final String name = cd.getName();
            System.out.println("Constraint name  :" + name);
            final String caption = cd.getCaption();
            System.out.println("Constraint caption  :" + caption);
            final String condition = cd.getCondition().toString();
            System.out.println("Constraint condition  :" + condition);
            //final String constraint = name + "(" + caption.substring(caption.indexOf("[")) + ", " + condition + ")";
            String activityA = caption.substring(StringUtils.ordinalIndexOf(caption, "[", 1), StringUtils.ordinalIndexOf(caption, "]", 1) + 1);
            activityA = activityA.replace("[", "").replace("]", "");
            activityA = activityA.toLowerCase().replace(" ", "_") + "_complete";
            String activityT = StringUtils.ordinalIndexOf(caption, "[", 2) > 0 ?
                    caption.substring(StringUtils.ordinalIndexOf(caption, "[", 2), StringUtils.ordinalIndexOf(caption, "]", 2) + 1) :
                    "";
            activityT = activityT.replace("[", "").replace("]", "");
            activityT = activityT.toLowerCase().replace(" ", "_") + "_complete";
            System.out.println("Constraint Activity A  :" + activityA + " , Constraint Activity B  :" + activityT);

            String activationCondition = condition.substring(StringUtils.ordinalIndexOf(condition, "[", 1), StringUtils.ordinalIndexOf(condition, "]", 1) + 1);
            activationCondition = activationCondition.replace("[", "( ").replace("]", " )");
            activationCondition = activationCondition.replace("A.", activityA + ".");

            String coRelationCondition = StringUtils.ordinalIndexOf(condition, "[", 2) > 0 ?
                    condition.substring(StringUtils.ordinalIndexOf(condition, "[", 2), StringUtils.ordinalIndexOf(condition, "]", 2) + 1) :
                    "";
            coRelationCondition = coRelationCondition.replace("[", "( ").replace("]", " )");
            coRelationCondition = coRelationCondition.replace("A.", activityA + ".");
            coRelationCondition = coRelationCondition.replace("T.", activityT + ".");

            String temporalCondition = StringUtils.ordinalIndexOf(condition, "[", 3) > 0 ?
                    condition.substring(StringUtils.ordinalIndexOf(condition, "[", 3), StringUtils.ordinalIndexOf(condition, "]", 3) + 1) :
                    "";
            temporalCondition = temporalCondition.replace("[", "( ").replace("]", " )");

            System.out.println("Activation Condition :" + activationCondition + " , Co-relation Condition   :" + coRelationCondition + " , Temporal Condition   :" + temporalCondition);

            String solverCondition = "";
            switch (name) {
                case ConstraintName.PRECEDENCE:
                case ConstraintName.NOT_PRECEDENCE:
                case ConstraintName.INIT:
                case ConstraintName.STRONG_INIT:
                case ConstraintName.SUCCESSION:
                case ConstraintName.NOT_SUCCESSION:
                case ConstraintName.CHAIN_PRECEDENCE:
                case ConstraintName.NOT_CHAIN_PRECEDENCE:
                case ConstraintName.NOT_CHAIN_SUCCESSION:
                case ConstraintName.CHAIN_SUCCESSION:
                case ConstraintName.ALTERNATE_PRECEDENCE:
                case ConstraintName.ALTERNATE_SUCCESSION:
                case ConstraintName.CO_EXISTENCE:
                case ConstraintName.EXCLUSIVE_CHOICE:
                case ConstraintName.CHOICE:
                case ConstraintName.ABSENCE2:
                case ConstraintName.ABSENCE3:
                case ConstraintName.EXISTENCE2:
                case ConstraintName.EXISTENCE3:
                case ConstraintName.EXACTLY1:
                case ConstraintName.EXACTLY2:
                    continue;
                case ConstraintName.EXISTENCE:
                    solverCondition = activationCondition;
                    solverCondition = solverCondition.isEmpty() || solverCondition.equals("(  )")? "( "+ activityT + ".foo" + " == 0 )": solverCondition;
                    if (solverMap.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        solverMap.put(activityA, h);
                    } else {
                        solverMap.get(activityA).put(cdPos, new Pair<>(activityA, solverCondition));
                    }

                    if (postActivation.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        postActivation.put(activityA, h);
                    } else {
                        postActivation.get(activityA).put(cdPos,new Pair<>(activityA, solverCondition));
                    }
                    break;
                case ConstraintName.ABSENCE:
                    solverCondition = activationCondition;
                    solverCondition = solverCondition.isEmpty() || solverCondition.equals("(  )")? "( "+ activityT + ".foo" + " == 0 )": solverCondition;
                    solverCondition = "!" + solverCondition;
                    if (solverMap.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        solverMap.put(activityA, h);
                    } else {
                        solverMap.get(activityA).put(cdPos, new Pair<>(activityA, solverCondition));
                    }

                    if (postActivation.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        postActivation.put(activityA, h);
                    } else {
                        postActivation.get(activityA).put(cdPos,new Pair<>(activityA, solverCondition));
                    }
                    break;

                case ConstraintName.NOT_RESPONSE:
                case ConstraintName.NOT_RESPONDED_EXISTENCE:
                case ConstraintName.NOT_CHAIN_RESPONSE:
                    solverCondition = coRelationCondition;
                    solverCondition = solverCondition.isEmpty() || solverCondition.equals("(  )") ? "( "+ activityT + ".foo" + " == 0 )": solverCondition;
                    solverCondition = "!" + solverCondition;
                    if (solverMap.get(activityT) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        solverMap.put(activityT, h);
                    } else {
                        solverMap.get(activityT).put(cdPos,new Pair<>(activityA, solverCondition));
                    }

                    activationCondition = activationCondition.isEmpty() || activationCondition.equals("(  )") ? "( "+ activityA + ".foo" + " == 0 )": activationCondition;
                    if (acMap.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityT, activationCondition));
                        acMap.put(activityA, h);
                    } else {
                        acMap.get(activityA).put(cdPos, new Pair<>(activityT, activationCondition));
                    }
                    break;
                default:
                    solverCondition = coRelationCondition;
                    solverCondition = solverCondition.isEmpty() || solverCondition.equals("(  )") ? "( "+ activityT + ".foo" + " == 0 )": solverCondition + " &&  ( "+ activityT + ".foo" + " == 0 )";
                    if (solverMap.get(activityT) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityA, solverCondition));
                        solverMap.put(activityT, h);
                    } else {
                        solverMap.get(activityT).put(cdPos,new Pair<>(activityA, solverCondition));
                    }

                    activationCondition = activationCondition.isEmpty() || activationCondition.equals("(  )") ? "( "+ activityA + ".foo" + " == 0 )": activationCondition;
                    if (acMap.get(activityA) == null) {
                        HashMap<Integer, Pair<String,String>> h = new HashMap<>();
                        h.put(cdPos, new Pair<>(activityT, activationCondition));
                        acMap.put(activityA, h);
                    } else {
                        acMap.get(activityA).put(cdPos, new Pair<>(activityT, activationCondition));
                    }

                    break;
            }

            cdPos++;
        }


    }

    public HashMap<String, HashMap<Integer, Pair<String,String>>> getPostActivation() {
        return this.postActivation;
    }
    public HashMap<String, HashMap<Integer, Pair<String,String>>> getActivationMap() {
        return this.acMap;
    }

    public HashMap<String, HashMap<Integer, Pair<String, String>>> getPosActs() {
        return posActs;
    }

    public void setPosActs(HashMap<String, HashMap<Integer, Pair<String, String>>> posActs) {
        this.posActs = posActs;
    }
}
