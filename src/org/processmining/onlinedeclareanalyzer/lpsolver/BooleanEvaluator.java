package org.processmining.onlinedeclareanalyzer.lpsolver;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.lexer.Lexer;
import org.processmining.onlinedeclareanalyzer.lpsolver.parser.RecursiveDescentParser;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class BooleanEvaluator {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner((System.in));
        String expression = "";
        if (args.length > 0 && args[0].equals("-f")) {
            while (sc.hasNextLine()) expression += sc.nextLine();
            System.out.println(expression);
        } else {
            System.out.println("Insert an expression:");
            //expression = "(cat > rat & ((true | ball < dog ) & (true & false)))"; //sc.nextLine();
            //expression = "((b >= 1) | (c > 4)) & ((c < 5) | (d < 2))) "; //sc.nextLine();
            //expression = "((( (b + c + d) >= 1) | (c > 4)) & ((c == 5) | (d != 2)))) "; //sc.nextLine();
            expression = "(b != 1) || (b == 3 )";
           // expression = "!(b == 1)";
            //expression = "!(b == 1) && !(b == 3 )";
            expression = "(!( b_complete.x == 3 )) && (!( b_complete.x == 5 )) && (( b_complete.x == 1 ) )";
            //expression = "(!( b_complete.x == 3 ))  && (( b_complete.x == 1 ) )";
           // expression = "( (b != 5) && (b <= 5)";
            //expression = "(((f - d) != 55) | ( (m + b) > 4)) & ((c < 5) | (d < 2))) ";
            //expression = "( ( (y - x - z) < 5) & ( x != 4) ) ";
            //expression = "  ((y - (x - z)) < 5 ) ";
            //expression = "((b > 1) | (c > 4))) "; //sc.nextLine();
            //expression = "((b > 1) & ((c < 5) | (d < 2))) "; //sc.nextLine();
            //expression = "((b > 1) | (c > 4)) "; //sc.nextLine();
            //expression = "((b > 1) | (c > 4)) & ((d < 5) | (e < 2))  & ((f < 5) | (g < 2)))  "; //sc.nextLine();
            //expression = "( (T.x > T.y ) & ( T.y == 4 ) " ;
            //expression = "( (T.x >= A.x) & (T.x < 5) & (A.x >= 4) )" ;
           // expression = "( (c_complete.x >= b_complete.x) & (c_complete.x < 5) & (b_complete.x >= 5) )" ;
        }
        BooleanEvaluator booleanEvaluator = new BooleanEvaluator();
        Lexer lexer = new Lexer(new ByteArrayInputStream(expression.getBytes()));
        RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
        BooleanExpression ast = parser.build();

        ArrayList<ArrayList<BooleanExpression>> o = ast.interpret();
        ArrayList<String> output = ast.traverse();
        System.out.println(String.format("AST: %s", ast));
        System.out.println(String.format("RES: %s", o));
        System.out.println(String.format("RES: %s", output));

        ArrayList<ArrayList<BooleanExpression>> problemSets = ast.interpret();
        System.out.println(String.format("AST: %s", ast));
        System.out.println(String.format("RES as array : %s", problemSets));

//        boolean conflict = false;
//
//        for (ArrayList<BooleanExpression> problemSet : problemSets) {
//            conflict = conflict || LpSolverUtil.hasConflict(problemSet);
//            //LpSolverUtil.getResults(problemSet);
//        }

        boolean conflict = LpSolverUtil.hasConflict(expression);

        System.out.println("Final Conflict " + conflict);
    }
}
