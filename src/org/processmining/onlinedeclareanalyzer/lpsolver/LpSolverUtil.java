package org.processmining.onlinedeclareanalyzer.lpsolver;


import net.sf.javailp.*;
import org.processmining.framework.util.Pair;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal.Co;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal.Lexp;
import org.processmining.onlinedeclareanalyzer.lpsolver.lexer.Lexer;

import org.processmining.onlinedeclareanalyzer.lpsolver.parser.RecursiveDescentParser;
import org.processmining.utils.NumbersUtil;

import java.io.ByteArrayInputStream;
import java.util.*;

/**
 * Created by Ubaier on 09/04/2016.
 */
public class LpSolverUtil {

    public static boolean solverForConflict(HashMap<Integer, Pair<String, String>> equationsList, String extraCondition) {

        String equations = joinExpressionsInList(equationsList);

        if (extraCondition != null || !equations.isEmpty()) {
            equations = equations + " & " + " (" + extraCondition + ")";
        }

        //System.out.println(" RecursiveDescentParser: " + equations);

        Lexer lexer = new Lexer(new ByteArrayInputStream(equations.getBytes()));
        RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
        BooleanExpression ast = parser.build();

        ArrayList<ArrayList<BooleanExpression>> problemSets = ast.interpret();
        System.out.println(String.format("AST: %s", ast));
        System.out.println(String.format("RES as array : %s", problemSets));
        System.out.println(String.format("RES as array : %s", problemSets));

        if(problemSets.isEmpty()){
            return false;
        }

        boolean solution = false;

        for (ArrayList<BooleanExpression> problemSet : problemSets) {
            solution = solution || LpSolverUtil.hasSolution(problemSet);
        }

        return !solution;
    }

    public static boolean hasConflict(String s) {

        BooleanExpression ast = getBooleanExpression(s);

        ArrayList<ArrayList<BooleanExpression>> problemSets = ast.interpret();
        System.out.println(String.format("AST: %s", ast));
        System.out.println(String.format("RES as array : %s", problemSets));

        if(problemSets.isEmpty()){
            return false;
        }

        boolean solution = false;

        for (ArrayList<BooleanExpression> problemSet : problemSets) {
            solution = solution || LpSolverUtil.hasSolution(problemSet);
        }

        return !solution;
    }

    public static boolean hasConflict(ArrayList<BooleanExpression> sysOfEqs) {
        System.out.println("============ hasConflict ===================");
        return LpSolverUtil.getResults(sysOfEqs) == null;
    }

    public static boolean hasSolution(ArrayList<BooleanExpression> sysOfEqs) {
        System.out.println("============ hasConflict ===================");
        return LpSolverUtil.getResults(sysOfEqs) != null;
    }

    public static HashMap<String, HashMap<String, Integer>> getResults(ArrayList<BooleanExpression> sysOfEqs) {
        System.out.println("============ getResults ===================");

        SolverFactory factory = new SolverFactoryLpSolve();
        factory.setParameter(Solver.VERBOSE, 0);
        factory.setParameter(Solver.TIMEOUT, 100);

        Problem problem = new Problem();

        HashMap<String, Integer> strings = new HashMap<>();

        for (BooleanExpression eq : sysOfEqs) {
            if (!(eq instanceof Co)) {
                continue;
            }
            Co e = (Co) eq;

            BooleanExpression l = ((Co) eq).getLeft();

            ArrayList<Pair<String, Integer>> lhsList = new ArrayList<>();
            if (l instanceof Lexp) {
                Lexp ll = (Lexp) l;
                lhsList.addAll(ll.getPairs(1));
            } else {
                lhsList.add(new Pair<>(l.toString(), 1));
            }

            String lhs;
            int sgn;

            String operator = e.getSymbol();

            double adjVal = 0.000001;
            int adj = 0;
            switch (operator) {
                case "==":
                    operator = "=";
                    adj = 0;
                    break;
                case "<=":
                    operator = "<=";
                    adj = 0;
                    break;
                case "!=":
                case "<":
                    operator = "<=";
                    adj = -1;
                    break;
                case ">=":
                    operator = ">=";
                    adj = 0;
                    break;
                case ">":
                    operator = ">=";
                    adj = 1;
                    break;
            }

            String rhsString = e.getRight().toString();
            Double rhs = NumbersUtil.parseDoubleSafely(rhsString);

            if (rhs == null) {
                if (rhsString.contains("_complete.") || rhsString.contains("A.") || rhsString.contains("T.")) {
                    int x = -1;
                    if (rhsString.startsWith("-")) {
                        x = 1;
                        rhsString = rhsString.replace("-", "");
                    }

                    lhsList.add(new Pair<>(rhsString, x));
                    rhs = 0d;
                    System.out.println("Right moved to lhs");
                } else {

                    Integer i = strings.size() + 1;
                    if (strings.get(rhsString) != null) {
                        i = strings.get(rhsString);
                    } else {
                        strings.put(rhsString, i);
                    }
                    rhs = Double.valueOf(i);
                    System.out.println("Right hand side is NaN. rhsString: " + rhsString + " value :" + i);
                }
            }

            // Clean and apply adjustments

            rhs = rhs + (adj * adjVal);

            for (Pair<String, Integer> p : lhsList) {
                if (!problem.getVariables().contains(p.getFirst())) {
                    lhs = p.getFirst();
                    sgn = p.getSecond();
                    Linear objective = problem.getObjective() == null ? new Linear() : problem.getObjective();
                    objective.add(1, lhs);
                    problem.setObjective(objective);
                    problem.setVarUpperBound(lhs, Integer.MAX_VALUE);
                    problem.setVarLowerBound(lhs, Integer.MIN_VALUE);
                    problem.setVarType(lhs, Double.class);
                }
            }

            Linear linear = new Linear();
            for (Pair<String, Integer> p : lhsList) {
                linear.add(p.getSecond(), p.getFirst());
            }

            problem.add(linear, operator, rhs);
        }

        problem.setObjective(problem.getObjective(), OptType.MAX);
        final Solver maxSolver = factory.get();
        final Result maxResult = maxSolver.solve(problem);
        problem.setObjective(problem.getObjective(), OptType.MIN);
        final Solver minSolver = factory.get();
        final Result minResult = minSolver.solve(problem);
        final boolean hasConflict = maxResult == null || minResult == null;
        System.out.println("@solver  Max result :" + maxResult + " for " + problem.getConstraints());
        System.out.println("@solver  Min result :" + minResult + " for " + problem.getConstraints());
        System.out.println("@solver  Has conflict :" + hasConflict);
        HashMap<String, Integer> innerMin = new HashMap<>();
        HashMap<String, Integer> innerMax = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> outer = new HashMap<>();

        for (Object s : problem.getVariables()) {
            if (!(s instanceof String)) {
                continue;
            }

            String k = s.toString();
            if (minResult == null || maxResult == null) {
                return null;
            }

            if (minResult.get(k) == null || maxResult.get(k) == null) {
                continue;
            }

            Integer vMin = minResult.get(k).intValue();
            Integer vMax = maxResult.get(k).intValue();

            innerMin.put(k, vMin);
            innerMax.put(k, vMax);

        }

        outer.put("min", innerMin);
        outer.put("max", innerMax);

        return outer;

    }

    public static boolean checkRelated(final HashMap<String, HashMap<Integer, Pair<String, String>>> acMap,
                                       final HashMap<String, HashMap<Integer, Pair<String, String>>> solverMap,
                                       final HashMap<String, HashMap<Integer, Pair<String, String>>> postActivation,
                                       final String activityName,
                                       final HashSet<String> conflictSets,
                                       String activationConditions) {

        boolean secondaryConflict = false;

        final HashMap<Integer, Pair<String, String>> equationsList = postActivation.get(activityName);

        try {
            HashMap<Integer, Pair<String, String>> actMapSet = acMap.get(activityName);

            if (actMapSet == null || equationsList == null) {
                return false;
            }
            //System.out.println("--------------checkRelated-------------");

            String exP = joinExpressionsInList(equationsList);
            exP = activationConditions.isEmpty() ? exP : activationConditions + " & " + exP;
            //exP = exP.replace("T.", "A.");
            //System.out.println("=== previous condition" + exP);
            BooleanExpression expTree1 = getBooleanExpression(exP);

            ArrayList<ArrayList<BooleanExpression>> problemSets1 = expTree1.interpret();

            HashMap<String, Integer> minMap = new HashMap<>();
            HashMap<String, Integer> maxMap = new HashMap<>();
            for (ArrayList<BooleanExpression> problemSet : problemSets1) {
                HashMap<String, HashMap<String, Integer>> resultMap = LpSolverUtil.getResults(problemSet);

                if (resultMap == null || resultMap.get("min") == null || resultMap.get("max") == null) {
                    // this should not happen
                    return false;
                }

                //System.out.println("Result for min: " + resultMap.get("min"));
                //System.out.println("Result for max: " + resultMap.get("max"));

                for (String k : resultMap.get("min").keySet()) {
                    minMap.put(k, resultMap.get("min").get(k));
                }

                for (String k : resultMap.get("max").keySet()) {
                    maxMap.put(k, resultMap.get("max").get(k));
                }
            }

            boolean bLessA = true;
            boolean bGreatA = true;
            for (int k : actMapSet.keySet()) {
                String ma = actMapSet.get(k).getFirst();
                String s = actMapSet.get(k).getSecond();
                String equations = "( " + s + " )";

                //System.out.println(" RecursiveDescentParser: " + equations);

                BooleanExpression expTree2 = getBooleanExpression(equations);

                ArrayList<ArrayList<BooleanExpression>> problemSets = expTree2.interpret();

                for (ArrayList<BooleanExpression> problemSet : problemSets) {
                    HashMap<String, HashMap<String, Integer>> resultMap = LpSolverUtil.getResults(problemSet);

                    if (resultMap == null || resultMap.get("min") == null || resultMap.get("max") == null) {
                        // this should not happen
                        return false;
                    }
                    System.out.println("Result for min: " + resultMap.get("min"));
                    System.out.println("Result for max: " + resultMap.get("max"));

                    for (String ki : resultMap.get("min").keySet()) {
                        Integer i = minMap.get(ki);
                        if (i == null) {
                            continue;
                        }
                        Integer j = resultMap.get("min").get(ki);

                        bLessA = bLessA && j <= i;

                    }

                    for (String ki : resultMap.get("max").keySet()) {
                        Integer i = maxMap.get(ki);
                        if (i == null) {
                            continue;
                        }
                        Integer j = resultMap.get("max").get(ki);

                        bGreatA = bGreatA && j >= i;
                    }
                }


                //System.out.println("Can activate another constrain " + bLessA + " " + bGreatA + " " + (bGreatA && bLessA));

                if ((bGreatA && bLessA)) {

                    String passForward = activationConditions.isEmpty() ? equations : activationConditions + " & " + equations;

                    for (String act : solverMap.keySet()) {
                        HashMap<Integer, Pair<String, String>> lm = solverMap.get(act);
                        if (lm == null) {
                            continue;
                        }

                        boolean checkNext = false;
                        for (int ks : lm.keySet()) {
                            String a = lm.get(ks).getFirst();
                            String c = lm.get(ks).getSecond();

                            if (!a.equals(activityName)) {
                                continue;
                            }

                            if (postActivation.get(act) != null && postActivation.get(act).get(ks) != null) {
                                continue;
                            } else if (postActivation.get(act) == null) {
                                HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                h.put(ks, new Pair<>(a, c));
                                postActivation.put(act, h);
                            } else {
                                postActivation.get(act).put(ks, new Pair<>(a, c));
                            }

                            if (LpSolverUtil.solverForConflict(postActivation.get(act), exP)) {
                                secondaryConflict = true;
                                conflictSets.add(act);
                            }

                            checkNext = true;

                        }

                        if (checkNext) {
                            //System.out.println("---- condition forwarded ---" +passForward );
                            if(checkRelated(acMap, solverMap, postActivation, act, conflictSets, passForward)){
                                secondaryConflict = true;
                                conflictSets.add(act);
                                System.out.println("FOUND SECONDARY CONFLICT" + conflictSets );
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return secondaryConflict;
    }

    private static String joinExpressionsInList(final HashMap<Integer, Pair<String, String>> equationsList) {
        String exP = "(";

        if (equationsList.size() > 1) {
            for (int key : equationsList.keySet()) {
                if(equationsList.get(key) == null){
                    continue;
                }

                String current = equationsList.get(key).getSecond();



                if (current != null && !current.isEmpty()) {
                    if (exP.equals("(")) {
                        exP = exP + current;
                    } else {
                        exP = exP + ") && (" + current;
                    }
                }
            }
        } else {
            for (int key : equationsList.keySet()) {
                String current = equationsList.get(key).getSecond();
                if (!current.isEmpty()) {
                    exP = exP + current;
                }
            }
        }
        exP = exP + " )";

        return exP;
    }

    private static BooleanExpression getBooleanExpression(String exp) {
        final Lexer lexer = new Lexer(new ByteArrayInputStream(exp.getBytes()));
        final RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
        return parser.build();
    }

}
