package org.processmining.onlinedeclareanalyzer.lpsolver.ast;

public abstract class Terminal implements BooleanExpression{
	protected String value;

	public Terminal(String value) {
		this.value = value;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String toString() {
		return String.format("%s", value);
	}
}
