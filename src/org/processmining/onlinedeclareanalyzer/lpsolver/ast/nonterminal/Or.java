package org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.NonTerminal;

import java.util.ArrayList;

public class Or extends NonTerminal {

	public ArrayList<ArrayList<BooleanExpression>> interpret() {
		ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
		ArrayList<ArrayList<BooleanExpression>> l = left.interpret();
		ArrayList<ArrayList<BooleanExpression>> r = right.interpret();

		out.addAll(l);
		out.addAll(r);
		return out;
	}

    @Override
    public ArrayList<ArrayList<BooleanExpression>> invertInterpret() {
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        ArrayList<ArrayList<BooleanExpression>> l = left.invertInterpret();
        ArrayList<ArrayList<BooleanExpression>> r = right.invertInterpret();

        for (ArrayList<BooleanExpression> aL : l) {
            for (ArrayList<BooleanExpression>aR : r) {
                ArrayList<BooleanExpression> s = new ArrayList<>();
                s.addAll(aL);
                s.addAll(aR);
                out.add(s);
            }
        }
        return out;
    }

    @Override
    public ArrayList<String> traverse() {
        ArrayList<String> out = new ArrayList<>();
        ArrayList<String> l = left.traverse();
        ArrayList<String> r = right.traverse();

        out.addAll(l);

        out.addAll(r);
        return out;
    }

    @Override
    public ArrayList<String> invertTraverse() {
        ArrayList<String> out = new ArrayList<>();
        ArrayList<String> l = left.invertTraverse();
        ArrayList<String> r = right.invertTraverse();

        for (String aL : l) {
            for (String aR : r) {
                String s = aL + " & " + aR;
                out.add(s);
            }
        }
        return out;
    }

    public String toString() {
		return String.format("(%s | %s)", left, right);
	}
}
