package org.processmining.onlinedeclareanalyzer.lpsolver.ast.terminal;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.Terminal;

import java.util.ArrayList;

/**
 * Created by Ubaier on 02/04/2016.
 */
public class TerminalValue extends Terminal {

    public TerminalValue() {
        super("");
    }

    public TerminalValue(String value) {
        super(value);
    }

    @Override
    public ArrayList<ArrayList<BooleanExpression>> interpret() {
        ArrayList<BooleanExpression> list = new ArrayList<>();
        list.add(this);
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        out.add(list);
        return out;
    }

    @Override
    public ArrayList<ArrayList<BooleanExpression>> invertInterpret() {
        ArrayList<BooleanExpression> list = new ArrayList<>();
        list.add(this);
        ArrayList<ArrayList<BooleanExpression>> out = new ArrayList<>();
        out.add(list);
        return out;
    }

    @Override
    public ArrayList<String> traverse() {
        ArrayList<String> out = new ArrayList<>();
        out.add(this.toString());
        return out;
    }

    @Override
    public ArrayList<String> invertTraverse() {
        ArrayList<String> out = new ArrayList<>();
        out.add(this.toString());
        return out;
    }


}
