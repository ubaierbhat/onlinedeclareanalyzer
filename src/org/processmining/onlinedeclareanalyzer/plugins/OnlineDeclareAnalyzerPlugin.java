package org.processmining.onlinedeclareanalyzer.plugins;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XExtendedEvent;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.*;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XEventImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.framework.util.Pair;
import org.processmining.onlinedeclareanalyzer.data.*;
import org.processmining.onlinedeclareanalyzer.lpsolver.LpSolverUtil;
import org.processmining.onlinedeclareanalyzer.replayers.*;
import org.processmining.operationalsupport.provider.AbstractProvider;
import org.processmining.operationalsupport.provider.Provider;
import org.processmining.operationalsupport.server.OSService;
import org.processmining.operationalsupport.session.Session;
import org.processmining.plugins.declare.visualizing.*;

import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.*;

/**
 * Online Declare Analyzer Plugin for ProM Operations Support
 *
 * @author Ubaier Ahmad Bhat
 */

@Plugin(
        name = "Online Declare Analyzer",
        parameterLabels = {"Operational Support Service",
                "Declare Model",
                "Weighted Declare Model"},
        returnLabels = {"Online Declare Analyzer"},
        returnTypes = {OnlineDeclareAnalyzerPlugin.class}, userAccessible = true)
public class OnlineDeclareAnalyzerPlugin extends AbstractProvider {

    public static final String PLUGIN_NAME = "Online Declare Analyzer";


    public static final String STATE_PERMANENTLY_SATISFIED = "sat";
    public static final String STATE_POSSIBLY_SATISFIED = "poss.sat";
    public static final String STATE_PERMANENTLY_VIOLATED = "viol";
    public static final String STATE_POSSIBLY_VIOLATED = "poss.viol";
    public static final String STATE_CONFLICT = "conflict";
    public static final String STATE_UNKNOWN = "unknown";

    public static final String SESSION_KEY_REPLAYS = "replays";
    public static final String SESSION_KEY_FORMULA_SET = "formulasSet";
    public static final String SESSION_KEY_STATE_OBJECT = "stateObject";
    public static final String SESSION_KEY_WEIGHTS = "weights";
    public static final String SESSION_KEY_TIME_WINDOW = "timeWindow";


    public static final String OUTPUT_KEY_POSITIVE = "positive";
    public static final String OUTPUT_KEY_NEGATIVE = "negative";
    public static final String OUTPUT_KEY_FLUENTS = "fluents";
    public static final String OUTPUT_KEY_HEALTH = "health";

    private static final boolean CONFLICT_DETECTION = true;
    XAttributeMap eventAttributeMap;


    public OnlineDeclareAnalyzerPlugin(OSService owner) {
        super(owner);
    }

    @Override
    public String getName() {
        return PLUGIN_NAME;
    }


    @UITopiaVariant(
            affiliation = UITopiaVariant.EHV,
            author = "Ubaier Bhat, F.M. Maggi",
            email = "ubaier.bhat@ut.ee , f.m.maggi@ut.ee ",
            uiLabel = PLUGIN_NAME,
            pack = "OnlineDeclareAnalyzer")
    @PluginVariant(variantLabel = PLUGIN_NAME,
            requiredParameterLabels = {0})
    public static Provider registerServiceProviderAUI(final UIPluginContext context, OSService service) {
        return registerServiceProviderA(context, service);
        //In the final version, it will use a translator plug-in Climb->Event Calculus, and then simply call the other variant!
    }

    public static Provider registerServiceProviderA(final PluginContext context, OSService service) {

        try {
            OnlineDeclareAnalyzerPlugin provider = new OnlineDeclareAnalyzerPlugin(service);
            context.getFutureResult(0).setLabel(context.getFutureResult(0).getLabel() + " on port " + service);
            return provider;
        } catch (Exception e) {
            context.log(e);
        }
        return null;
    }

    public boolean accept(final Session session, final List<String> modelLanguages, final List<String> queryLanguages,
                          Object model) {
        if (modelLanguages.equals(DeclareLanguage.MIME_TYPE)) {
            return false;
        }
        if (!matchesLanguage(queryLanguages, DeclareMonitorQuery.MIME_TYPE)) {
            return false;
        }

        System.out.format("+---------------------------+%n");
        System.out.format("|  Online Declare Analyzer  |%n");
        System.out.format("+---------------------------+%n");
        System.out.format("|      Receiving Model      |%n");
        System.out.format("+---------------------------+%n");

        final long startTime = System.nanoTime();
        Hashtable replays = new Hashtable();
        session.put(SESSION_KEY_REPLAYS, replays);
        try {
            SAXBuilder sb = new SAXBuilder();
            DOMOutputter outputter = new DOMOutputter();
            Document xesDocument = sb.build(new StringReader((String) model));
            XMLOutputter xmlOutput = new XMLOutputter();

            // display in pretty format
            xmlOutput.setFormat(Format.getPrettyFormat());
            File modelFile = File.createTempFile("model", ".xml");
            modelFile.deleteOnExit();
            xmlOutput.output(xesDocument, new FileWriter(modelFile));

            AssignmentViewBroker broker = XMLBrokerFactory.newAssignmentBroker(modelFile.getAbsolutePath());
            AssignmentModel amodel = broker.readAssignment();

            String[] constraints = getConstraints(amodel);
            String[] forms = new String[amodel.constraintDefinitionsCount()];

            FormulasSet fs = new FormulasSet();
            fs.setModel(amodel);
            session.put(SESSION_KEY_FORMULA_SET, fs);

            String[][] matrix = new String[constraints.length][1];
            fs.setConstraints(constraints);

            StateObject currentState = new StateObject();
            currentState.setMatrix(matrix);
            currentState.prepareDataStructure(amodel);
            currentState.buildLPSolver(amodel, constraints);
            currentState.setEventPos(-1);
            currentState.setHealth(1);
            currentState.setHealthWindow(new Vector());
            session.put(SESSION_KEY_STATE_OBJECT, currentState);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final long endTime = System.nanoTime();

        final long duration = (endTime - startTime) / 1000000 ;  //divide by 1000000 to get milliseconds

        System.out.println(" \t $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println(" \t Online Declare Analyzer took: " + duration + " milliseconds to prepare model" );
        System.out.println(" \t $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        return true;
    }


    public <R, L> R simple(final Session session, final XLog availableItems, final String langauge, final L query,
                           final boolean done) throws Exception {

        if ((query == null) || !(query instanceof String)) {
            return null;
        }
        if (!langauge.equals(DeclareMonitorQuery.MIME_TYPE)) {
            return null;
        }

        System.out.println("*****************Online Declare Analyzer*****************");
        System.out.println("*********************Receiving Trace*********************");
        System.out.println("**********************Session Info***********************");
        System.out.println("\t Session id: " + session.getId() + "\t Query id: " + query);
        System.out.println("\t Total number of sessions: " + this.getOwner().getSessions().size());
        System.out.println("*********************************************************");

        final long startTime = System.nanoTime();

        Hashtable output = null;


        boolean coloured = true;

        XTrace lastTrace = availableItems.get(0);

        if (lastTrace == null || lastTrace.size() == 0) {
            System.out.println("*********************Last Trace IS NULL **********************");
            return null;
        }

        // Attach attributes to last trace
        addAttributeToTrace(lastTrace, "concept:name", query.toString());

        Hashtable replays = (Hashtable) session.get(SESSION_KEY_REPLAYS);
        String systemID = (String) query;
        XTrace xTraceReplay = null;

        if (replays.containsKey(systemID)) {
            xTraceReplay = (XTrace) replays.get(systemID);
        } else {
            xTraceReplay = new XTraceImpl(new XAttributeMapImpl());
        }

        // Formula cet
        FormulasSet fs = (FormulasSet) session.get(SESSION_KEY_FORMULA_SET);
        AssignmentModel assignmentModel = fs.getModel();

        String[] constraints = fs.getConstraints();
        String positive = "";
        String negative = "";

        StateObject currentState = (StateObject) session.get(SESSION_KEY_STATE_OBJECT);
        boolean data = currentState.hasData();

        Set<String> candidateActivations = currentState.getCandidateActivations();

        String[][] matrix = null;
        Vector weights = session.getObject(SESSION_KEY_WEIGHTS);
        double health = -1;
        Vector<Double> healthWindow = currentState.getHealthWindow();

        double eventAffection = 0;
        int startingIndex = session.getCompleteTrace().size() - 5;
        if (startingIndex < 0) {
            startingIndex = 0;
        }
        int evPos;

        evPos = currentState.getEventPos() + 1;
        currentState.setEventPos(evPos);
        matrix = new String[assignmentModel.getConstraintDefinitions().size()][evPos + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < (matrix[0].length - 1); j++) {
                matrix[i][j] = currentState.getMatrix()[i][j];
            }
        }


        HashMap<String, DeclareReplayer> replayers = currentState.getReplayers();

        OnlineTraceReaderAndReplayer readerReplayer = currentState.getReaderReplayer();


        DataSnapshotListener listener = null;
        XTrace completeTrace = null;

        if (data) {
            try {
                if (readerReplayer == null) {
                    readerReplayer = new OnlineTraceReaderAndReplayer(lastTrace);
                    listener = new DataSnapshotListener(readerReplayer.getDataTypes(), readerReplayer.getActivityLabels());
                    readerReplayer.addReplayerListener(listener);
                    readerReplayer.replayLog(candidateActivations);
                    completeTrace = readerReplayer.getTrace();
                } else {
                    listener = (DataSnapshotListener) readerReplayer.getListner(); // currentState.getDataSnapshotListner();

                    if (listener == null) {
                        System.out.print("listener == null");
                        return null;
                    }

                    readerReplayer.replayTrace(candidateActivations, lastTrace);
                    completeTrace = readerReplayer.getTrace();

                }


            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        HashMap<String, HashMap<Integer, Pair<String, String>>> solverMap = deepCloneMap1(currentState.getSolverMap());
        HashMap<String, HashMap<Integer, Pair<String, String>>> negativeTemplateSet = currentState.getPostActivation();
        HashMap<String, HashMap<Integer, Pair<String, String>>> acMap = deepCloneMap1(currentState.getActivationMap());

        HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activePositiveTemps = currentState.getActivePositiveTemps();
        HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activeNegativeTemps = currentState.getActiveNegativeTemps();
        try {
            for (XEvent e : lastTrace) {

                XEvent completeEvent = new XEventImpl();
                XExtendedEvent eva = XExtendedEvent.wrap(e);
                XTimeExtension.instance().assignTimestamp(completeEvent, eva.getTimestamp());
                XConceptExtension.instance().assignName(completeEvent, eva.getName());

                String evName;

                evName = ((XAttributeLiteral) e.getAttributes().get("concept:name")).getValue().toLowerCase().replace(" ", "_") + "_complete";

                String traceId = XConceptExtension.instance().extractName(lastTrace);

                System.out.println(" processing traceID: " + traceId);

                String event = XConceptExtension.instance().extractName(e);
                String transitionType = XLifecycleExtension.instance().extractTransition(e);
                if (transitionType == null) {
                    event += "-complete";
                } else {
                    event += "-" + transitionType;
                }

                System.out.println("\t Event position: " + evPos + "\t Activity name: " + event);
                System.out.println("*********************************************************");
                for (DeclareReplayer replayer : replayers.values()) {
                    System.out.println("\t\t\t replayer.process( " + event.toLowerCase() + " transitionType: " + transitionType + " traceid: " + traceId);
                    System.out.println("*********************************************************");
                    replayer.process(evPos, event.toLowerCase(), completeTrace, traceId, listener);
                }

                int cdPos = 0;

                HashSet<String> conflictSets = new HashSet<>();

                for (ConstraintDefinition cd : assignmentModel.getConstraintDefinitions()) {
                    ArrayList<String> disposs = new ArrayList<>();
                    for (Parameter p : cd.getParameters()) {
                        for (ActivityDefinition b : cd.getBranches(p)) {
                            String bName = b.getName().toLowerCase();

                            if ((!bName.contains("-assign") && !bName.contains("-ate_abort") && !bName.contains("-suspend") && !bName.contains("-complete") && !bName.contains("-autoskip") && !bName.contains("-manualskip") && !bName.contains("pi_abort") && !bName.contains("-reassign") && !bName.contains("-resume") && !bName.contains("-schedule") && !bName.contains("-start") && !bName.contains("-unknown") && !bName.contains("-withdraw")) && (!bName.contains("<center>assign") && !bName.contains("<center>ate_abort") && !bName.contains("<center>suspend") && !bName.contains("<center>complete") && !bName.contains("<center>autoskip") && !bName.contains("<center>manualskip") && !bName.contains("<center>pi_abort") && !bName.contains("<center>reassign") && !bName.contains("<center>resume") && !bName.contains("<center>schedule") && !bName.contains("<center>start") && !bName.contains("<center>unknown") && !bName.contains("<center>withdraw"))) {
                                disposs.add(bName + "-complete");
                            } else {
                                disposs.add(bName);
                            }
                        }

                    }

                    String cdName = cd.getName().toLowerCase();
                    String dispEvent = disposs.get(0);


                    // add result to matrix
                    // System.out.println("#SetsOfConditions# activePositiveTemps set at event pos: " + evPos + "  cdPos: " + cdPos + " before analysis " + activePositiveTemps.toString());
                    // System.out.println("#SetsOfConditions# negativeTemplateSet set at event pos: " + evPos + "  cdPos: " + cdPos + " before analysis " + negativeTemplateSet.toString());

                    String state =  analyse(
                            evPos,
                            cdPos,
                            evName,
                            cdName,
                            replayers,
                            traceId,
                            dispEvent,
                            cd,
                            disposs,
                            done,
                            solverMap,
                            acMap,
                            negativeTemplateSet,
                            conflictSets,
                            activePositiveTemps,
                            activeNegativeTemps,
                            listener);

                    matrix[cdPos][evPos] = state;
                    System.out.println("#SetsOfConditions# activePositiveTemps set at event pos: " + evPos + "  cdPos: " + cdPos + " after analysis " + activePositiveTemps.toString());
                    System.out.println("#SetsOfConditions# negativeTemplateSet set at event pos: " + evPos + "  cdPos: " + cdPos + " after analysis " + negativeTemplateSet.toString());
                    // increment constraint index
                    cdPos++;


                }





                Vector<Double> previousHealth = new Vector<>();
                for (int i = 0; i < matrix.length; i++) {
                    double ph = 1.0;
                    if (healthWindow.size() == matrix.length) {
                        ph = healthWindow.get(i);
                    }
                    previousHealth.add(ph);
                }


                Vector<Double> constrainHealth = new Vector<>();
                for (int i = 0; i < matrix.length; i++) {
                    String m = matrix[i][evPos];
                    switch (m) {
                        case STATE_PERMANENTLY_VIOLATED:
                            constrainHealth.add(0.0);
                            break;
                        case STATE_POSSIBLY_VIOLATED:
                            constrainHealth.add(0.5);
                            break;
                        case STATE_CONFLICT:
                            constrainHealth.add(previousHealth.get(i));
                            break;
                        default:
                            constrainHealth.add(1.0);
                            break;
                    }
                }

                double temp = 0;
                for (int h = 0; h < weights.size(); h++) {
                    temp = temp + constrainHealth.get(h) * new Double((String) weights.get(h)).doubleValue();
                }
                double sumOfWeights = 0;
                for (int l = 0; l < weights.size(); l++) {
                    sumOfWeights = sumOfWeights + new Double((String) weights.get(l)).doubleValue();
                }

                if (sumOfWeights != 0) {
                    health = temp / sumOfWeights;
                }

                healthWindow = new Vector<>(constrainHealth);

                for (String key : conflictSets) {
                    HashMap<Integer, Pair<String, String>> hm = solverMap.get(key);
                    if (hm == null) {
                        continue;
                    }
                    for (int i : hm.keySet()) {
                        matrix[i][evPos] = STATE_CONFLICT;
                    }
                }


                // Create results string
                System.out.println("*********************************************************");

                String result = getResult(matrix, constraints, completeTrace);


//                System.out.println("Returning:"
//                        + " \n " + OUTPUT_KEY_POSITIVE + " : " + positive
//                        + " \n " + OUTPUT_KEY_NEGATIVE + ": " + negative
//                        + " \n " + OUTPUT_KEY_FLUENTS + ": " + result.replace("mholds_for", "\nmholds_for")
//                        + " \n " + OUTPUT_KEY_HEALTH + ": " + health);
                output = new Hashtable();
                output.put(OUTPUT_KEY_POSITIVE, positive);
                output.put(OUTPUT_KEY_NEGATIVE, negative);
                output.put(OUTPUT_KEY_FLUENTS, result);
                output.put(OUTPUT_KEY_HEALTH, health);

            }


        /*
        matrix constrain , even
        If full.size >= 1;
            matrix[0][currentEvent] = "sat"


         */

            // Update session before exit
            currentState.setMatrix(matrix);
            currentState.setEventPos(evPos);
            currentState.setHealth(health);
            currentState.setHealthWindow(healthWindow);
            currentState.setReaderReplayer(readerReplayer);
            session.put(SESSION_KEY_STATE_OBJECT, currentState);

        } catch (Exception e) {
            e.printStackTrace();
            for(int i = 0; i <matrix.length ; i++ ) {
                matrix[i][evPos] = STATE_UNKNOWN;
            }
            String result = getResult(matrix, constraints, completeTrace);
            output.put(OUTPUT_KEY_POSITIVE, positive);
            output.put(OUTPUT_KEY_NEGATIVE, negative);
            output.put(OUTPUT_KEY_FLUENTS, result);
            output.put(OUTPUT_KEY_HEALTH, health);
        }

        final long endTime = System.nanoTime();

        final long duration = (endTime - startTime) / 1000000 ;  //divide by 1000000 to get milliseconds

        System.out.println(" \t $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println(" \t Online Declare Analyzer took: " + duration + " milliseconds " + " for Session: " + session.getId() + "\t Query id: " + query + " event no: " + evPos );
        System.out.println(" \t $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        return (R) output;
    }


    private String convert(XTrace trace, int pos) {
        XExtendedEvent ev = XExtendedEvent.wrap(trace.get(pos));
        return "" + ev.getTimestamp().getTime();
    }

    /**
     * Example of a constrain string"response([Receive Payment],[Receive Order],),poss.sat),[1253770511100,inf])]
     *
     * @param model AssignmentModel
     * @return a array of strings with constraints
     */
    private String[] getConstraints(AssignmentModel model) {
        System.out.println("getConstraints()");
        final String[] constraints = new String[model.constraintDefinitionsCount()];
        int i = 0;
        for (ConstraintDefinition cd : model.getConstraintDefinitions()) {
            final String name = cd.getName();
            final String caption = cd.getCaption();
            final String condition = cd.getCondition().toString();
            final String constraint = name + "(" + caption.substring(caption.indexOf("[")) + ", " + condition + ")";
            constraints[i] = constraint;
            i++;
        }
        return constraints;
    }


    private String analyse(int evPos,
                           int cdPos,
                           final String evName,
                           final String cdName,
                           HashMap<String, DeclareReplayer> replayers,
                           String traceId, String dispEvent,
                           ConstraintDefinition cd,
                           ArrayList<String> disposs,
                           boolean done,

                           final HashMap<String, HashMap<Integer, Pair<String, String>>> solverMap,
                           final HashMap<String, HashMap<Integer, Pair<String, String>>> acMap,
                           final HashMap<String, HashMap<Integer, Pair<String, String>>> negativeTemplatesSet,
                           final HashSet<String> conflictSets,
                           final HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activePositiveTemps,
                           final HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> activeNegativeTemps,
                           final DataSnapshotListener listener) {
        String analysis = STATE_UNKNOWN;

        boolean activated = false;
        boolean updatedPermenent = false;
        boolean emptyBin = false;
        boolean usePending = false;
        boolean useActivations = false;
        boolean useFullfilments = false;

        Set<Integer> pendingActivations = null;
        Set<Integer> fulfillments = null;
        Set<Integer> activations = null;
        Set<Integer> violations = null;

        switch (cdName) {
            case ConstraintName.RESPONSE: {
                ResponseAnalyzer replayer = (ResponseAnalyzer) replayers.get(ConstraintName.RESPONSE);
                pendingActivations = replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(pendingActivations);

                if (pendingActivations.size() > 0) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                if (pendingActivations.size() > 0) {
                    activated = true;
                } else {
                    emptyBin = true;
                    usePending = true;
                }
                break;
            }
            case ConstraintName.NOT_RESPONSE: {
                NotResponseAnalyzer replayer = (NotResponseAnalyzer) replayers.get(ConstraintName.NOT_RESPONSE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                pendingActivations = replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(pendingActivations);
                activations.addAll(violations);


                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                if (pendingActivations.size() > 0) {
                    activated = true;
                }

                updatedPermenent = true;

                break;
            }
            case ConstraintName.PRECEDENCE: {
                PrecedenceAnalyzer replayer = (PrecedenceAnalyzer) replayers.get(ConstraintName.PRECEDENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;
                break;
            }
            case ConstraintName.NOT_PRECEDENCE: {
                NotPrecedenceAnalyzer replayer = (NotPrecedenceAnalyzer) replayers.get(ConstraintName.NOT_PRECEDENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;
                updatedPermenent = true;
                break;
            }
            case ConstraintName.INIT: {
                InitAnalyzer replayer = (InitAnalyzer) replayers.get(ConstraintName.INIT);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (fulfillments.size() > 0) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;

                break;
            }
            case ConstraintName.STRONG_INIT: {
                InitAnalyzer replayer = (InitAnalyzer) replayers.get(ConstraintName.STRONG_INIT);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (fulfillments.size() > 0) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                break;
            }
            case ConstraintName.ABSENCE: {
                AbsenceAnalyzer replayer = (AbsenceAnalyzer) replayers.get(ConstraintName.ABSENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = new HashSet<>();
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;
                break;
            }
            case ConstraintName.ABSENCE2: {
                Absence2Analyzer replayer = (Absence2Analyzer) replayers.get(ConstraintName.ABSENCE2);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                if (fulfillments.size() > 0) {
                    activated = true;
                }
                activated = false;
                updatedPermenent = false;
                break;
            }
            case ConstraintName.ABSENCE3: {
                Absence3Analyzer replayer = (Absence3Analyzer) replayers.get(ConstraintName.ABSENCE3);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;
                updatedPermenent = false;
                break;
            }
            case ConstraintName.EXISTENCE: {
                ExistenceAnalyzer replayer = (ExistenceAnalyzer) replayers.get(ConstraintName.EXISTENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                final int limit = 1;
                if (fulfillments.size() >= limit) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                if (fulfillments.size() < 1) {
                    activated = true;
                } else {
                    emptyBin = true;
                    useFullfilments = true;
                }

                break;
            }
            case ConstraintName.EXISTENCE2: {
                Existence2Analyzer replayer = (Existence2Analyzer) replayers.get(ConstraintName.EXISTENCE2);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                final int limit = 2;
                if (fulfillments.size() >= limit) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = false;

                break;
            }
            case ConstraintName.EXISTENCE3: {
                Existence3Analyzer replayer = (Existence3Analyzer) replayers.get(ConstraintName.EXISTENCE3);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                final int limit = 3;
                if (fulfillments.size() >= limit) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = false;

                break;
            }
            case ConstraintName.EXACTLY1: {
                Exactly1Analyzer replayer = (Exactly1Analyzer) replayers.get(ConstraintName.EXACTLY1);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                final int limit = 1;
                if (violations.size() == 0 && fulfillments.size() < limit) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                } else if (violations.size() == 0 && fulfillments.size() == limit) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }
                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = false;

                break;
            }
            case ConstraintName.EXACTLY2: {
                Exactly2Analyzer replayer = (Exactly2Analyzer) replayers.get(ConstraintName.EXACTLY2);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString());
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString());
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                final int limit = 2;
                if (violations.size() == 0 && fulfillments.size() < limit) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                } else if (violations.size() == 0 && fulfillments.size() == limit) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }
                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = false;

                break;
            }
            case ConstraintName.RESPONDED_EXISTENCE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.RESPONDED_EXISTENCE);
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                violations = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations.addAll(violations);
                activations.addAll(fulfillments);

                if (fulfillments.size() == activations.size()) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (fulfillments.size() < activations.size()) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                if (fulfillments.size() < activations.size()) {
                    activated = true;
                } else {
                    emptyBin = true;
                    useFullfilments = true;
                }

                break;
            }
            case ConstraintName.NOT_RESPONDED_EXISTENCE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.NOT_RESPONDED_EXISTENCE);
                fulfillments = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                violations = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations.addAll(violations);
                activations.addAll(fulfillments);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                if (fulfillments.size() > 0) {
                    activated = true;
                }
                updatedPermenent = true;
                break;
            }
            case ConstraintName.SUCCESSION: {
                ResponseAnalyzer replayerResponse = (ResponseAnalyzer) replayers.get(ConstraintName.SUCCESSION_RESPONSE);
                PrecedenceAnalyzer replayerPrecedence = (PrecedenceAnalyzer) replayers.get(ConstraintName.SUCCESSION_PRECEDENCE);
                violations = replayerResponse.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(replayerPrecedence.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments = replayerResponse.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(replayerPrecedence.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0 && activations.size() == fulfillments.size()) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() == 0 && activations.size() > fulfillments.size()) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                break;
            }
            case ConstraintName.NOT_SUCCESSION: {
                NotResponseAnalyzer replayerResponse = (NotResponseAnalyzer) replayers.get(ConstraintName.NOT_SUCCESSION_RESPONSE);
                NotPrecedenceAnalyzer replayerPrecedence = (NotPrecedenceAnalyzer) replayers.get(ConstraintName.NOT_SUCCESSION_PRECEDENCE);
                violations = replayerResponse.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(replayerPrecedence.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments = replayerResponse.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(replayerPrecedence.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }
                activated = false;
                updatedPermenent = true;
                break;
            }
            case ConstraintName.CHAIN_RESPONSE: {
                ChainResponseAnalyzer replayer = (ChainResponseAnalyzer) replayers.get(ConstraintName.CHAIN_RESPONSE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                pendingActivations = replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (pendingActivations.size() > 0 && violations.size() == 0) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (pendingActivations.size() == 0 && violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }
                if (pendingActivations.size() > 0) {
                    activated = true;
                } else {
                    emptyBin = true;
                    usePending = true;
                }
                break;
            }
            case ConstraintName.NOT_CHAIN_RESPONSE: {
                NotChainResponseAnalyzer replayer = (NotChainResponseAnalyzer) replayers.get(ConstraintName.NOT_CHAIN_RESPONSE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                pendingActivations = replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                if (pendingActivations.size() > 0) {
                    activated = true;
                } else {
                    emptyBin = true;
                    usePending = true;
                }
                updatedPermenent = true;
                break;
            }
            case ConstraintName.CHAIN_PRECEDENCE: {
                ChainPrecedenceAnalyzer replayer = (ChainPrecedenceAnalyzer) replayers.get(ConstraintName.CHAIN_PRECEDENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;

                break;
            }
            case ConstraintName.NOT_CHAIN_PRECEDENCE: {
                NotChainPrecedenceAnalyzer replayer = (NotChainPrecedenceAnalyzer) replayers.get(ConstraintName.NOT_CHAIN_PRECEDENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;
                updatedPermenent = true;
                break;
            }
            case ConstraintName.NOT_CHAIN_SUCCESSION: {
                NotChainResponseAnalyzer replayerResponse = (NotChainResponseAnalyzer) replayers.get(ConstraintName.NOT_CHAIN_SUCCESSION_RESPONSE);
                NotChainPrecedenceAnalyzer replayerPrecedence = (NotChainPrecedenceAnalyzer) replayers.get(ConstraintName.NOT_CHAIN_SUCCESSION_PRECEDENCE);
                //violations = replayerResponse.getPendingActivations(traceId, param0, disposs.get(1));
                violations = replayerPrecedence.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(replayerResponse.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments = replayerResponse.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(replayerPrecedence.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments.addAll(replayerResponse.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = true;
                break;
            }
            case ConstraintName.CHAIN_SUCCESSION: {
                ChainResponseAnalyzer replayerResponse = (ChainResponseAnalyzer) replayers.get(ConstraintName.CHAIN_SUCCESSION_RESPONSE);
                ChainPrecedenceAnalyzer replayerPrecedence = (ChainPrecedenceAnalyzer) replayers.get(ConstraintName.CHAIN_SUCCESSION_PRECEDENCE);
                violations = replayerResponse.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(replayerResponse.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                violations.addAll(replayerPrecedence.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments = replayerResponse.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(replayerPrecedence.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0 && activations.size() == fulfillments.size()) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() == 0 && activations.size() > fulfillments.size()) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;

                break;
            }
            case ConstraintName.ALTERNATE_RESPONSE: {
                AlternateResponseAnalyzer replayer = (AlternateResponseAnalyzer) replayers.get(ConstraintName.ALTERNATE_RESPONSE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                pendingActivations = replayer.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (pendingActivations.size() > 0 && violations.size() == 0) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                } else if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (pendingActivations.size() == 0 && violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                if (pendingActivations.size() > 0) {
                    activated = true;
                } else {
                    emptyBin = true;
                    usePending = true;
                }
                break;
            }
            case ConstraintName.ALTERNATE_PRECEDENCE: {
                AlternatePrecedenceAnalyzer replayer = (AlternatePrecedenceAnalyzer) replayers.get(ConstraintName.ALTERNATE_PRECEDENCE);
                violations = replayer.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                }

                activated = false;

                break;
            }
            case ConstraintName.ALTERNATE_SUCCESSION: {
                AlternateResponseAnalyzer replayerResponse = (AlternateResponseAnalyzer) replayers.get(ConstraintName.ALTERNATE_SUCCESSION_RESPONSE);
                AlternatePrecedenceAnalyzer replayerPrecedence = (AlternatePrecedenceAnalyzer) replayers.get(ConstraintName.ALTERNATE_SUCCESSION_PRECEDENCE);
                violations = replayerResponse.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(replayerPrecedence.getViolations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments = replayerResponse.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                Set<Integer> pending = replayerResponse.getPendingActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(pending);
                fulfillments.addAll(replayerPrecedence.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0 && activations.size() == fulfillments.size()) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() == 0 && activations.size() > fulfillments.size()) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;

                break;
            }
            case ConstraintName.CO_EXISTENCE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.CO_EXISTENCE);
                RespondedExistenceAnalyzer invreplayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.INV_CO_EXISTENCE);
                violations = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(invreplayer.getActivations(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(invreplayer.getFulfillments(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0 && activations.size() == fulfillments.size()) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() == 0 && activations.size() > fulfillments.size()) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }


                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;

                break;
            }
            case ConstraintName.CHOICE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.CHOICE);
                RespondedExistenceAnalyzer invreplayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.INV_CHOICE);
                fulfillments = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(invreplayer.getActivations(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                fulfillments.addAll(replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1)));
                fulfillments.addAll(invreplayer.getFulfillments(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                //activations.addAll(violations);

                if (fulfillments.size() > 0) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;

                break;
            }
            case ConstraintName.NOT_CO_EXISTENCE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.NOT_CO_EXISTENCE);
                RespondedExistenceAnalyzer invreplayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.INV_NOT_CO_EXISTENCE);
                violations = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(invreplayer.getActivations(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(invreplayer.getFulfillments(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                updatedPermenent = false;
                break;
            }
            case ConstraintName.EXCLUSIVE_CHOICE: {
                RespondedExistenceAnalyzer replayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.EXCLUSIVE_CHOICE);
                RespondedExistenceAnalyzer invreplayer = (RespondedExistenceAnalyzer) replayers.get(ConstraintName.INV_EXCLUSIVE_CHOICE);
                violations = replayer.getActivations(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                violations.addAll(invreplayer.getActivations(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                fulfillments = replayer.getFulfillments(traceId, dispEvent + cd.getCondition().toString(), disposs.get(1));
                fulfillments.addAll(invreplayer.getFulfillments(traceId, disposs.get(1), dispEvent + cd.getCondition().toString()));
                activations = new HashSet<>();
                activations.addAll(fulfillments);
                activations.addAll(violations);

                if (violations.size() > 0) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                } else if (violations.size() == 0 && fulfillments.size() > 0) {
                    analysis = STATE_POSSIBLY_SATISFIED;
                } else if (violations.size() == 0 && fulfillments.size() == 0) {
                    analysis = STATE_POSSIBLY_VIOLATED;
                }

                if (done && analysis.equals(STATE_POSSIBLY_SATISFIED)) {
                    analysis = STATE_PERMANENTLY_SATISFIED;
                } else if (done && analysis.equals(STATE_POSSIBLY_VIOLATED)) {
                    analysis = STATE_PERMANENTLY_VIOLATED;
                }

                activated = false;
                break;
            }
        }
        System.out.println("\t\t Constrain name: " + cdName + " \t analysis: " + analysis + " \tisDone: " + done);
        System.out.println("\t\t isActivated : " + activated + " \t updatedPermenent: " + updatedPermenent +" \t emptyBin: " + emptyBin + " \tisDone: " + done);

        if (emptyBin) {

            Vector<String> empty = new Vector<>();
            Vector<Removable> emptyMe = new Vector<>();
            if (isUnaryConstraint(cdName) || updatedPermenent) {


                for (String k : negativeTemplatesSet.keySet()) {
                    HashMap<Integer, Pair<String, String>> r = negativeTemplatesSet.get(k);
                    if (r.get(cdPos) != null) {
                        r.remove(cdPos);
                    }
                    if (r.size() == 0) {
                        empty.add(k);
                    }
                }


                for (String k : empty) {
                    negativeTemplatesSet.remove(k);
                    System.out.println("Removing negative  condition cdPos: " + cdPos + " of " + k);
                }
            } else {

                for (String k : activePositiveTemps.keySet()) {
                    HashMap<Integer, HashMap<Integer, Pair<String, String>>> x = activePositiveTemps.get(k);

                    if (x == null) {
                        continue;
                    }

                    HashMap<Integer, Pair<String, String>> y = x.get(cdPos);

                    if (y == null) {
                        continue;
                    }

                    for (Integer i : y.keySet()) {

                        if (usePending && !pendingActivations.contains(i)) {
                            System.out.println("We need to remove this : " + i + "  should be removed from cdPos: " + cdPos + " of " + k);
                            emptyMe.add(new Removable(k, cdPos, i));
                        }

                        if (useFullfilments && fulfillments.contains(i)) {
                            System.out.println("We need to remove this : " + i + "  should be removed from cdPos: " + cdPos + " of " + k);
                            emptyMe.add(new Removable(k, cdPos, i));
                        }

                    }

                }

                for (Removable r : emptyMe) {
                    activePositiveTemps.get(r.getActivityName()).get(r.getCdPosition()).remove(r.getEventPosition());
                    System.out.println("We removed this : " + r.getEventPosition() + "  from cdPos: " + r.getCdPosition() + " of " + r.getActivityName());
                }


            }
        }

        if (CONFLICT_DETECTION && activated) {

            for (String act : solverMap.keySet()) {

                HashMap<Integer, Pair<String, String>> z = solverMap.get(act);

                if (z.get(cdPos) != null) {
                    String activity = z.get(cdPos).getFirst();
                    String equation = z.get(cdPos).getSecond();

                    if (activity.equals(evName)) {
                        if (listener.getInstances().get(dispEvent) != null) {
                            List<Pair<Integer, Map<String, String>>> snapshotsActivation = listener.getInstances().get(dispEvent).get(traceId);
                            Map<String, String> snapshotActivation = null;
                            Map<String, String> datavalues = new HashMap<String, String>();
                            for (Pair pair : snapshotsActivation) {
                                if ((Integer) pair.getFirst() == evPos) {
                                    snapshotActivation = (Map<String, String>) pair.getSecond();
                                    for (String attribute : snapshotActivation.keySet()) {
                                        datavalues.put(evName + "." + attribute, snapshotActivation.get(attribute));
                                    }
                                    break;
                                }
                            }


                            System.out.println("\t Equation for \"" + cdName + "\" \n\t " + evName + " before replace  \n\t = " + equation);


                            if (!isUnaryConstraint(cdName)) {
                                for (String key : datavalues.keySet()) {
                                    if (equation.contains(key)) {
                                        equation = equation.replace(key, datavalues.get(key));
                                    }
                                }
                            }

                            if (!equation.contains(evName + ".") || isUnaryConstraint(cdName)) {


                                if (updatedPermenent) {
                                    if (negativeTemplatesSet.get(act) == null) {

                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(cdPos, new Pair<>(activity, equation));

                                        negativeTemplatesSet.put(act, h);
                                    } else {
                                        HashMap<Integer, Pair<String, String>> h;
                                        h = negativeTemplatesSet.get(act);
                                        Pair<String, String> p = h.get(cdPos);
                                        String e = p != null ? p.getSecond() : "";
                                        String equation1 = e.isEmpty() ? equation : equation + " & " + e;
                                        negativeTemplatesSet.get(act).put(cdPos, new Pair<>(activity, equation1));
                                    }
                                }

                                if (updatedPermenent) {
                                    if (activeNegativeTemps.get(act) == null) {
                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(evPos, new Pair<>(activity, equation));
                                        HashMap<Integer, HashMap<Integer, Pair<String, String>>> cN = new HashMap<>();
                                        cN.put(cdPos, h);
                                        activeNegativeTemps.put(act, cN);
                                    } else if (activeNegativeTemps.get(act).get(cdPos) == null) {
                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(evPos, new Pair<>(activity, equation));
                                        activeNegativeTemps.get(act).put(cdPos, h);
                                    } else if (activeNegativeTemps.get(act).get(cdPos).get(evPos) == null) {
                                        activeNegativeTemps.get(act).get(cdPos).put(evPos, new Pair<>(activity, equation));
                                    }
                                } else {
                                    if (activePositiveTemps.get(act) == null) {
                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(evPos, new Pair<>(activity, equation));
                                        HashMap<Integer, HashMap<Integer, Pair<String, String>>> cN = new HashMap<>();
                                        cN.put(cdPos, h);
                                        activePositiveTemps.put(act, cN);
                                    } else if (activePositiveTemps.get(act).get(cdPos) == null) {
                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(evPos, new Pair<>(activity, equation));
                                        activePositiveTemps.get(act).put(cdPos, h);
                                    } else if (activePositiveTemps.get(act).get(cdPos).get(evPos) == null) {
                                        activePositiveTemps.get(act).get(cdPos).put(evPos, new Pair<>(activity, equation));
                                    }
                                }


                                HashMap<String, HashMap<Integer, Pair<String, String>>> sol = deepCloneMap1(negativeTemplatesSet);
                                HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> temporaryActivePositiveTemps = deepCloneMap2(activePositiveTemps);

                                if (updatedPermenent && temporaryActivePositiveTemps.get(act) != null) {

                                    boolean conflict = false;

                                    for (Integer i : temporaryActivePositiveTemps.get(act).keySet()) {
                                        HashMap<Integer, Pair<String, String>> x = temporaryActivePositiveTemps.get(act).get(i);

                                        for (Integer k : x.keySet()) {
                                            Pair<String, String> y = x.get(k);
                                            if (sol.get(act) != null) {
                                                sol.get(act).put(i, y);
                                            } else {
                                                HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                                h.put(i, y);
                                                sol.put(act, h);
                                            }
                                            System.out.println("\t Equation for \"" + cdName + "\" - \n\t " + evName + " after replace  \n\t = " + equation);
                                            conflict = conflict || LpSolverUtil.solverForConflict(sol.get(act), null);
                                            if (conflict) {
                                                analysis = STATE_CONFLICT;
                                                conflictSets.add(act);
                                            }
//                                            if (LpSolverUtil.checkRelated(acMap, solverMap, sol, act, conflictSets, "")) {
//                                                System.out.println("FOUND SECONDARY CONFLICT -- FIRST LEVEL");
//                                                analysis = STATE_CONFLICT;
//                                                conflictSets.add(act);
//                                            }
                                        }
                                    }
                                } else if (temporaryActivePositiveTemps.get(act) != null) {
                                    HashMap<Integer, Pair<String, String>> x = temporaryActivePositiveTemps.get(act).get(cdPos);

                                    boolean conflict = false;
                                    boolean secondaryConflict = false;

                                    if (x == null) {
                                        continue;
                                    }

                                    Pair<String, String> y = x.get(evPos);

                                    if (y == null) {
                                        continue;
                                    }

//                                    for (Integer k : x.keySet()) {
//                                        Pair<String, String> y = x.get(k);
                                    if (sol.get(act) != null) {
                                        sol.get(act).put(cdPos, y);
                                    } else {
                                        HashMap<Integer, Pair<String, String>> h = new HashMap<>();
                                        h.put(cdPos, y);
                                        sol.put(act, h);
                                    }
                                    System.out.println(" Equation for \"" + cdName + "\" - \n\t " + evName + " after replace  \n\t = " + equation);

                                    conflict = conflict || LpSolverUtil.solverForConflict(sol.get(act), null);
                                    if (conflict) {
                                        //analysis = STATE_CONFLICT;
                                        conflictSets.add(act);
                                    }

                                    secondaryConflict = secondaryConflict || LpSolverUtil.checkRelated(acMap, solverMap, sol, act, conflictSets, "");
                                    if (secondaryConflict) {
                                        System.out.println("FOUND SECONDARY CONFLICT -- FIRST LEVEL");
                                        //analysis = STATE_CONFLICT;
                                        conflictSets.add(act);
//                                        }
                                    }
                                } else {
                                    System.out.println(act + " is not present in positives");
                                }


                            } else {
                                System.out.println("We still have some Data values missing");
                            }

                        }
                    }
                }
            }
        }
        return analysis;
    }


    private String getResult(final String[][] matrix, final String[] constraints, final XTrace trace) {
        String result = "[";
        int intCounterMin;
        int intCounterMax;
        String INF = "inf";
        for (int i = 0; i < matrix.length; i++) {
            intCounterMin = 0;
            intCounterMax = 0;
            String oldStatus = matrix[i][0];
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == null) {
                    matrix[i][j] = oldStatus;
                }
                if ((j == (matrix[0].length - 1)) && (i == (matrix.length - 1))) {
                    if (!matrix[i][j].equals(oldStatus)) {
                        result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
                                + convert(trace, intCounterMin) + "," + convert(trace, intCounterMax) + "]),";
                        oldStatus = matrix[i][j];
                        intCounterMin = intCounterMax;
                    }
                    result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j] + "),["
                            + convert(trace, intCounterMin) + "," + INF + "])]";
                    intCounterMin = intCounterMax;
                    intCounterMax++;
                } else {
                    if (j == (matrix[0].length - 1)) {
                        if (matrix[i][j].equals(oldStatus)) {
                            result = result + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
                                    + "),[" + convert(trace, intCounterMin) + "," + INF + "]),";
                        } else {
                            result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
                                    + convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
                                    + "])," + "mholds_for(status(" + constraints[i] + "," + matrix[i][j]
                                    + "),[" + convert(trace, intCounterMax) + "," + INF + "]),";
                        }
                    } else {
                        if (!matrix[i][j].equals(oldStatus)) {
                            result = result + "mholds_for(status(" + constraints[i] + "," + oldStatus + "),["
                                    + convert(trace, intCounterMin) + "," + convert(trace, intCounterMax)
                                    + "]),";
                            oldStatus = matrix[i][j];
                            intCounterMin = intCounterMax;
                        }
                    }
                }
                intCounterMax++;

            }
        }
        return result;
    }


    private void addAttributeToTrace(final XTrace trace, final String key, final String value) {

        XAttribute name = new XAttributeLiteralImpl(key, value);

        trace.getAttributes().put(key, name);

    }


    private boolean isUnaryConstraint(String cdName) {
        return (cdName.equals(ConstraintName.ABSENCE)
                || cdName.equals(ConstraintName.ABSENCE2)
                || cdName.equals(ConstraintName.ABSENCE3)
                || cdName.equals(ConstraintName.EXACTLY1)
                || cdName.equals(ConstraintName.EXACTLY2)
                || cdName.equals(ConstraintName.EXISTENCE)
                || cdName.equals(ConstraintName.EXISTENCE2)
                || cdName.equals(ConstraintName.EXISTENCE2));
    }


    public static HashMap<String, HashMap<Integer, Pair<String, String>>> deepCloneMap1(HashMap<String, HashMap<Integer, Pair<String, String>>> original) {
        HashMap<String, HashMap<Integer, Pair<String, String>>> newMap = new HashMap<>();

        for (String s : original.keySet()) {
            String key1 = s + "";
            HashMap<Integer, Pair<String, String>> x = original.get(s);
            HashMap<Integer, Pair<String, String>> y = new HashMap<>();
            for (Integer i : x.keySet()) {
                int key2 = i;

                Pair<String, String> pair = x.get(i);
                Pair<String, String> newPair = new Pair<>(pair.getFirst() + "", pair.getSecond() + "");
                y.put(key2, newPair);
            }
            newMap.put(key1, y);
        }
        return newMap;
    }


    public static HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> deepCloneMap2(HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> original) {
        HashMap<String, HashMap<Integer, HashMap<Integer, Pair<String, String>>>> newMap = new HashMap<>();

        for (String s : original.keySet()) {
            String key1 = s + "";
            HashMap<Integer, HashMap<Integer, Pair<String, String>>> x = original.get(s);
            HashMap<Integer, HashMap<Integer, Pair<String, String>>> newX = new HashMap<>();

            for (Integer i : x.keySet()) {
                int key2 = i;
                HashMap<Integer, Pair<String, String>> y = x.get(i);
                HashMap<Integer, Pair<String, String>> newY = new HashMap<>();
                for (Integer j : y.keySet()) {
                    int key3 = j;
                    Pair<String, String> pair = y.get(j);
                    Pair<String, String> newPair = new Pair<>(pair.getFirst() + "", pair.getSecond() + "");
                    newY.put(key3, newPair);
                }
                newX.put(key2, newY);
            }

            newMap.put(key1, newX);

        }
        return newMap;
    }
}
