package org.processmining.onlinedeclareanalyzer.replayers;

import org.deckfour.xes.model.XTrace;
import org.processmining.onlinedeclareanalyzer.data.DataSnapshotListener;

public interface DeclareReplayer {
	
	public void process(int eventIndex, String event, XTrace trace, String traceId,  DataSnapshotListener listener );

}
