package org.processmining.stream.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.util.Pair;
import org.processmining.models.cnet.CNet;
import org.processmining.stream.algorithms.budgetlossycounting.DBudgetActivities;
import org.processmining.stream.algorithms.budgetlossycounting.DBudgetCases;
import org.processmining.stream.algorithms.budgetlossycounting.DBudgetRelations;
import org.processmining.stream.algorithms.budgetlossycounting.SharedDelta;
import org.processmining.stream.algorithms.lossycounting.CNetGenerator;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.config.algorithms.BudgetLossyCountingMinerConfiguration;
import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.utils.Utils;

/**
 * A Budget Lossy Counting version of the Heuristics Miner
 * 
 * @author Andrea Burattin
 */
@OnlineAlgorithm(
	name = "Heuristics Miner with Budget Lossy Counting",
	requiredConfiguration = BudgetLossyCountingMinerConfiguration.class
)
public class BudgetLossyCountingHM extends CNetBasedOnlineMiningAlgorithm {

	private BudgetConfiguration bc;
	private HashMap<String, Integer> startingActivities;
	private HashMap<String, Integer> finishingActivities;
	private Collection<Pair<String, String>> logStartsEnd;
	private ArrayList<String> ends;
	private DBudgetActivities activities;
	private DBudgetRelations relations;
	private DBudgetCases cases;
	private double budget;
	
	private String latestActivity = null;
	private double activityThreshold = 10.0;
	
	private CNet modelCache = null;

	/**
	 * Default constructor
	 */
	public BudgetLossyCountingHM() {}
	
	/**
	 * 
	 * @param plugin
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public BudgetLossyCountingHM(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		super(plugin);
	}
	
	@Override
	public void setup(MinerConfiguration configuration) {
		bc = (BudgetConfiguration) configuration.getConfiguration(BudgetConfiguration.class);
		
		startingActivities = new HashMap<String, Integer>();
		finishingActivities = new HashMap<String, Integer>();
		logStartsEnd = ((ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class)).getLogStartsEnds();
		
		ends = new ArrayList<String>();
		for (Pair<String, String> p : logStartsEnd) {
			ends.add(p.getSecond());
		}
		
		budget = bc.getBudget();
		
		SharedDelta delta = new SharedDelta();
		delta.budget = (int) budget;
		activities = new DBudgetActivities(delta);
		relations = new DBudgetRelations(delta);
		cases = new DBudgetCases(delta, startingActivities, finishingActivities);
	}
	
	@Override
	protected void analyzeEvent(XTrace trace, boolean isTraceStart, boolean isTraceComplete) {
		cacheCleanup();
		
		XEvent event = trace.get(0);
		String activityName = Utils.getActivityName(event);
		String caseId = Utils.getCaseID(trace);
		
		// update data structures
		activities.addObservation(activityName);
		latestActivity = cases.addObservation(caseId, activityName);
		if (latestActivity != null) {
			relations.addObservation(latestActivity, activityName);
		}
		
		// this activity is a termination activity
		if (isTraceComplete && !ends.contains(activityName)) {
			ends.add(activityName);
		}
		if (ends.contains(activityName)) {
			cases.lastActivityObserved(activityName);
		}
	}

	@Override
	protected Object updateModel() {
		if (modelCache == null) {
			CNetGenerator generator = new CNetGenerator(
					activities.getActivities(),
					relations.getRelations(),
					startingActivities,
					finishingActivities);
			modelCache = generator.generateModel(0.9, activityThreshold, 0.1);
//			CNetHelper.saveModelToFile(getContext(), "/home/delas/desktop/", "event-" + observedEvents + ".cnet", modelCache);
		}
		return modelCache;
	}

	@Override
	protected void cacheCleanup() {
		super.cacheCleanup();
		modelCache = null;
	}

	@Override
	protected Collection<Pair<String, String>> getLogStartEnd() {
		return logStartsEnd;
	}
}
