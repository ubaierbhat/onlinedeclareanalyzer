package org.processmining.stream.algorithms;

import java.util.Collection;

import org.deckfour.xes.model.XLog;
import org.processmining.framework.util.Pair;
import org.processmining.models.cnet.CNet;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.utils.BasicPluginConfiguration;
import org.processmining.stream.utils.Utils;
import org.processmining.stream.utils.helpers.CNetHelper;
import org.processmining.stream.utils.helpers.FitnessHelper;
import org.processmining.stream.utils.helpers.PrecisionHelper;

/**
 * Abstract online mining class for algorithms based on {@link CNet}. The
 * fitness and the precision values, for models computed starting from this
 * class, require the specification of the {@link #getLogStartEnd()} method.
 * 
 * @author Andrea Burattin
 */
public abstract class CNetBasedOnlineMiningAlgorithm extends OnlineMiningAlgorithm {

	private Petrinet petriCache = null;
	
	/**
	 * Default constructor
	 */
	public CNetBasedOnlineMiningAlgorithm() {}
	
	/**
	 * 
	 * @param plugin
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public CNetBasedOnlineMiningAlgorithm(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		super(plugin);
	}

	/**
	 * This method cleans up the cache
	 */
	protected void cacheCleanup() {
		petriCache = null;
	}

	@Override
	protected double computeFitness() {
		XLog logForFitness = FitnessHelper.traceSet2Log(fitnessLog, getLogStartEnd());
		if (petriCache == null) {
			petriCache = CNetHelper.convert(getContext(), (CNet) updateModel());
			petriCache = (Petrinet) Utils.addArtificialStartEnd(getContext(),
					petriCache,
					BasicPluginConfiguration.ARTIFICIAL_START_NAME,
					BasicPluginConfiguration.ARTIFICIAL_END_NAME)[0];
		}
		return FitnessHelper.getFitness(getContext(), petriCache, logForFitness);
	}

	@Override
	protected double computePrecision() {
		XLog logForPrecision = FitnessHelper.traceSet2Log(precisionLog, getLogStartEnd());
		if (petriCache == null) {
			petriCache = CNetHelper.convert(getContext(), (CNet) updateModel());
			petriCache = (Petrinet) Utils.addArtificialStartEnd(getContext(),
					petriCache,
					BasicPluginConfiguration.ARTIFICIAL_START_NAME,
					BasicPluginConfiguration.ARTIFICIAL_END_NAME)[0];
		}
		return PrecisionHelper.getPrecision(getContext(), petriCache, logForPrecision);
	}
	
	/**
	 * This method is required in order to obtain the set of pairs of events
	 * that are allowed to start and end each trace. This method is allowed to
	 * return and empty collection.
	 * 
	 * @return the collection of pairs
	 */
	protected abstract Collection<Pair<String, String>> getLogStartEnd();
}
