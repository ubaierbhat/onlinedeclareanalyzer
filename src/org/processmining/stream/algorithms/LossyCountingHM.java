package org.processmining.stream.algorithms;

import java.util.Collection;
import java.util.HashMap;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.util.Pair;
import org.processmining.models.cnet.CNet;
import org.processmining.stream.algorithms.lossycounting.CNetGenerator;
import org.processmining.stream.algorithms.lossycounting.DActivities;
import org.processmining.stream.algorithms.lossycounting.DCases;
import org.processmining.stream.algorithms.lossycounting.DRelations;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.config.algorithms.LossyCountingMinerConfiguration;
import org.processmining.stream.config.fragments.LossyCountingConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.utils.Utils;

/**
 * A Lossy Counting version of the Heuristics Miner
 * 
 * @author Andrea Burattin
 */
@OnlineAlgorithm(
	name = "Heuristics Miner with Lossy Counting",
	requiredConfiguration = LossyCountingMinerConfiguration.class
)
public class LossyCountingHM extends CNetBasedOnlineMiningAlgorithm {

	private LossyCountingConfiguration lcc;
	private HashMap<String, Integer> startingActivities;
	private HashMap<String, Integer> finishingActivities;
	private Collection<Pair<String, String>> logStartsEnd;
	private DActivities activities;
	private DRelations relations;
	private DCases cases;
	private int bucketWidth = -1;
	private int currentBucket = -1;
	
	private String latestActivity = null;
	private double activityThreshold = 10.0;
	
	private CNet modelCache = null;
	
	/**
	 * Default constructor
	 */
	public LossyCountingHM() {}
	
	/**
	 * 
	 * @param plugin
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public LossyCountingHM(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		super(plugin);
	}
	
	@Override
	public void setup(MinerConfiguration configuration) {
		lcc = (LossyCountingConfiguration) configuration.getConfiguration(LossyCountingConfiguration.class);
		
		startingActivities = new HashMap<String, Integer>();
		finishingActivities = new HashMap<String, Integer>();
		logStartsEnd = ((ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class)).getLogStartsEnds();
		activities = new DActivities();
		relations = new DRelations();
		cases = new DCases(startingActivities, finishingActivities);
		
		bucketWidth = (int)(1.0 / lcc.getError());
	}
	
	@Override
	protected void analyzeEvent(XTrace trace, boolean isTraceStart, boolean isTraceComplete) {
		cacheCleanup();
		
		currentBucket = (int)(observedEvents / bucketWidth);
		XEvent event = trace.get(0);
		String activityName = Utils.getActivityName(event);
		String caseId = Utils.getCaseID(trace);
		
		// update data structures
		activities.addActivityObservation(activityName, currentBucket);
		latestActivity = cases.addCaseObservation(caseId, activityName, currentBucket);
		if (latestActivity != null) {
			relations.addRelationObservation(latestActivity, activityName, currentBucket);
		}
		
		// cleanup when required
		if (observedEvents % bucketWidth == 0) {
			activities.cleanup(currentBucket);
			cases.cleanup(currentBucket);
			relations.cleanup(currentBucket);
		}
	}

	@Override
	protected Object updateModel() {
		if (modelCache == null) {
			CNetGenerator generator = new CNetGenerator(
					activities.getActivities(),
					relations.getRelations(),
					startingActivities,
					finishingActivities);
			modelCache = generator.generateModel(0.9, activityThreshold, 0.1);
//			CNetHelper.saveModelToFile(getContext(), "/home/delas/desktop/", "event-" + observedEvents + ".cnet", modelCache);
		}
		return modelCache;
	}

	@Override
	protected void cacheCleanup() {
		super.cacheCleanup();
		modelCache = null;
	}

	@Override
	protected Collection<Pair<String, String>> getLogStartEnd() {
		return logStartsEnd;
	}
}
