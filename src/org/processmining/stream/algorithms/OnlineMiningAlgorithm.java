package org.processmining.stream.algorithms;

import java.util.concurrent.ArrayBlockingQueue;

import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.cli.CLIPluginContext;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.exceptions.MissingConfigurationException;
import org.processmining.stream.exceptions.PluginMissingException;
import org.processmining.stream.exceptions.WrongConfigurationException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.plugins.context.CLIStreamContext;
import org.processmining.stream.plugins.context.CLIStreamPluginContext;
import org.processmining.stream.utils.Utils;

/**
 * This abstract class identifies a general mining online mining algorithm
 * 
 * @author Andrea Burattin
 */
public abstract class OnlineMiningAlgorithm {

	private CLIPluginContext context = null;
	protected OnlineMinerPlugin plugin = null;
	
	protected long observedEvents = 0;
	protected MinerConfiguration configuration;
	
	private boolean updateModel = false;
	private int updateModelFrequency;
	
	private boolean computeFitness = false;
	private int fitnessComputationFrequency;
	protected ArrayBlockingQueue<XTrace> fitnessLog;
	
	private boolean computePrecision = false;
	private int precisionComputationFrequency;
	protected ArrayBlockingQueue<XTrace> precisionLog;

	/**
	 * Default constructor
	 */
	public OnlineMiningAlgorithm() {}

	/**
	 * Constractor with the plugin
	 * 
	 * @param plugin the plugin to associate to the current miner
	 * @throws ClassIsNotOnlineAlgorithmException
	 */
	public OnlineMiningAlgorithm(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		setPlugin(plugin);
	}

	/**
	 * This method updates the plugin associated to the current mining algorithm
	 * 
	 * @param plugin the new plugin to set
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public void setPlugin(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		if (!getClass().isAnnotationPresent(OnlineAlgorithm.class)) {
			throw new ClassIsNotOnlineAlgorithmException("The provided class is not decorated as an @OnlineAlgorithm");
		}
		this.plugin = plugin;
		this.context = new CLIStreamPluginContext(new CLIStreamContext(), "Context for " + getClass().getAnnotation(OnlineAlgorithm.class).name());
	}
	
	/**
	 * This method configures the miner. This method is called before the
	 * beginning of the streaming session.
	 * 
	 * @param configuration the miner configuration
	 * @throws WrongConfigurationException
	 * @throws MissingConfigurationException
	 * @throws PluginMissingException
	 */
	public void prepareMiner(MinerConfiguration configuration) throws WrongConfigurationException, MissingConfigurationException, PluginMissingException {
		if (plugin == null) {
			throw new PluginMissingException("You must call the setPlugin method first!");
		}
		Class<? extends MinerConfiguration> required = getClass().getAnnotation(OnlineAlgorithm.class).requiredConfiguration();
		if (required != null) {
			if (!required.isAssignableFrom(configuration.getClass())) {
				throw new WrongConfigurationException(
						"Configuration is not of the proper type: required "
						+ "`" + required.getCanonicalName() + "' provided"
						+ "`" + configuration.getClass().getCanonicalName() + "'");
			}
		}
		
		this.configuration = configuration;
		configuration.validateConfiguration();
		
		if (configuration.containsConfiguration(ModelMetricsConfiguration.class)) {
			ModelMetricsConfiguration config = (ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class);
			updateModel = config.updateModel();
			computeFitness = config.computeFitness();
			computePrecision = config.computePrecision();
			
			if (updateModel) {
				updateModelFrequency = config.getModelUpdateFrequency();
			}
			
			if (computeFitness) {
				fitnessComputationFrequency = config.getFitnessComputationFrequency();
				fitnessLog = new ArrayBlockingQueue<XTrace>(fitnessComputationFrequency);
			}
			
			if (computePrecision) {
				precisionComputationFrequency = config.getPrecisionComputationFrequency();
				precisionLog = new ArrayBlockingQueue<XTrace>(precisionComputationFrequency);
			}
		}
		
		setup(configuration);
	}
	
	/**
	 * This method to get the current miner configuration
	 * 
	 * @return the miner configuration
	 */
	public MinerConfiguration getConfiguration() {
		return configuration;
	}
	
	/**
	 * This method is called all the time that a new event is observed
	 * 
	 * @param trace the trace (containing only one event) observed onto the
	 * stream
	 */
	public void observe(XTrace trace) {
		observedEvents++;
		
		analyzeEvent(
				trace,
				Utils.isTraceStart(trace.get(0)),
				Utils.isTraceComplete(trace.get(0)));
		
		// model update
		if (updateModel) {
			if (observedEvents % updateModelFrequency == 0) {
				plugin.onModelUpdate(updateModel());
			}
		}
		
		// fitness
		if (computeFitness) {
			if (fitnessLog.size() == fitnessComputationFrequency) {
				fitnessLog.remove();
			}
			fitnessLog.add(trace);
			
			if (observedEvents % fitnessComputationFrequency == 0) {
				plugin.onNewFitnessValue(computeFitness());
			}
		}

		// precision
		if (computePrecision) {
			if (precisionLog.size() == precisionComputationFrequency) {
				precisionLog.remove();
			}
			precisionLog.add(trace);
			
			if (observedEvents % precisionComputationFrequency == 0) {
				plugin.onNewPrecisionValue(computePrecision());
			}
		}
	}
	
	/**
	 * This method can be used to get a local context
	 * 
	 * @return a local plugin context
	 */
	protected PluginContext getContext() {
		return context;
	}
	
	/**
	 * This method is called before that the actual mining start
	 * 
	 * @param configuration
	 */
	protected abstract void setup(MinerConfiguration configuration);
	
	/**
	 * This method is responsible of the actual analysis of the provided trace
	 * (containing only one event)
	 * 
	 * @param trace the trace observed
	 * @param isTraceStart this variable is set to true if the event collected
	 * is explicitly marked as the first event of the trace
	 * @param isTraceComplete this variable is set to true if the event collected
	 * is explicitly marked as the last event of the trace
	 */
	protected abstract void analyzeEvent(XTrace trace, boolean isTraceStart, boolean isTraceComplete);
	
	/**
	 * This method is called all the time that (if configured) the model needs
	 * to be updated
	 * 
	 * @return a representation of the model
	 */
	protected abstract Object updateModel();
	
	/**
	 * This method is called all the time that (if configured) the fitness has
	 * to be computed
	 */
	protected abstract double computeFitness();
	
	/**
	 * This method is called all the time that (if configured) the precision has
	 * to be computed
	 */
	protected abstract double computePrecision();
}
