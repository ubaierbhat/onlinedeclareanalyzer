package org.processmining.stream.algorithms;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.util.Pair;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.HeuristicsMiner;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.settings.HeuristicsMinerSettings;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.config.algorithms.SlidingWindowHMConfiguration;
import org.processmining.stream.config.fragments.HeuristicsMinerConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.utils.helpers.FitnessHelper;

/**
 * The Heuristics Miner applied on a Sliding Window Policy
 * 
 * @author Andrea Burattin
 */
@OnlineAlgorithm(
	name = "Heuristics Miner with Sliding Window",
	requiredConfiguration = SlidingWindowHMConfiguration.class
)
public class SlidingWindowHM extends HeuristicsNetBasedOnlineMiningAlgorithm {

	private ArrayBlockingQueue<XTrace> miningLog;
	private int miningLogSize;
	private HeuristicsMinerSettings minerSettings;
	private Collection<Pair<String, String>> logStartsEnd;
	
	private HeuristicsNet modelCache = null;

	/**
	 * Default constructor
	 */
	public SlidingWindowHM() {}
	
	/**
	 * 
	 * @param plugin
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public SlidingWindowHM(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		super(plugin);
	}
	
	@Override
	public void setup(MinerConfiguration configuration) {
		miningLogSize = ((ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class)).getModelUpdateFrequency();
		minerSettings = ((HeuristicsMinerConfiguration) configuration.getConfiguration(HeuristicsMinerConfiguration.class)).getSettings();
		logStartsEnd = ((ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class)).getLogStartsEnds();
		
		miningLog = new ArrayBlockingQueue<XTrace>(miningLogSize);
	}
	
	@Override
	protected void analyzeEvent(XTrace trace, boolean isTraceStart, boolean isTraceComplete) {
		cacheCleanup();
		
		// mining log updating
		if (miningLog.size() == miningLogSize) {
			miningLog.remove();
		}
		miningLog.add((XTrace) trace.clone());
	}

	@Override
	protected Object updateModel() {
		// log for mining
		XLog logForMining = FitnessHelper.traceSet2Log(miningLog, null);
		
		// mining
		ByteArrayOutputStream pipeOut = new ByteArrayOutputStream();
		PrintStream old_out = System.out;
		System.setOut(new PrintStream(pipeOut));
		HeuristicsMiner hm = new HeuristicsMiner(getContext(), logForMining, minerSettings);
		modelCache = hm.mine();
		System.setOut(old_out);
		return modelCache;
	}

	@Override
	protected void cacheCleanup() {
		super.cacheCleanup();
		modelCache = null;
	}

	@Override
	protected Collection<Pair<String, String>> getLogStartEnd() {
		return logStartsEnd;
	}
}
