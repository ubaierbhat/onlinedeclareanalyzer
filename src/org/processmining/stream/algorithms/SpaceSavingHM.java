package org.processmining.stream.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.util.Pair;
import org.processmining.models.cnet.CNet;
import org.processmining.stream.algorithms.lossycounting.CNetGenerator;
import org.processmining.stream.algorithms.spacesaving.DActivities;
import org.processmining.stream.algorithms.spacesaving.DCases;
import org.processmining.stream.algorithms.spacesaving.DRelations;
import org.processmining.stream.algorithms.spacesaving.SharedWindow;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.config.algorithms.SpaceSavingMinerConfiguration;
import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.ClassIsNotOnlineAlgorithmException;
import org.processmining.stream.plugins.OnlineMinerPlugin;
import org.processmining.stream.utils.Utils;

/**
 * A Space Saving version of the Heuristics Miner
 * 
 * @author Andrea Burattin
 */
@OnlineAlgorithm(
	name = "Heuristics Miner with Space Saving",
	requiredConfiguration = SpaceSavingMinerConfiguration.class
)
public class SpaceSavingHM extends CNetBasedOnlineMiningAlgorithm {

	private BudgetConfiguration bc;
	protected Collection<Pair<String, String>> logStartsEnd;
	private HashMap<String, Integer> startingActivities;
	private HashMap<String, Integer> finishingActivities;
	private ArrayList<String> ends;
	private DActivities activities;
	private DRelations relations;
	private DCases cases;
	private double budget;
	private String latestActivity = null;
	
	private CNet modelCache = null;
	private double activityThreshold = 10.0;

	/**
	 * Default constructor
	 */
	public SpaceSavingHM() {}
	
	/**
	 * 
	 * @param plugin
	 * @throws ClassIsNotOnlineAlgorithmException 
	 */
	public SpaceSavingHM(OnlineMinerPlugin plugin) throws ClassIsNotOnlineAlgorithmException {
		super(plugin);
	}
	
	@Override
	public void setup(MinerConfiguration configuration) {
		bc = (BudgetConfiguration) configuration.getConfiguration(BudgetConfiguration.class);
		budget = bc.getBudget();
		
		startingActivities = new HashMap<String, Integer>();
		finishingActivities = new HashMap<String, Integer>();
		
		logStartsEnd = ((ModelMetricsConfiguration) configuration.getConfiguration(ModelMetricsConfiguration.class)).getLogStartsEnds();
		ends = new ArrayList<String>();
		for (Pair<String, String> p : logStartsEnd) {
			ends.add(p.getSecond());
		}
		
		SharedWindow window = new SharedWindow();
		window.budget = (int) budget;
		activities = new DActivities(window);
		relations = new DRelations(window);
		cases = new DCases(window, startingActivities, finishingActivities);
	}
	
	@Override
	protected void analyzeEvent(XTrace trace, boolean isTraceStart, boolean isTraceComplete) {
		cacheCleanup();
		
		XEvent event = trace.get(0);
		String activityName = Utils.getActivityName(event);
		String caseId = Utils.getCaseID(trace);
		
		// update data structures
		activities.addObservation(activityName);
		latestActivity = cases.addObservation(caseId, activityName);
		if (latestActivity != null) {
			relations.addObservation(latestActivity, activityName);
		}
		
		// this activity is a termination activity
		if (isTraceComplete && !ends.contains(activityName)) {
			ends.add(activityName);
		}
		if (ends.contains(activityName)) {
			cases.lastActivityObserved(activityName);
		}
	}

	@Override
	protected Object updateModel() {
		if (modelCache == null) {
			CNetGenerator generator = new CNetGenerator(
					activities.getActivities(),
					relations.getRelations(),
					startingActivities,
					finishingActivities);
			modelCache = generator.generateModel(0.9, activityThreshold, 0.1);
//			CNetHelper.saveModelToFile(getContext(), "/home/delas/desktop/", "event-" + observedEvents + ".cnet", modelCache);
		}
		return modelCache;
	}

	@Override
	protected void cacheCleanup() {
		super.cacheCleanup();
		modelCache = null;
	}

	@Override
	protected Collection<Pair<String, String>> getLogStartEnd() {
		return logStartsEnd;
	}
}
