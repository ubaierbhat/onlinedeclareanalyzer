package org.processmining.stream.algorithms.lossycounting;

import java.util.HashMap;
import java.util.Iterator;

import org.processmining.framework.util.Pair;

/**
 * This data structure is used to manage the activity for the Lossy Counting
 * algorithm.
 * 
 * @author Andrea Burattin
 */
public class DActivities extends HashMap<String, Pair<Integer, Integer>> {

	private static final long serialVersionUID = -1093789267479291150L;
	private int size = 0;

	/**
	 * 
	 * @param activityName
	 * @param currentBucket
	 */
	public void addActivityObservation(String activityName, Integer currentBucket) {
		if (containsKey(activityName)) {
			Pair<Integer, Integer> v = get(activityName);
			put(activityName, new Pair<Integer, Integer>(v.getFirst() + 1, v.getSecond()));
		} else {
			put(activityName, new Pair<Integer, Integer>(1, currentBucket - 1));
			size++;
		}
	}
	
	/**
	 * 
	 * @param currentBucket
	 */
	public void cleanup(Integer currentBucket) {
		for (Iterator<String> it = keySet().iterator(); it.hasNext();) {
			String activity = it.next();
			Pair<Integer, Integer> v = get(activity);
			Integer age = v.getFirst() + v.getSecond();
			if (age <= currentBucket) {
				it.remove();
				size--;
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public HashMap<String, Double> getActivities() {
		HashMap<String, Double> activities = new HashMap<String, Double>();
		for (Iterator<String> it = keySet().iterator(); it.hasNext();) {
			String activity = it.next();
			activities.put(activity, (double)get(activity).getFirst());
		}
		return activities;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getSize() {
		return size;
	}
}
