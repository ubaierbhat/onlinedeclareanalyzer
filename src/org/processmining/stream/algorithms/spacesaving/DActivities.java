package org.processmining.stream.algorithms.spacesaving;

import java.util.HashMap;
import java.util.Iterator;

public class DActivities extends SpaceSaving<String> {

	private static final long serialVersionUID = -8273492627072397889L;

	public DActivities(SharedWindow window) {
		super(window);
		window.activities = this;
	}
	
	public HashMap<String, Double> getActivities() {
		HashMap<String, Double> activities = new HashMap<String, Double>();
		for (Iterator<String> it = keySet().iterator(); it.hasNext();) {
			String activity = it.next();
			activities.put(activity, (double)get(activity));
		}
		return activities;
	}
}
