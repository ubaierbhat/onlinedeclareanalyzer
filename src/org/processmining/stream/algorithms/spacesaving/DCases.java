package org.processmining.stream.algorithms.spacesaving;

import java.util.HashMap;

import org.processmining.framework.util.Pair;

public class DCases extends HashMap<String, Pair<String, Integer>> implements SpaceSavingInterface<String> {

	private static final long serialVersionUID = 7034002584616814469L;
	private HashMap<String, Integer> startingActivities;
	private HashMap<String, Integer> finishingActivities;
	private SharedWindow window;
	
	public DCases(SharedWindow window, HashMap<String, Integer> startingActivities, HashMap<String, Integer> finishingActivities) {
		this.startingActivities = startingActivities;
		this.finishingActivities = finishingActivities;
		this.window = window;
		window.cases = this;
	}

	public void addObservation(String newValue) {
		addObservation(newValue, null);
	}
	
	public String addObservation(String caseId, String latestActivity) {
		if (containsKey(caseId)) {
			Pair<String, Integer> v = get(caseId);
			put(caseId, new Pair<String, Integer>(latestActivity, v.getSecond() + 1));
			return v.getFirst();
		} else {
			int oldValue = 0;
			if (window.getSize() == window.budget) {
				oldValue = cleanup();
			}
			incrementIntHashMap(startingActivities, latestActivity);
			put(caseId, new Pair<String, Integer>(latestActivity, oldValue + 1));
		}
		return null;
	}
	
	private void incrementIntHashMap(HashMap<String, Integer> hm, String key) {
		Integer freq = hm.get(key);
		if (freq == null) {
			hm.put(key, 1);
		} else {
			hm.put(key, freq + 1);
		}
	}

	public Pair<String, Integer> getMin() {
		int min = Integer.MAX_VALUE;
		String value = null;
		for (String s : keySet()) {
			int current = get(s).getSecond();
			if (current < min) {
				min = current;
				value = s;
			}
		}
		return new Pair<String, Integer>(value, min);
	}

	public Integer removeCase(String element) {
		incrementIntHashMap(finishingActivities, get(element).getFirst());
		return super.remove(element).getSecond();
	}

	public int cleanup() {
		Pair<String, Integer> min = getMin();
		removeCase(min.getFirst());
		return min.getSecond();
	}
	
	public void lastActivityObserved(String activityName) {
		if (containsKey(activityName)) {
			removeCase(activityName);
			incrementIntHashMap(finishingActivities, activityName);
		}
	}
}
