package org.processmining.stream.algorithms.spacesaving;

import java.util.HashMap;
import java.util.Iterator;

import org.processmining.framework.util.Pair;

public class DRelations extends SpaceSaving<Pair<String, String>> {

	private static final long serialVersionUID = 341355305134621512L;

	public DRelations(SharedWindow window) {
		super(window);
		window.relations = this;
	}
	
	public void addObservation(String source, String destination) {
		addObservation(new Pair<String, String>(source, destination));
	}
	
	public HashMap<Pair<String, String>, Double> getRelations() {
		HashMap<Pair<String, String>, Double> relations = new HashMap<Pair<String, String>, Double>();
		for (Iterator<Pair<String, String>> it = keySet().iterator(); it.hasNext();) {
			Pair<String, String> relationName = it.next();
			relations.put(relationName, (double)get(relationName));
		}
		return relations;
	}
}
