package org.processmining.stream.algorithms.spacesaving;

public class SharedWindow {

	public int budget = 0;
	public DActivities activities;
	public DRelations relations;
	public DCases cases;
	
	public int getSize() {
		int sum = 0;
		sum += cases.size();
		sum += activities.size();
		sum += relations.size();
		return sum;
	}
}
