package org.processmining.stream.algorithms.spacesaving;

import java.util.HashMap;

import org.processmining.framework.util.Pair;

public class SpaceSaving<T> extends HashMap<T, Integer> implements SpaceSavingInterface<T> {

	private static final long serialVersionUID = -1492255347927769883L;
	private SharedWindow window;

	public SpaceSaving(SharedWindow window) {
		this.window = window;
	}
	
	public Pair<T, Integer> getMin() {
		int min = Integer.MAX_VALUE;
		T value = null;
		for (T t : keySet()) {
			int current = get(t);
			if (current < min) {
				min = current;
				value = t;
			}
		}
		return new Pair<T, Integer>(value, min);
	}
	
	public void addObservation(T newValue) {
		if (containsKey(newValue)) {
			Integer v = get(newValue);
			put(newValue, v + 1);
		} else {
			int oldValue = 0;
			if (window.getSize() == window.budget) {
				oldValue = cleanup();
			}
			put(newValue, oldValue + 1);
		}
	}

	public int cleanup() {
		Pair<T, Integer> min = getMin();
		remove(min.getFirst());
		return min.getSecond();
	}
}
