package org.processmining.stream.algorithms.spacesaving;

import org.processmining.framework.util.Pair;

public interface SpaceSavingInterface<T> {

	public Pair<T, Integer> getMin();
	
	public void addObservation(T newValue);
	
	public int cleanup();
}
