package org.processmining.stream.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.processmining.stream.config.interfaces.MinerConfiguration;

/**
 * This annotation indicates whether a class represents an online mining
 * algorithm
 * 
 * @author Andrea Burattin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OnlineAlgorithm {

	/**
	 * The name of the mining algorithm
	 * @return
	 */
	String name();
	
	/**
	 * The required configuration for the algorithm
	 * @return
	 */
	Class<? extends MinerConfiguration> requiredConfiguration();
}
