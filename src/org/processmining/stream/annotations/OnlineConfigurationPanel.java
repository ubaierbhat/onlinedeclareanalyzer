package org.processmining.stream.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.processmining.stream.config.interfaces.Configuration;

/**
 * This annotation is used into the {@link Configuration} hierarchy to indicate
 * which miner configuration a panel can be used for.
 * 
 * @author Andrea Burattin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OnlineConfigurationPanel {

	/**
	 * The configuration associated to the current panel
	 * 
	 * @return
	 */
	Class<? extends Configuration> configurationFor();
}
