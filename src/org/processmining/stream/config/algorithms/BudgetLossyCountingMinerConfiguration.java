package org.processmining.stream.config.algorithms;

import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.config.interfaces.SourceConfiguration;

/**
 * This class defines the required configurations for the Lossy Counting with
 * Budget mining algorithm
 * 
 * @author Andrea Burattin
 */
public class BudgetLossyCountingMinerConfiguration extends MinerConfiguration {

	/**
	 * Class constructor
	 */
	public BudgetLossyCountingMinerConfiguration() {
		requiredConfigurations.add(SourceConfiguration.class);
		requiredConfigurations.add(BudgetConfiguration.class);
		requiredConfigurations.add(ModelMetricsConfiguration.class);
	}
}
