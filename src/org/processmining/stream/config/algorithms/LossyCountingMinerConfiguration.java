package org.processmining.stream.config.algorithms;

import org.processmining.stream.config.fragments.LossyCountingConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.config.interfaces.SourceConfiguration;

/**
 * This class defines the required configurations for the Lossy Counting mining
 * algorithm
 * 
 * @author Andrea Burattin
 */
public class LossyCountingMinerConfiguration extends MinerConfiguration {

	/**
	 * Class constructor
	 */
	public LossyCountingMinerConfiguration() {
		requiredConfigurations.add(SourceConfiguration.class);
		requiredConfigurations.add(LossyCountingConfiguration.class);
		requiredConfigurations.add(ModelMetricsConfiguration.class);
	}
}
