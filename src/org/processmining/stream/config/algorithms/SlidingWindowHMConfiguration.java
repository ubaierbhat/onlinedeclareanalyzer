package org.processmining.stream.config.algorithms;

import org.processmining.stream.config.fragments.HeuristicsMinerConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.config.interfaces.SourceConfiguration;

/**
 * This class defines the required configurations for the Heuristics Miner with
 * Sliding Window
 * 
 * @author Andrea Burattin
 */
public class SlidingWindowHMConfiguration extends MinerConfiguration {

	/**
	 * Class constructor
	 */
	public SlidingWindowHMConfiguration() {
		requiredConfigurations.add(SourceConfiguration.class);
		requiredConfigurations.add(HeuristicsMinerConfiguration.class);
		requiredConfigurations.add(ModelMetricsConfiguration.class);
	}
}
