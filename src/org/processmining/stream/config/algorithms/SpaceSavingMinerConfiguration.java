package org.processmining.stream.config.algorithms;

import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.config.interfaces.SourceConfiguration;

/**
 * This class defines the required configurations for the Space Saving mining
 * algorithm
 * 
 * @author Andrea Burattin
 */
public class SpaceSavingMinerConfiguration extends MinerConfiguration {

	/**
	 * Class constructor
	 */
	public SpaceSavingMinerConfiguration() {
		requiredConfigurations.add(SourceConfiguration.class);
		requiredConfigurations.add(BudgetConfiguration.class);
		requiredConfigurations.add(ModelMetricsConfiguration.class);
	}
}
