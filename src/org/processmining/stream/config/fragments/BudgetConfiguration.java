package org.processmining.stream.config.fragments;

import org.processmining.stream.config.interfaces.Configuration;

/**
 * This configuration contains a general budget requirement
 * 
 * @author Andrea Burattin
 */
public class BudgetConfiguration implements Configuration {

	private int budget = 1000;

	/**
	 * This method to get the available budget
	 * 
	 * @return the error
	 */
	public double getBudget() {
		return budget;
	}

	/**
	 * This method to set the available budget
	 * 
	 * @param error the error to set
	 */
	public void setBudget(int budget) {
		this.budget = budget;
	}

	@Override
	public String getConfigurationName() {
		return "Budget Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the maximum budget allowed for the execution of the approach.";
	}
}
