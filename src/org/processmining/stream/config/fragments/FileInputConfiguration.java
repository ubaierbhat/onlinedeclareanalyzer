package org.processmining.stream.config.fragments;

import org.processmining.stream.config.interfaces.SourceConfiguration;

/**
 * This configuration is required if the stream is saved on a single file (one
 * line per event).
 * 
 * @author Andrea Burattin
 */
public class FileInputConfiguration implements SourceConfiguration {

	private String filename;

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String getConfigurationName() {
		return "File Input Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the file source with the stream.";
	}
}
