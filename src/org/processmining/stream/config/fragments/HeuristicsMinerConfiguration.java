package org.processmining.stream.config.fragments;

import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.settings.HeuristicsMinerSettings;
import org.processmining.stream.config.interfaces.Configuration;

/**
 * This configuration contains the parameters required by Heuristics Miner
 * 
 * @author Andrea Burattin
 */
public class HeuristicsMinerConfiguration implements Configuration {

	private HeuristicsMinerSettings settings;

	/**
	 * This method to get the settings
	 * 
	 * @return the settings
	 */
	public HeuristicsMinerSettings getSettings() {
		return settings;
	}

	/**
	 * This method to set the settings
	 * 
	 * @param error the settings to set
	 */
	public void setSettings(HeuristicsMinerSettings settings) {
		this.settings = settings;
	}

	@Override
	public String getConfigurationName() {
		return "Heuristics Miner Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the Heuristics Miner algorithm setup.";
	}
}
