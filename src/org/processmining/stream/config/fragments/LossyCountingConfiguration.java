package org.processmining.stream.config.fragments;

import org.processmining.stream.config.interfaces.Configuration;

/**
 * This configuration contains the parameters required by Lossy Counting
 * 
 * @author Andrea Burattin
 */
public class LossyCountingConfiguration implements Configuration {

	private double error = 0.1;

	/**
	 * This method to get the maximum approximation error
	 * 
	 * @return the error
	 */
	public double getError() {
		return error;
	}

	/**
	 * This method to set the maximum approximation error
	 * 
	 * @param error the error to set
	 */
	public void setError(double error) {
		this.error = error;
	}

	@Override
	public String getConfigurationName() {
		return "Lossy Counting Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the Lossy Counting maximum error.";
	}
}
