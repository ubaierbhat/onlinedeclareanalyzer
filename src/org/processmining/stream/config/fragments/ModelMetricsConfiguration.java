package org.processmining.stream.config.fragments;

import java.util.ArrayList;
import java.util.Collection;

import org.processmining.framework.util.Pair;
import org.processmining.stream.config.interfaces.Configuration;

/**
 * This configuration is used to set whether the plugin should refresh the model
 * and compute fitness and precision (with the corresponding frequencies)
 * 
 * @author Andrea Burattin
 */
public class ModelMetricsConfiguration implements Configuration {

	private boolean updateModel = true;
	private int modelUpdateFrequency = 1000;
	
	private ArrayList<Pair<String, String>> metricLogStartsEnds = new ArrayList<Pair<String, String>>();
	
	private boolean computeFitness = false;
	private int fitnessComputationFrequency = 1000;
	
	private boolean computePrecision = false;
	private int precisionComputationFrequency = 1000;

	/**
	 * @return the updateModel
	 */
	public boolean updateModel() {
		return updateModel;
	}

	/**
	 * @param updateModel the updateModel to set
	 */
	public void setUpdateModel(boolean updateModel) {
		this.updateModel = updateModel;
	}

	/**
	 * @return the modelUpdateFrequency
	 */
	public int getModelUpdateFrequency() {
		return modelUpdateFrequency;
	}

	/**
	 * @param modelUpdateFrequency the modelUpdateFrequency to set
	 */
	public void setModelUpdateFrequency(int modelUpdateFrequency) {
		this.modelUpdateFrequency = modelUpdateFrequency;
	}
	
	/**
	 * @return the computeFitness
	 */
	public boolean computeFitness() {
		return computeFitness;
	}
	
	/**
	 * @param computeFitness the computeFitness to set
	 */
	public void setComputeFitness(boolean computeFitness) {
		this.computeFitness = computeFitness;
	}
	
	/**
	 * @return the fitnessComputationFrequency
	 */
	public int getFitnessComputationFrequency() {
		return fitnessComputationFrequency;
	}
	
	/**
	 * @param fitnessComputationFrequency the fitnessComputationFrequency to set
	 */
	public void setFitnessComputationFrequency(int fitnessComputationFrequency) {
		this.fitnessComputationFrequency = fitnessComputationFrequency;
	}

	/**
	 * @return the computePrecision
	 */
	public boolean computePrecision() {
		return computePrecision;
	}

	/**
	 * @param computePrecision the computePrecision to set
	 */
	public void setComputePrecision(boolean computePrecision) {
		this.computePrecision = computePrecision;
	}

	/**
	 * @return the precisionComputationFrequency
	 */
	public int getPrecisionComputationFrequency() {
		return precisionComputationFrequency;
	}

	/**
	 * @param precisionComputationFrequency the precisionComputationFrequency to set
	 */
	public void setPrecisionComputationFrequency(int precisionComputationFrequency) {
		this.precisionComputationFrequency = precisionComputationFrequency;
	}

	/**
	 * @return the logStartsEnds
	 */
	public Collection<Pair<String, String>> getLogStartsEnds() {
		return metricLogStartsEnds;
	}

	/**
	 * @param logStartsEnds the logStartsEnds to set
	 */
	public void setLogStartsEnds(ArrayList<Pair<String, String>> logStartsEnds) {
		this.metricLogStartsEnds = logStartsEnds;
	}

	/**
	 * Adds a single new log start/end
	 * 
	 * @param start
	 * @param end
	 */
	public void addLogStartEnd(String start, String end) {
		this.metricLogStartsEnds.add(new Pair<String, String>(start, end));
	}

	@Override
	public String getConfigurationName() {
		return "Model and Metrics Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the model update frequency and the fitness/precision computation frequencies.";
	}
}
