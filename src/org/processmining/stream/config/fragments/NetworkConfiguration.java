package org.processmining.stream.config.fragments;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.processmining.stream.config.interfaces.SourceConfiguration;
import org.processmining.stream.utils.BasicPluginConfiguration;

/**
 * This configuration is required for the network connection.
 * 
 * @author Andrea Burattin
 */
public class NetworkConfiguration implements SourceConfiguration {

	private int port;
	private InetAddress address;
	
	/**
	 * Default constructor
	 */
	public NetworkConfiguration() {
		try {
			port = BasicPluginConfiguration.DEFAULT_NETWORK_PORT;
			this.address = InetAddress.getByName("localhost");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * @return the address
	 */
	public InetAddress getAddress() {
		return address;
	}
	
	/**
	 * @param address the address to set
	 */
	public void setAddress(InetAddress address) {
		this.address = address;
	}

	@Override
	public String getConfigurationName() {
		return "Network Source Configuration";
	}

	@Override
	public String getConfigurationDescription() {
		return "This configuration provides the stream network source.";
	}
}
