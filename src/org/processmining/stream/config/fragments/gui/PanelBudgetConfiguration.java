package org.processmining.stream.config.fragments.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JTextField;

import org.processmining.stream.annotations.OnlineConfigurationPanel;
import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.interfaces.Configuration;
import org.processmining.stream.config.interfaces.ConfigurationWizard;
import org.processmining.stream.utils.GUIUtils;

/**
 * 
 * @author Andrea Burattin
 */
@OnlineConfigurationPanel(
		configurationFor = BudgetConfiguration.class)
public class PanelBudgetConfiguration extends ConfigurationWizard {

	private static final long serialVersionUID = 2693602802701923530L;
	private BudgetConfiguration configuration = new BudgetConfiguration();
	private JTextField budgetField = null;
	
	public PanelBudgetConfiguration() {
		budgetField = GUIUtils.prepareIntegerField((int) configuration.getBudget());
		
		GridBagConstraints c = new GridBagConstraints();
		setOpaque(false);
		setLayout(new GridBagLayout());
		int indexY = 0;
		
		c.gridx = 0; c.gridy = ++indexY;
		c.gridwidth = 2;
		c.insets = new Insets(0, 0, 15, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		add(GUIUtils.prepareLabel(configuration.getConfigurationDescription()), c);

		c.gridx = 0; c.gridy = ++indexY;
		c.gridwidth = 1;
		c.insets = new Insets(5, 0, 50, 5);
		c.anchor = GridBagConstraints.NORTH;
		add(GUIUtils.prepareLabel("Budget:"), c);

		c.gridx = 1; c.gridy = indexY;
		c.insets = new Insets(0, 0, 50, 0);
		c.weightx = 1;
		c.weighty = 1;
		add(GUIUtils.wrapInRoundedPanel(budgetField), c);
	}

	@Override
	public Configuration getConfiguration() {
		configuration.setBudget(Integer.parseInt(budgetField.getText()));
		return configuration;
	}

}
