package org.processmining.stream.config.fragments.gui;

import java.awt.BorderLayout;

import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.gui.ParametersPanel;
import org.processmining.stream.annotations.OnlineConfigurationPanel;
import org.processmining.stream.config.fragments.HeuristicsMinerConfiguration;
import org.processmining.stream.config.interfaces.Configuration;
import org.processmining.stream.config.interfaces.ConfigurationWizard;
import org.processmining.stream.utils.GUIUtils;

/**
 * 
 * @author Andrea Burattin
 */
@OnlineConfigurationPanel(
		configurationFor = HeuristicsMinerConfiguration.class)
public class PanelHeuristicsMinerConfiguration extends ConfigurationWizard {

	private static final long serialVersionUID = 2693602802701923530L;
	private HeuristicsMinerConfiguration configuration = new HeuristicsMinerConfiguration();
	private ParametersPanel hmSettings;
	
	public PanelHeuristicsMinerConfiguration() {
		hmSettings = new ParametersPanel();
		
		hmSettings.setOpaque(false);
		setOpaque(false);
		setLayout(new BorderLayout());
		
		add(GUIUtils.prepareLabel(configuration.getConfigurationDescription()), BorderLayout.NORTH);
		add(hmSettings, BorderLayout.CENTER);
	}

	@Override
	public Configuration getConfiguration() {
		configuration.setSettings(hmSettings.getSettings());
		return configuration;
	}

}
