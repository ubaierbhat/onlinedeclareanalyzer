package org.processmining.stream.config.fragments.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.processmining.framework.util.Pair;
import org.processmining.stream.annotations.OnlineConfigurationPanel;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.interfaces.Configuration;
import org.processmining.stream.config.interfaces.ConfigurationWizard;
import org.processmining.stream.utils.GUIUtils;
import org.processmining.stream.utils.UIColors;

import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * 
 * @author Andrea Burattin
 */
@OnlineConfigurationPanel(
		configurationFor = ModelMetricsConfiguration.class)
public class PanelModelMetricsConfiguration extends ConfigurationWizard {

	private static final long serialVersionUID = 2693602802701923530L;
	private ModelMetricsConfiguration configuration = new ModelMetricsConfiguration();
	private JCheckBox fieldModelUpdate;
	private JTextField fieldEventsForModelUpdate;
	private JCheckBox fieldFitnessComputation;
	private JTextField fieldEventsForFitness;
	private JCheckBox fieldPrecisionComputation;
	private JTextField fieldEventsForPrecision;
	private JTextArea fieldStarts;
	private JTextArea fieldEnds;
	
	public PanelModelMetricsConfiguration() {
		fieldModelUpdate = SlickerFactory.instance().createCheckBox("Update the model", configuration.updateModel());
		fieldEventsForModelUpdate = GUIUtils.prepareIntegerField(configuration.getModelUpdateFrequency());
		fieldFitnessComputation = SlickerFactory.instance().createCheckBox("Calculate the fitness", configuration.computeFitness());
		fieldEventsForFitness = GUIUtils.prepareIntegerField(configuration.getFitnessComputationFrequency());
		fieldPrecisionComputation = SlickerFactory.instance().createCheckBox("Calculate the precision", configuration.computePrecision());
		fieldEventsForPrecision = GUIUtils.prepareIntegerField(configuration.getPrecisionComputationFrequency());
		fieldStarts = GUIUtils.prepareTextArea("");
		fieldEnds = GUIUtils.prepareTextArea("");

		fieldFitnessComputation.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fieldEventsForFitness.setEnabled(fieldFitnessComputation.isSelected());
				fieldStarts.setEnabled(fieldFitnessComputation.isSelected() || fieldPrecisionComputation.isSelected());
				fieldEnds.setEnabled(fieldFitnessComputation.isSelected() || fieldPrecisionComputation.isSelected());
			}
		});
		
		fieldPrecisionComputation.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fieldEventsForPrecision.setEnabled(fieldPrecisionComputation.isSelected());
				fieldStarts.setEnabled(fieldFitnessComputation.isSelected() || fieldPrecisionComputation.isSelected());
				fieldEnds.setEnabled(fieldFitnessComputation.isSelected() || fieldPrecisionComputation.isSelected());
			}
		});
		
		GridBagConstraints c = new GridBagConstraints();
		setOpaque(false);
		setLayout(new GridBagLayout());
		int indexY = 0;
		
		c.gridx = 0; c.gridy = ++indexY;
		c.gridwidth = 3;
		c.insets = new Insets(0, 0, 15, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		add(GUIUtils.prepareLabel("<html>" + configuration.getConfigurationDescription() + "</html>"), c);

		// model update --------------------------------------------------------
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = ++indexY;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(0, 0, 5, 0);
		add(fieldModelUpdate, c);

		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = indexY;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.prepareLabel("Events frequency: ", SwingConstants.RIGHT, UIColors.darkGray), c);
		
		c = new GridBagConstraints();
		c.gridx = 2; c.gridy = indexY;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldEventsForModelUpdate), c);
		
		// fitness -------------------------------------------------------------
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = ++indexY;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(0, 0, 5, 0);
		add(fieldFitnessComputation, c);

		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = indexY;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.prepareLabel("Events frequency: ", SwingConstants.RIGHT, UIColors.darkGray), c);
		
		c = new GridBagConstraints();
		c.gridx = 2; c.gridy = indexY;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldEventsForFitness), c);
		
		// precision -----------------------------------------------------------
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = ++indexY;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(0, 0, 15, 0);
		add(fieldPrecisionComputation, c);

		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = indexY;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 15, 0);
		add(GUIUtils.prepareLabel("Events frequency: ", SwingConstants.RIGHT, UIColors.darkGray), c);
		
		c = new GridBagConstraints();
		c.gridx = 2; c.gridy = indexY;
		c.insets = new Insets(0, 0, 15, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldEventsForPrecision), c);
		
		// spacer --------------------------------------------------------------
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = ++indexY;
		c.weighty = 100;
		add(GUIUtils.prepareLabel(""), c);
		
		// start complete ------------------------------------------------------
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = ++indexY;
		c.gridwidth = 3;
		c.insets = new Insets(0, 0, 15, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		add(GUIUtils.prepareLabel("<html>Use this form for the configuration of the "
				+ "firsts and lasts events of each trace (just for the <br>"
				+ "computation of the fitness and precision)</html>."), c);

		c.gridx = 0; c.gridy = ++indexY;
		c.gridwidth = 1;
		c.insets = new Insets(5, 0, 0, 5);
		c.anchor = GridBagConstraints.NORTH;
		add(GUIUtils.prepareLabel("Possible start events (one per line):"), c);

		c.gridx = 1; c.gridy = indexY;
		c.gridwidth = 2;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldStarts), c);
		
		c.gridx = 0; c.gridy = ++indexY;
		c.anchor = GridBagConstraints.NORTH;
		add(GUIUtils.prepareLabel("Possible end events (one per line):"), c);
		
		c.gridx = 1; c.gridy = indexY;
		c.weightx = 1; c.weighty = 1;
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.NORTH;
		add(GUIUtils.wrapInRoundedPanel(fieldEnds), c);
	}

	@Override
	public Configuration getConfiguration() {
		configuration.setUpdateModel(fieldModelUpdate.isSelected());
		configuration.setComputeFitness(fieldFitnessComputation.isSelected());
		configuration.setComputePrecision(fieldPrecisionComputation.isSelected());
		
		configuration.setModelUpdateFrequency(Integer.parseInt(fieldEventsForModelUpdate.getText()));
		configuration.setFitnessComputationFrequency(Integer.parseInt(fieldEventsForFitness.getText()));
		configuration.setPrecisionComputationFrequency(Integer.parseInt(fieldEventsForPrecision.getText()));
		
		configuration.setLogStartsEnds(getStartEndActivities());
		
		return configuration;
	}
	
	/**
	 * This method generates an {@link ArrayList} of {@link Pair}. Each pair
	 * consists of a possible combination of start and end of the traces.
	 * 
	 * @return
	 */
	private ArrayList<Pair<String, String>> getStartEndActivities() {
		if (fieldStarts == null || fieldEnds == null) {
			return null;
		}
		
		String[] starts = fieldStarts.getText().split("\n");
		String[] ends = fieldEnds.getText().split("\n");
		
		if (starts.length == 0 || ends.length == 0) {
			return null;
		}
		
		ArrayList<Pair<String, String>> toReturn = new ArrayList<Pair<String, String>>();
		for (String s : starts) {
			for (String e : ends) {
				s = s.trim();
				e = e.trim();
				if (s.length() > 0 && e.length() > 0) {
					toReturn.add(new Pair<String, String>(s, e));
				}
			}
		}
		return toReturn;
	}
}
