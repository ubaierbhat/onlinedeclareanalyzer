package org.processmining.stream.config.fragments.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JTextField;

import org.processmining.stream.annotations.OnlineConfigurationPanel;
import org.processmining.stream.config.fragments.NetworkConfiguration;
import org.processmining.stream.config.interfaces.Configuration;
import org.processmining.stream.config.interfaces.ConfigurationWizard;
import org.processmining.stream.utils.GUIUtils;

/**
 * 
 * @author Andrea Burattin
 */
@OnlineConfigurationPanel(
		configurationFor = NetworkConfiguration.class)
public class PanelNetworkConfiguration extends ConfigurationWizard {

	private static final long serialVersionUID = 2693602802701923530L;
	private NetworkConfiguration configuration = new NetworkConfiguration();
	private JTextField fieldPort;
	private JTextField fieldAddress;
	
	public PanelNetworkConfiguration() {
		fieldPort = GUIUtils.prepareIntegerField(configuration.getPort());
		fieldAddress = GUIUtils.prepareIPField(configuration.getAddress().toString());
		GridBagConstraints c = new GridBagConstraints();
		
		setOpaque(false);
		setLayout(new GridBagLayout());
		
		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(0, 0, 15, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		add(GUIUtils.prepareLabel(configuration.getConfigurationDescription()), c);
		
		c.gridx = 0; c.gridy = 1;
		c.insets = new Insets(0, 0, 5, 0);
		add(GUIUtils.prepareLabel("Stream source port:"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = 1;
		c.weightx = 1; c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 5, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldPort), c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 2;
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(5, 0, 5, 0);
		add(GUIUtils.prepareLabel("Stream source address:"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = 2;
		c.weightx = 1; c.weighty = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(0, 5, 5, 0);
		add(GUIUtils.wrapInRoundedPanel(fieldAddress), c);
	}

	@Override
	public Configuration getConfiguration() {
		try {
			configuration.setAddress(InetAddress.getByName(fieldAddress.getText()));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		configuration.setPort(Integer.parseInt(fieldPort.getText()));
		return configuration;
	}

}
