package org.processmining.stream.config.interfaces;


/**
 * This interface is used only for reference purposes.
 * 
 * @author Andrea Burattin
 */
public interface Configuration {

	/**
	 * This method returns the name of the configuration
	 * 
	 * @return the name of the configuration
	 */
	public String getConfigurationName();
	
	/**
	 * This method returns a description of the provided configuration.
	 * 
	 * @return the description of the configuration
	 */
	public String getConfigurationDescription();
}
