package org.processmining.stream.config.interfaces;

import javax.swing.JPanel;

/**
 * This interface must be implemented by all panels that are used for the
 * configuration of a fragment
 * 
 * @author Andrea Burattin
 */
public abstract class ConfigurationWizard extends JPanel {

	private static final long serialVersionUID = 8895773956879666016L;

	/**
	 * This method returns the actual configuration
	 * 
	 * @return the actual configuration
	 */
	public abstract Configuration getConfiguration();
}
