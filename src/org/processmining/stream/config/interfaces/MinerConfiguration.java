package org.processmining.stream.config.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.processmining.stream.exceptions.MissingConfigurationException;


/**
 * This interface represents the main interface to the configuration of mining
 * plugins.
 * 
 * @author Andrea Burattin
 */
public abstract class MinerConfiguration {
	
	protected List<Class<? extends Configuration>> requiredConfigurations = new ArrayList<Class<? extends Configuration>>();
	protected ArrayList<Configuration> configurations = new ArrayList<Configuration>();

	/**
	 * This method adds a new configuration to the current miner configuration
	 * 
	 * @param configuration the new configuration perspective to add
	 */
	public void addConfiguration(Configuration configuration) {
		configurations.add(configuration);
	}

	/**
	 * This method returns the configuration required
	 * 
	 * @param requiredConfiguration the configuration to return
	 * @return the required configuration, or <code>null</code> if it is not
	 * available
	 */
	public Configuration getConfiguration(Class<? extends Configuration> requiredConfiguration) {
		for (Configuration configuration : configurations) {
			Class<?> configurationClass = configuration.getClass();
			if (requiredConfiguration.isAssignableFrom(configurationClass)) {
				return configuration;
			}
		}
		return null;
	}

	/**
	 * This method checks whether the provided configuration is available or not
	 * 
	 * @param requiredConfiguration the configuration to check
	 * @return <code>true</code> if the required configuration is available,
	 * <code>false</code> otherwise
	 */
	public boolean containsConfiguration(Class<? extends Configuration> requiredConfiguration) {
		for (Configuration configuration : configurations) {
			Class<?> configurationClass = configuration.getClass();
			if (requiredConfiguration.isAssignableFrom(configurationClass)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method validates the current configuration with respect to the
	 * required configurations
	 * 
	 * @return <code>true</code> if the current configuration fulfills all
	 * required parameters, <code>false</code> otherwise
	 * @throws MissingConfigurationException 
	 */
	public boolean validateConfiguration() throws MissingConfigurationException {
		for (Class<? extends Configuration> requiredConfiguration : requiredConfigurations) {
			if (!containsConfiguration(requiredConfiguration)) {
				throw new MissingConfigurationException("Required `" + requiredConfiguration.getCanonicalName() + "' which is missing!");
			}
		}
		return true;
	}
	
	/**
	 * This method returns the list of configurations required by the algorithm
	 * 
	 * @return the list of configuration classes required
	 */
	public List<Class<? extends Configuration>> getRequiredConfigurations() {
		return requiredConfigurations;
	}
}
