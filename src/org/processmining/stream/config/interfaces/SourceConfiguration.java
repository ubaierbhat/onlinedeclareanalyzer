package org.processmining.stream.config.interfaces;

/**
 * This intermediate class is used to identify different source types.
 * 
 * @author Andrea Burattin
 */
public interface SourceConfiguration extends Configuration {

}
