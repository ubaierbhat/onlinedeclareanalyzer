package org.processmining.stream.exceptions;

/**
 * This class identifies an error caused by a class which is not an online
 * algorithm
 * 
 * @author Andrea Burattin
 */
public class ClassIsNotOnlineAlgorithmException extends OnlineException {

	private static final long serialVersionUID = 7160655016991996632L;

	/**
	 * Constructs a new exception with the specified detail message
	 * 
	 * @param message the detail message
	 */
	public ClassIsNotOnlineAlgorithmException(String message) {
		super(message);
	}
}
