package org.processmining.stream.exceptions;

/**
 * This class identifies a missing configuration
 * 
 * @author Andrea Burattin
 */
public class MissingConfigurationException extends OnlineException {

	private static final long serialVersionUID = 7160655016991996632L;

	/**
	 * Constructs a new exception with the specified detail message
	 * 
	 * @param message the detail message
	 */
	public MissingConfigurationException(String message) {
		super(message);
	}
}
