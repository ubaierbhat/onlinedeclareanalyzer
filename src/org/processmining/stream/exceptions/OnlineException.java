package org.processmining.stream.exceptions;

/**
 * A general exception that the online mining package might throw
 * 
 * @author Andrea Burattin
 */
public abstract class OnlineException extends Exception {

	private static final long serialVersionUID = 2315913610435877699L;

	/**
	 * Constructs a new exception with the specified detail message
	 * 
	 * @param message the detail message
	 */
	public OnlineException(String message) {
		super(message);
	}
}
