package org.processmining.stream.exceptions;

/**
 * An exception that indicates that the setup of an online plugin was not
 * correct
 * 
 * @author Andrea Burattin
 */
public class PluginMissingException extends OnlineException {

	private static final long serialVersionUID = 5045114184193074535L;

	/**
	 * Constructs a new exception with the specified detail message
	 * 
	 * @param message the detail message
	 */
	public PluginMissingException(String message) {
		super(message);
	}
}
