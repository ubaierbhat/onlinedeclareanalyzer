package org.processmining.stream.exceptions;

/**
 * This class identifies a wrong configuration exception
 * 
 * @author Andrea Burattin
 */
public class WrongConfigurationException extends OnlineException {

	private static final long serialVersionUID = 7160655016991996632L;

	/**
	 * Constructs a new exception with the specified detail message
	 * 
	 * @param message the detail message
	 */
	public WrongConfigurationException(String message) {
		super(message);
	}
}
