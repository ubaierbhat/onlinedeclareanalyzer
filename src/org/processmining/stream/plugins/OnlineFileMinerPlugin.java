package org.processmining.stream.plugins;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.deckfour.xes.model.XTrace;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.config.fragments.FileInputConfiguration;

/**
 * This class generates an online miner from file
 * 
 * @author Andrea Burattin
 */
public abstract class OnlineFileMinerPlugin extends OnlineMinerPlugin {

	@Override
	public void start(final OnlineMiningAlgorithm onlineAlgorithm) {
		minerRunning = true;
		
		final FileInputConfiguration configuration =
				(FileInputConfiguration) onlineAlgorithm.getConfiguration().
					getConfiguration(FileInputConfiguration.class);
		
		if (configuration != null) {
			miner = new Thread(new Runnable() {
				public void run() {
					try {
						FileInputStream fstream = new FileInputStream(configuration.getFilename());
						DataInputStream in = new DataInputStream(fstream);
						BufferedReader br = new BufferedReader(new InputStreamReader(in));
						String streamLine;
						XTrace t;
						while (minerRunning && (streamLine = br.readLine()) != null) {
							t = (XTrace) converter.fromXML(streamLine);
							onlineAlgorithm.observe(t);
							inc();
						}
						br.close();
						in.close();
						fstream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					notifyFinish();
				}
			});
			miner.start();
		}
	}
}
