package org.processmining.stream.plugins;

import org.processmining.operationalsupport.xml.OSXMLConverter;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;

/**
 * This class represent a general wrapper of a mining instance. We expect that
 * each mining plugin will be based on this class.
 * 
 * @author Andrea Burattin
 */
public abstract class OnlineMinerPlugin {

	protected boolean minerRunning = true;
	protected OSXMLConverter converter = new OSXMLConverter();
	protected Thread miner;
	
	/**
	 * This method starts the online mining
	 */
	public abstract void start(OnlineMiningAlgorithm algorithm);
	
	/**
	 * This method stops the online mining
	 */
	public void stop() {
		minerRunning = false;
	}
	
	/**
	 * This method is called each time an event has been processed
	 */
	public abstract void inc();
	
	/**
	 * This method is called when the stream connection is over
	 */
	public abstract void notifyFinish();
	
	/**
	 * This method is called when the algorithm generates a new model
	 * 
	 * @param newModel the new model generated
	 */
	public abstract void onModelUpdate(Object newModel);
	
	/**
	 * This method is called when the algorithm generates a new fitness value
	 * 
	 * @param newFitnessValue the new fitness value
	 */
	public abstract void onNewFitnessValue(double newFitnessValue);
	
	/**
	 * This method is called when the algorithm generates a new precision value
	 * 
	 * @param newFitnessValue the new precision value
	 */
	public abstract void onNewPrecisionValue(double newPrecisionValue);
}
