package org.processmining.stream.plugins;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.deckfour.xes.model.XTrace;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.config.fragments.NetworkConfiguration;

/**
 * This class generates an online miner from a network source
 * 
 * @author Andrea Burattin
 */
public abstract class OnlineNetworkMinerPlugin extends OnlineMinerPlugin {

	@Override
	public void start(final OnlineMiningAlgorithm onlineAlgorithm) {
		
		final NetworkConfiguration configuration =
				(NetworkConfiguration) onlineAlgorithm.getConfiguration().
					getConfiguration(NetworkConfiguration.class);
		
		if (configuration != null) {
			miner = new Thread(new Runnable() {
				public void run() {
					try {
						Socket s = new Socket(configuration.getAddress(), configuration.getPort());
						BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
						String streamLine;
						XTrace t;
						while (minerRunning && (streamLine = br.readLine()) != null) {
							t = (XTrace) converter.fromXML(streamLine);
							onlineAlgorithm.observe(t);
							inc();
						}
						br.close();
						s.close();
		
					} catch (IOException e) {
						e.printStackTrace();
					}
					notifyFinish();
				}
			});
			miner.start();
		}
	}
}
