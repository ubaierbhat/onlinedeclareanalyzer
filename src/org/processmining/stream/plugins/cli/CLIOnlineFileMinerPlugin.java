package org.processmining.stream.plugins.cli;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.processmining.models.cnet.CNet;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.MissingConfigurationException;
import org.processmining.stream.exceptions.OnlineException;
import org.processmining.stream.exceptions.WrongConfigurationException;
import org.processmining.stream.plugins.OnlineFileMinerPlugin;

/**
 * This class contains a CLI miner container for streams on file
 * 
 * @author Andrea Burattin
 */
public class CLIOnlineFileMinerPlugin extends OnlineFileMinerPlugin {

	/**
	 * This field contains the date format for the logging text.
	 */
	public static final String DATE_FORMAT_NOW = "HH:mm:ss.SSS";
	
	/**
	 * This is the actual mining method
	 * 
	 * @param algorithm the mining algorithm
	 * @param configuration the mining configuration
	 * @throws MissingConfigurationException
	 * @throws WrongConfigurationException 
	 */
	public void mine(OnlineMiningAlgorithm algorithm, MinerConfiguration configuration) throws OnlineException {
		algorithm.prepareMiner(configuration);
		start(algorithm);
	}

	@Override
	public void inc() { }

	@Override
	public void notifyFinish() {}

	@Override
	public void onModelUpdate(Object newModel) {
		if (newModel instanceof CNet) {
			System.out.println(now() + " model updated --> " + ((CNet)newModel).getNodes().size());
		} else if (newModel instanceof HeuristicsNet) {
			System.out.println(now() + " model updated --> " + ((HeuristicsNet)newModel).getActivitiesActualFiring().length);
		}
	}

	@Override
	public void onNewFitnessValue(double newFitnessValue) {
		System.out.println(now() + " new fitness value: " + newFitnessValue);
	}

	@Override
	public void onNewPrecisionValue(double newPrecisionValue) {
		System.out.println(now() + " new precision value: " + newPrecisionValue);
	}
	
	/**
	 * This method returns the current time.
	 * 
	 * @return a string representation of the current time.
	 * @see #DATE_FORMAT_NOW
	 */
	private String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
}
