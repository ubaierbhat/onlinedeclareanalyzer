package org.processmining.stream.plugins.cli;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.processmining.stream.algorithms.SpaceSavingHM;
import org.processmining.stream.config.algorithms.SpaceSavingMinerConfiguration;
import org.processmining.stream.config.fragments.BudgetConfiguration;
import org.processmining.stream.config.fragments.ModelMetricsConfiguration;
import org.processmining.stream.config.fragments.NetworkConfiguration;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.OnlineException;


public class CLITester {

	public static void main(String[] args) throws OnlineException, UnknownHostException {
		
		MinerConfiguration c = new SpaceSavingMinerConfiguration();
//		MinerConfiguration c = new BudgetLossyCountingMinerConfiguration();
		
//		FileInputConfiguration fic = new FileInputConfiguration();
//		fic.setFilename("/home/delas/work-tmp/papers-related/2014-wcci/experiment1/log1.stream");
//		fic.setFilename("/home/delas/work-tmp/papers-related/2014-wcci/experiment-cluster/log/bpi12.stream");
//		c.addConfiguration(fic);
		NetworkConfiguration nc = new NetworkConfiguration();
		nc.setAddress(InetAddress.getByName("127.0.0.1"));
		nc.setPort(1234);
		c.addConfiguration(nc);
		
		ModelMetricsConfiguration mmc = new ModelMetricsConfiguration();
		mmc.setUpdateModel(true);
		mmc.setModelUpdateFrequency(500);
		mmc.setComputeFitness(true);
		mmc.setFitnessComputationFrequency(500);
		mmc.setComputePrecision(true);
		mmc.setPrecisionComputationFrequency(500);
//		mmc.addLogStartEnd("A", "B");
		c.addConfiguration(mmc);
		
//		LossyCountingConfiguration lcc = new LossyCountingConfiguration();
//		lcc.setError(0.01);
//		c.addConfiguration(lcc);
		
		BudgetConfiguration bc = new BudgetConfiguration();
		bc.setBudget(500);
		c.addConfiguration(bc);
		
//		HeuristicsMinerConfiguration hmc = new HeuristicsMinerConfiguration();
//		hmc.setSettings(new HeuristicsMinerSettings());
//		c.addConfiguration(hmc);
		
		CLIOnlineFileMinerPlugin miner = new CLIOnlineFileMinerPlugin();
		miner.mine(new SpaceSavingHM(miner), c);
//		miner.mine(new BudgetLossyCountingHM(miner), c);
		
		System.out.println("done");
		
	}
}
