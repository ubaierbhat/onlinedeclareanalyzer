package org.processmining.stream.plugins.context;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.impl.AbstractGlobalContext;

public class CLIStreamContext extends AbstractGlobalContext {

	private final CLIStreamPluginContext mainPluginContext;

	public CLIStreamContext() {
		super();

		mainPluginContext = new CLIStreamPluginContext(this, "Main Plugin Context");
	}

	@Override
	protected CLIStreamPluginContext getMainPluginContext() {
		return mainPluginContext;
	}

	@Override
	public Class<? extends PluginContext> getPluginContextType() {
		return CLIStreamPluginContext.class;
	}
}
