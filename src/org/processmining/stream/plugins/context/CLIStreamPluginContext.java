package org.processmining.stream.plugins.context;

import org.processmining.contexts.cli.CLIPluginContext;
import org.processmining.framework.plugin.GlobalContext;
import org.processmining.framework.plugin.impl.AbstractPluginContext;

public class CLIStreamPluginContext extends CLIPluginContext {

	
	public CLIStreamPluginContext(GlobalContext context, String label) {
		super(context, label);
		progress = new CLIDummyProgress();
	}
	
	public CLIStreamPluginContext(AbstractPluginContext context, String label) {
		super(context, label);
		progress = new CLIDummyProgress();
	}

}
