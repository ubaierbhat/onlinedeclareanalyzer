package org.processmining.stream.plugins.gui;

import org.processmining.framework.annotations.AuthoredType;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.utils.BasicPluginConfiguration;

/**
 * Class with the basic configuration of the stream miner. This configuration is
 * composed of the algorithm-specific requirements
 * 
 * @author Andrea Burattin
 */
@AuthoredType(
	typeName = "Stream Miner Configuration",
	author = BasicPluginConfiguration.AUTHOR,
	email = BasicPluginConfiguration.EMAIL,
	affiliation = BasicPluginConfiguration.AFFILIATION
)
public class GUIStreamConfiguration {

	private OnlineMiningAlgorithm algorithm;
	private MinerConfiguration configuration;
	
	/**
	 * @return the algorithm
	 */
	public OnlineMiningAlgorithm getAlgorithm() {
		return algorithm;
	}
	
	/**
	 * @param algorithm the algorithm to set
	 */
	public void setAlgorithm(OnlineMiningAlgorithm algorithm) {
		this.algorithm = algorithm;
	}
	
	/**
	 * @return the configuration
	 */
	public MinerConfiguration getConfiguration() {
		return configuration;
	}
	
	/**
	 * @param configuration the configuration to set
	 */
	public void setConfiguration(MinerConfiguration configuration) {
		this.configuration = configuration;
	}
}
