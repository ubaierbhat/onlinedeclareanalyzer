package org.processmining.stream.plugins.gui;

import java.util.ArrayList;
import java.util.List;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.util.Pair;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.annotations.OnlineAlgorithm;
import org.processmining.stream.annotations.OnlineConfigurationPanel;
import org.processmining.stream.config.fragments.NetworkConfiguration;
import org.processmining.stream.config.interfaces.Configuration;
import org.processmining.stream.config.interfaces.ConfigurationWizard;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.config.interfaces.SourceConfiguration;
import org.processmining.stream.plugins.gui.wizards.AlgorithmSelection;
import org.processmining.stream.utils.BasicPluginConfiguration;

/**
 * Class with the plugin for the construction of the miner configuration object.
 * This plugin assumes that the source of the stream is a network source.
 * 
 * @author Andrea Burattin
 */
public class GUIStreamMinerConfigurator {

	private PluginContext context;
	
	@Plugin(
		name = "Stream Miner",
		parameterLabels = { },
		returnLabels = { "A miner configuration" },
		returnTypes = { GUIStreamConfiguration.class },
		userAccessible = true
	)
	@UITopiaVariant(
		author = BasicPluginConfiguration.AUTHOR,
		email = BasicPluginConfiguration.EMAIL,
		affiliation = BasicPluginConfiguration.AFFILIATION
	)
	public GUIStreamConfiguration createStreamConfiguration(UIPluginContext context) {
		this.context = context;
		
		// configuration to select the mining algorithm
		AlgorithmSelection algorithmSelector = new AlgorithmSelection(context);
		if (context.showConfiguration("Select the Mining Algorithm to Use", algorithmSelector) == InteractionResult.CANCEL) {
			return null;
		}
		
		try {
			String streamName = "Stream Configuration";
			Class<? extends OnlineMiningAlgorithm> algorithmSelected = algorithmSelector.getSelectedAlgorithm();
			
			// build the list of configurations required
			OnlineMiningAlgorithm algorithm = algorithmSelected.newInstance();
			MinerConfiguration minerConfig = algorithmSelected.getAnnotation(OnlineAlgorithm.class).requiredConfiguration().newInstance();
			List<Pair<Configuration, ConfigurationWizard>> configurations = new ArrayList<Pair<Configuration, ConfigurationWizard>>();
			
			for (Class<? extends Configuration> config : minerConfig.getRequiredConfigurations()) {
				// this configuration plugin assumes that the stream source is a
				// network source
				if (config.equals(SourceConfiguration.class)) {
					config = NetworkConfiguration.class;
				}
				Configuration c = config.newInstance();
				configurations.add(new Pair<Configuration, ConfigurationWizard>(c, fromConfigurationToWizard(config)));
			}
			
			// show the configuration panel for each required configuration
			InteractionResult result = InteractionResult.NEXT;
			int currentStep = 0;
			int nofSteps = configurations.size();
			boolean configurationOngoing = true;
			while (configurationOngoing && currentStep < nofSteps) {
				Pair<Configuration, ConfigurationWizard> config = configurations.get(currentStep);
				result = context.showWizard(
						config.getFirst().getConfigurationName(),
						currentStep == 0,
						currentStep == nofSteps - 1,
						config.getSecond());
				
				switch (result) {
					case NEXT :
						currentStep++;
						break;
					case PREV :
						currentStep--;
						break;
					case FINISHED :
						configurationOngoing = false;
						break;
					case CANCEL :
						return null;
					default :
						configurationOngoing = false;
						break;
				}
			}
			if (result != InteractionResult.FINISHED) {
				return null;
			}
			for (int i = 0; i < configurations.size(); i++) {
				Pair<Configuration, ConfigurationWizard> config = configurations.get(i);
				minerConfig.addConfiguration(config.getSecond().getConfiguration());
				
				if (config.getFirst() instanceof NetworkConfiguration) {
					streamName = "Stream from " 
							+ ((NetworkConfiguration) config.getFirst()).getAddress() 
							+ ":"
							+ ((NetworkConfiguration) config.getFirst()).getPort();
				}
			}
			
			// wrap everything into a single object to be shipped
			GUIStreamConfiguration configuration = new GUIStreamConfiguration();
			configuration.setAlgorithm(algorithm);
			configuration.setConfiguration(minerConfig);
			
			context.getFutureResult(0).setLabel(streamName);
			return configuration;
			
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private ConfigurationWizard fromConfigurationToWizard(Class<? extends Configuration> c) throws InstantiationException, IllegalAccessException {
		for (Class<?> configurationPanel : context.getPluginManager().getKnownClassesAnnotatedWith(OnlineConfigurationPanel.class)) {
			if (c.equals(configurationPanel.getAnnotation(OnlineConfigurationPanel.class).configurationFor()) &&
					ConfigurationWizard.class.isAssignableFrom(configurationPanel)) {
				return (ConfigurationWizard) configurationPanel.newInstance();
			}
		}
		return null;
	}
}
