package org.processmining.stream.plugins.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.contexts.uitopia.model.ProMResource;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.cnet.CNet;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.cnet.CNetVisualization;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.config.interfaces.MinerConfiguration;
import org.processmining.stream.exceptions.OnlineException;
import org.processmining.stream.plugins.OnlineNetworkMinerPlugin;
import org.processmining.stream.utils.BasicPluginConfiguration;
import org.processmining.stream.utils.GUIUtils;
import org.processmining.stream.utils.UIColors;
import org.processmining.streamer.gui.FitnessPrecisionVisualizer;

import com.fluxicon.slickerbox.components.RoundedPanel;
import com.fluxicon.slickerbox.components.SlickerTabbedPane;
import com.fluxicon.slickerbox.factory.SlickerDecorator;
import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * This contains the main visualizer of a network miner
 * 
 * @author Andrea Burattin
 */
public class GUIStreamMinerVisualizer extends OnlineNetworkMinerPlugin {

	private OnlineMiningAlgorithm algorithm;
	private MinerConfiguration minerConfiguration;
	
	private static DecimalFormat twoDForm = new DecimalFormat("#.##");
	private Date statsStreamStartDate;
	private int statsEventsReceived = 0;
	
	private UIPluginContext context;
	private JPanel mainContainer;
	private JPanel mineResult;
	private JLabel statsStreamStart;
	private JLabel statsEventsReceivedLabel;
	private JLabel statsEventsThroughput;
	private FitnessPrecisionVisualizer measuresVisualizer;
	private JButton start;
	private JButton stop;
	private JTextArea logger;
	
	@Plugin(
		name = "Stream Miner Visualizer",
		parameterLabels = { "A stream miner configuration" },
		returnLabels = { "Stream miner visualization" },
		returnTypes = { JComponent.class },
		userAccessible = false
	)
	@UITopiaVariant(
		author = BasicPluginConfiguration.AUTHOR,
		email = BasicPluginConfiguration.EMAIL,
		affiliation = BasicPluginConfiguration.AFFILIATION
	)
	@Visualizer(name = "Stream Miner Visualizer")
	public JComponent visualize(UIPluginContext context, GUIStreamConfiguration configuration) {
		this.context = context;
		this.minerConfiguration = configuration.getConfiguration();
		this.algorithm = configuration.getAlgorithm();
		
		try {
			this.algorithm.setPlugin(this);
			this.algorithm.prepareMiner(minerConfiguration);
		} catch (OnlineException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(mainContainer, e.getLocalizedMessage(),
					"Online Error!", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		initComponents();
		return mainContainer;
	}
	
	@Override
	public void inc() {
		statsEventsReceived++;
		
		// refresh the throughput every 10 traces
		if (statsEventsReceived % 5 == 0) {
			refreshStats();
		}
		
		// clean up the workspace
		if (statsEventsReceived % 100 == 0) {
			workspaceCleanup();
		}
	}

	@Override
	public void notifyFinish() {
		stop.setEnabled(false);
		start.setEnabled(true);
	}

	@Override
	public void onModelUpdate(final Object newModel) {
		new Thread(new Runnable() {
			public void run() {
				log("MODEL", "refreshing model");
				JComponent visualizer = null;
				
				if (newModel instanceof CNet) {
					try {
						CNetVisualization visualization = new CNetVisualization();
						visualizer = visualization.visualize(context, (CNet) newModel);
					} catch(Exception e) {
						e.printStackTrace();
					}
				} else if (newModel instanceof HeuristicsNet) {
					// TODO: FIXME
				}
				mineResult.removeAll();
				mineResult.add(visualizer, BorderLayout.CENTER);
			}
		}).start();
	}

	@Override
	public void onNewFitnessValue(double newFitnessValue) {
		measuresVisualizer.addFitnessObservation(newFitnessValue);
	}

	@Override
	public void onNewPrecisionValue(double newPrecisionValue) {
		measuresVisualizer.addPrecisionObservation(newPrecisionValue);
	}
	
	/*
	 * Logging method
	 */
	private void log(String component, String message) {
		message = "[" + System.currentTimeMillis() + "] " + component.toUpperCase() + " :: " + message;
		logger.setText(logger.getText() + "\n" + message);
	}

	/*
	 * Method to refresh the value of the label that describes the throughput
	 */
	private void refreshStats() {
		if (statsStreamStartDate == null) {
			statsEventsThroughput.setText("n.a.");
			statsEventsReceivedLabel.setText("n.a.");
		} else {
			Date now = new Date();
			long millisecs = now.getTime() - statsStreamStartDate.getTime();
			Double throughput = (double) (statsEventsReceived / (millisecs / 1000.));
			statsEventsThroughput.setText("" + twoDForm.format(throughput) + " events/sec");
			statsEventsReceivedLabel.setText("" + statsEventsReceived);
		}
	}
	
	/*
	 * Graphical components initializer
	 */
	private void initComponents() {
		mainContainer = new JPanel();
		
		mainContainer.setBackground(new Color(40, 40, 40));
		mainContainer.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		// main collector
		// ---------------------------------------------------------------------
		SlickerTabbedPane visualizerContainer = SlickerFactory.instance().createTabbedPane("", UIColors.lightGray, UIColors.darkGray, UIColors.lightGray);		
		mineResult = new RoundedPanel(15, 5, 3);
		mineResult.setBackground(Color.WHITE);
		mineResult.setLayout(new BorderLayout());
		JLabel nothingToDisplay = new JLabel("Nothing to display...", JLabel.CENTER);
		nothingToDisplay.setForeground(UIColors.lightLightGray);
		mineResult.add(nothingToDisplay, BorderLayout.CENTER);
		
		logger = new JTextArea();
		logger.setEditable(false);
		logger.setBackground(UIColors.lightLightGray);
		logger.setForeground(UIColors.gray);
		logger.setSelectionColor(UIColors.lightGray);
		logger.setSelectedTextColor(UIColors.darkGray);
		
		JScrollPane scrollLogger = new JScrollPane(logger);
		scrollLogger.setBackground(UIColors.lightLightGray);
		scrollLogger.setBorder(BorderFactory.createEmptyBorder());
		SlickerDecorator.instance().decorate(scrollLogger.getHorizontalScrollBar(), UIColors.lightLightGray, UIColors.gray, UIColors.lightGray);
		SlickerDecorator.instance().decorate(scrollLogger.getVerticalScrollBar(), UIColors.lightLightGray, UIColors.gray, UIColors.lightGray);
		
		visualizerContainer.addTab("Miner result", mineResult);
		visualizerContainer.addTab("Log", GUIUtils.wrapInRoundedPanel(scrollLogger));
		

		// collector stream stats
		// ---------------------------------------------------------------------
		statsStreamStart = GUIUtils.prepareLabel("n.a.", SwingConstants.LEFT, UIColors.darkGray);
		statsEventsReceivedLabel = GUIUtils.prepareLabel("n.a.", SwingConstants.LEFT, UIColors.darkGray);
		statsEventsThroughput = GUIUtils.prepareLabel("n.a.", SwingConstants.LEFT, UIColors.darkGray);

		RoundedPanel streamStats = new RoundedPanel(15, 5, 3);
		streamStats.setLayout(new BorderLayout());
		streamStats.setBackground(UIColors.lightGray);
		streamStats.setMinimumSize(new Dimension(250, 250));
		streamStats.setPreferredSize(new Dimension(250, 250));
		streamStats.add(GUIUtils.prepareTitle("Stream stats"), BorderLayout.NORTH);

		JPanel streamStatsPanel = new JPanel();
		streamStatsPanel.setLayout(new BoxLayout(streamStatsPanel, BoxLayout.Y_AXIS));
		streamStatsPanel.setOpaque(false);
		streamStatsPanel.add(GUIUtils.prepareLabel("Stream start", SwingConstants.RIGHT, UIColors.gray));
		streamStatsPanel.add(statsStreamStart);
		streamStatsPanel.add(Box.createVerticalStrut(10));
		streamStatsPanel.add(GUIUtils.prepareLabel("Events observed", SwingConstants.RIGHT, UIColors.gray));
		streamStatsPanel.add(statsEventsReceivedLabel);
		streamStatsPanel.add(Box.createVerticalStrut(10));
		streamStatsPanel.add(GUIUtils.prepareLabel("Events throughput", SwingConstants.RIGHT, UIColors.gray));
		streamStatsPanel.add(statsEventsThroughput);
		streamStatsPanel.add(Box.createVerticalStrut(10));

		streamStats.add(streamStatsPanel, BorderLayout.CENTER);

		// stream configuration
		// ---------------------------------------------------------------------
		start = SlickerFactory.instance().createButton("Start");
		stop = SlickerFactory.instance().createButton("Stop");
		stop.setEnabled(false);
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				statsStreamStartDate = new Date();
				statsEventsReceived = 0;
				refreshStats();
				
				start(algorithm);

				stop.setEnabled(true);
				start.setEnabled(false);
			}
		});
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stop();

				stop.setEnabled(false);
				start.setEnabled(true);
			}
		});

		RoundedPanel streamConfiguration = new RoundedPanel(15, 5, 3);
		streamConfiguration.setBackground(UIColors.lightGray);
		streamConfiguration.setMinimumSize(new Dimension(250, 100));
		streamConfiguration.setPreferredSize(new Dimension(250, 100));
		streamConfiguration.setLayout(new BorderLayout());
		streamConfiguration.add(GUIUtils.prepareTitle("Stream configuration"), BorderLayout.NORTH);
		JPanel buttonContainer = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonContainer.setOpaque(false);
		buttonContainer.add(start);
		buttonContainer.add(stop);
		streamConfiguration.add(buttonContainer, BorderLayout.CENTER);
		
		// fitness visualizer
		// ---------------------------------------------------------------------
		RoundedPanel fitnessVisualizerContainer = new RoundedPanel(15, 5, 3);
		fitnessVisualizerContainer.setBackground(UIColors.lightGray);
		fitnessVisualizerContainer.setMinimumSize(new Dimension(250, 200));
		fitnessVisualizerContainer.setPreferredSize(new Dimension(250, 200));
		fitnessVisualizerContainer.setLayout(new BorderLayout());
		JLabel qualityMeasuresTitle = GUIUtils.prepareTitle("Quality measures");
		qualityMeasuresTitle.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		fitnessVisualizerContainer.add(qualityMeasuresTitle, BorderLayout.NORTH);
		
		measuresVisualizer = new FitnessPrecisionVisualizer(UIColors.lightGray);
		fitnessVisualizerContainer.add(measuresVisualizer, BorderLayout.CENTER);
		
		// memory visualizer
		// ---------------------------------------------------------------------
		RoundedPanel memoryUsedContainer = new RoundedPanel(15, 5, 3);
		memoryUsedContainer.setBackground(UIColors.lightGray);
		memoryUsedContainer.setMinimumSize(new Dimension(250, 200));
		memoryUsedContainer.setPreferredSize(new Dimension(250, 200));
		memoryUsedContainer.setLayout(new BorderLayout());
		memoryUsedContainer.add(GUIUtils.prepareLabel("Memory used", SwingConstants.LEFT, UIColors.gray), BorderLayout.NORTH);
		
		//memoryUsed = new MemoryUsage(UIColors.lightGray);
		//memoryUsedContainer.add(memoryUsed, BorderLayout.CENTER);


		// add everything to the main panel
		// ---------------------------------------------------------------------
		JPanel sideContainer = new JPanel();
		sideContainer.setOpaque(false);
		sideContainer.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.VERTICAL;
		sideContainer.add(streamStats, c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		sideContainer.add(streamConfiguration, c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		sideContainer.add(fitnessVisualizerContainer, c);

		mainContainer.setLayout(new BorderLayout());
		mainContainer.add(sideContainer, BorderLayout.WEST);
		mainContainer.add(visualizerContainer, BorderLayout.CENTER);
		
		log("GUI", "Interface loaded");
	}
	
	/*
	 * This method to clean up the workspace of all the layout that are
	 * generated. It should not be required, but with this method the execution
	 * is MUCH better
	 */
	private void workspaceCleanup() {
		new Thread(new Runnable() {
			public void run() {
				for (ProMResource<?> r : context.getGlobalContext().getResourceManager().getAllResources()) {
					if (r.getType().getTypeName().equals("GraphLayoutConnection")) {
						r.destroy();
					}
				}
			}
		}).start();
	}
}
