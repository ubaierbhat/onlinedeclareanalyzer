package org.processmining.stream.plugins.gui.wizards;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import org.processmining.framework.plugin.PluginContext;
import org.processmining.stream.algorithms.OnlineMiningAlgorithm;
import org.processmining.stream.annotations.OnlineAlgorithm;

/**
 * Class for the selection of an algorithm
 * 
 * @author Andrea Burattin
 */
public class AlgorithmSelection extends JPanel {

	private static final long serialVersionUID = 2592131911123427965L;
	private JList<Class<? extends OnlineMiningAlgorithm>> listOnlineAlgorithms = new JList<Class<? extends OnlineMiningAlgorithm>>();

	@SuppressWarnings("unchecked")
	public AlgorithmSelection(PluginContext context) {
		DefaultListModel<Class<? extends OnlineMiningAlgorithm>> listModel = new DefaultListModel<Class<? extends OnlineMiningAlgorithm>>();
		for (Class<?> algorithm : context.getPluginManager().getKnownClassesAnnotatedWith(OnlineAlgorithm.class)) {
			listModel.addElement((Class<? extends OnlineMiningAlgorithm>) algorithm);
		}
		listOnlineAlgorithms.setModel(listModel);
		listOnlineAlgorithms.setCellRenderer(new ListCellRenderer<Class<? extends OnlineMiningAlgorithm>>() {
			@Override
			public Component getListCellRendererComponent(
					JList<? extends Class<? extends OnlineMiningAlgorithm>> list, Class<? extends OnlineMiningAlgorithm> value, int index,
					boolean isSelected, boolean cellHasFocus) {
				JLabel l = new JLabel(value.getAnnotation(OnlineAlgorithm.class).name());
				l.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
				if (isSelected) {
					l.setForeground(Color.white);
					l.setBackground(new Color(0, 140, 0));
					l.setOpaque(true);
				} else {
					l.setForeground(Color.darkGray);
					l.setOpaque(false);
				}
				return l;
			}
		});
		listOnlineAlgorithms.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listOnlineAlgorithms.setBackground(Color.lightGray);
		listOnlineAlgorithms.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));
		listOnlineAlgorithms.setSelectedIndex(0);
		
		setLayout(new BorderLayout());
		add(listOnlineAlgorithms, BorderLayout.CENTER);
	}
	
	public Class<? extends OnlineMiningAlgorithm> getSelectedAlgorithm() {
		return listOnlineAlgorithms.getSelectedValue();
	}
}
