package org.processmining.stream.utils;

/**
 * This class contains the basic configurations of the plugins
 * 
 * @author Andrea Burattin
 */
public class BasicPluginConfiguration {

	// plugin information
	public static final String AUTHOR = "A. Burattin";
	public static final String EMAIL = "burattin" + (char) 0x40 + "math.unipd.it";
	public static final String AFFILIATION = "Università degli Studi di Padova";
	
	// default network configuration
	public static int DEFAULT_NETWORK_PORT = 1234;
	
	// artificial start and end events
	public static final String ARTIFICIAL_START_NAME = "ARTIFICIAL_START";
	public static final String ARTIFICIAL_END_NAME = "ARTIFICIAL_END";
}
