package org.processmining.stream.utils.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import nl.tue.astar.AStarException;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryBufferedImpl;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.Pair;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.astar.petrinet.PetrinetReplayerWithoutILP;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayAlgorithm;
import org.processmining.plugins.petrinet.replayer.algorithms.costbasedcomplete.CostBasedCompleteParam;
import org.processmining.plugins.petrinet.replayresult.PNRepResult;
import org.processmining.plugins.replayer.replayresult.SyncReplayResult;
import org.processmining.stream.utils.BasicPluginConfiguration;
import org.processmining.stream.utils.Utils;

/**
 * Class with only static methods to simplify the computation of the fitness
 * 
 * @author jbuijs
 * @author Andrea Burattin
 */
public class FitnessHelper {
	
	public static XFactory xesFactory = new XFactoryBufferedImpl();
	
	/**
	 * Method to inject artificial start and end events into a given trace
	 * 
	 * @param t the trace that needs to be injected with the 
	 */
	public static void injectArtificialStartEnd(XTrace t) {
		// start
		XEvent artificialStart = xesFactory.createEvent();
		XAttributeTimestamp startTime = (XAttributeTimestamp) t.get(0).getAttributes().get("time:timestamp");
		artificialStart.getAttributes().put("concept:name",
				xesFactory.createAttributeLiteral("concept:name", BasicPluginConfiguration.ARTIFICIAL_START_NAME, null));
		artificialStart.getAttributes().put("time:timestamp",
				xesFactory.createAttributeTimestamp("time:timestamp", startTime.getValueMillis() - 1000, null));
		artificialStart.getAttributes().put("lifecycle:transition",
				xesFactory.createAttributeLiteral("lifecycle:transition", "Task_Execution", null));
		
		// end
		XEvent artificialEnd = xesFactory.createEvent();
		XAttributeTimestamp endTime = (XAttributeTimestamp) t.get(t.size() - 1).getAttributes().get("time:timestamp");
		artificialEnd.getAttributes().put("concept:name",
				xesFactory.createAttributeLiteral("concept:name", BasicPluginConfiguration.ARTIFICIAL_END_NAME, null));
		artificialEnd.getAttributes().put("time:timestamp",
				xesFactory.createAttributeTimestamp("time:timestamp", endTime.getValueMillis() + 1000, null));
		artificialEnd.getAttributes().put("lifecycle:transition",
				xesFactory.createAttributeLiteral("lifecycle:transition", "Task_Execution", null));
		
		// injection
		t.add(0, artificialStart);
		t.add(artificialEnd);
	}
	
	/**
	 * Method to filter the traces of a log to consider only the "complete" ones
	 * (i.e. the ones which starts and finishes with a given pair of events)
	 * 
	 * @param log the first log
	 * @param startsEnds list of pairs of starting and finishing events (the
	 * first event of the pair identifies the first event, the second identifies
	 * the last)
	 * @return a new log with only the traces which starts and ends with the one
	 * of the pair of events given
	 */
	public static XLog keepOnlyTracesWithStartEnd(XLog log, ArrayList<Pair<String, String>> startsEnds) {
		
		if (startsEnds == null || startsEnds.size() == 0) {
			System.out.println("startsEnds empty or null");
			return log;
		}
		System.out.println("proceed");
		
		XLog finalLog = xesFactory.createLog((XAttributeMap) log.getAttributes().clone());
		for (XTrace t : log) {
			String firstEventName = ((XAttributeLiteral) t.get(0).getAttributes().get("concept:name")).getValue();
			String lastEventName = ((XAttributeLiteral) t.get(t.size() - 1).getAttributes().get("concept:name")).getValue();
			boolean match = false;
			for (Pair<String, String> p : startsEnds) {
				System.out.println(firstEventName);
				System.out.println(lastEventName);
				System.out.println(p);
				System.out.println("==========================");
				if (p.getFirst().equals(firstEventName) && p.getSecond().equals(lastEventName)) {
					System.out.println(".");
					match = true;
					break;
				}
			}
			
			if (match) {
				XTrace newT = (XTrace) t.clone();
				finalLog.add(newT);
			}
		}
		
		return finalLog;
	}
	
	/**
	 * Method to generate an "artificial" initial marking, by checking the
	 * places with no incoming connections
	 * 
	 * @param net the petrinet model
	 * @return an artificial initial marking
	 */
	public static Marking getInitialMarking(Petrinet net) {
		Marking initMarking = new Marking();
		for (Place place : net.getPlaces()) {
			//TODO improve by looking at the number of tokens?
			if (net.getInEdges(place).isEmpty()) {
				initMarking.add(place);
				//break;?
			}
		}
		return initMarking;
	}
	
	/**
	 * Method to generate an "artificial" final marking, by checking the
	 * places with no outgoing connections
	 * 
	 * @param net the Petri net model
	 * @return an artificial final marking
	 */
	public static Marking getFinalMarking(Petrinet net) {
		Marking finalMarking = new Marking();
		for (Place place : net.getPlaces()) {
			//TODO improve by looking at the number of tokens?
			if (net.getOutEdges(place).isEmpty()) {
				finalMarking.add(place);
			}
		}
		return finalMarking;
	}
	
	/**
	 * Method to generate a mapping (based on
	 * {@link XLogInfoImpl.NAME_CLASSIFIER}) between a Petri net and a log
	 * 
	 * @param log the given log
	 * @param dummyEventClass a dummy event class (to map unmappable events)
	 * @param net the given Petri net
	 * @return the mapping
	 */
	public static TransEvClassMapping getLogModelMapping(XLog log, XEventClass dummyEventClass, Petrinet net) {
		XLogInfo logInfo = XLogInfoFactory.createLogInfo(log, XLogInfoImpl.NAME_CLASSIFIER);
		XEventClasses eventClassesInLog = logInfo.getEventClasses(XLogInfoImpl.NAME_CLASSIFIER);
		
		TransEvClassMapping mapping = new TransEvClassMapping(XLogInfoImpl.NAME_CLASSIFIER, dummyEventClass);
		for (Transition trans : net.getTransitions()) {
			//For each transition, find the event class with the exact same name
			String eventClassName = trans.getLabel();
			if (eventClassName.contains("+")) {
				eventClassName = eventClassName.substring(0, eventClassName.lastIndexOf("+"));
			}
			XEventClass eventClass = eventClassesInLog.getByIdentity(eventClassName);
			
			if (eventClass != null) {
				mapping.put(trans, eventClass);
			}
		}
		return mapping;
	}

	/**
	 * Method that computes the fitness, using the Replayer package. This method
	 * is extracted from TreeEvaluator class.
	 * 
	 * @see org.processmining.plugins.joosbuijs.blockminer.genetic.TreeEvaluator
	 * @author jbuijs
	 * @param context the plugin context
	 * @param net the model as a Petri net
	 * @param originalLog the log
	 * @param startsEnds list of pairs of starting and finishing events (the
	 * first event of the pair identifies the first event, the second identifies
	 * the last)
	 * @return the value of the fitness
	 */
	public static Double getFitness(PluginContext context, Petrinet net, XLog log) {
		
		XLogInfo logInfo = XLogInfoFactory.createLogInfo(log, XLogInfoImpl.NAME_CLASSIFIER);
		final HashMap<XEventClass, Integer> logCosts = new HashMap<XEventClass, Integer>();
		XEventClasses eventClassesInLog = logInfo.getEventClasses(XLogInfoImpl.NAME_CLASSIFIER);
		XEventClass dummyEventClass = new XEventClass("DUMMY", -1);

		// model costs
		for (XEventClass eventClass : eventClassesInLog.getClasses()) {
//			logCosts.put(eventClass, eventClass.size());
			logCosts.put(eventClass, 1);
		}
		logCosts.put(dummyEventClass, 1);

		// We don't have an initial marking but we can create one
		Marking initMarking = getInitialMarking(net);
		Marking finalMarking = getFinalMarking(net);

		// Build a LogPetrinetConnection
		// And a mapping between the log and the Petri Net
		TransEvClassMapping mapping = getLogModelMapping(log, dummyEventClass, net);
		// and the move-on-model costs
		HashMap<Transition, Integer> modelCosts = new HashMap<Transition, Integer>();
		for (Transition trans : net.getTransitions()) {
			//For each transition, find the event class with the exact same name
			String eventClassName = trans.getLabel();
			if (eventClassName.contains("+")) {
				eventClassName = eventClassName.substring(0, eventClassName.lastIndexOf("+"));
			}
			XEventClass eventClass = eventClassesInLog.getByIdentity(eventClassName);
			
			if (logCosts.containsKey(eventClass)) {
//				modelCosts.put(trans, logCosts.get(eventClass));
				modelCosts.put(trans, 1);
			} else {
				modelCosts.put(trans, 0);
			}
		}

		CostBasedCompleteParam parametersFitnessAlg = new CostBasedCompleteParam(logCosts, modelCosts);
		parametersFitnessAlg.setFinalMarkings(new Marking[] { finalMarking });
		parametersFitnessAlg.setInitialMarking(initMarking);
		parametersFitnessAlg.setMapEvClass2Cost(logCosts); //move on log
		parametersFitnessAlg.setMapTrans2Cost(modelCosts);
		parametersFitnessAlg.setMaxNumOfStates(20000);
		parametersFitnessAlg.setCreateConn(false);
		parametersFitnessAlg.setGUIMode(false);
		
		IPNReplayAlgorithm selectedFitnessAlg = new PetrinetReplayerWithoutILP();
//		IPNReplayAlgorithm selectedFitnessAlg = new PrefixBasedPetrinetReplayer();
//		PrefixBasedPetrinetReplayer selectedFitnessAlg
		
		PNRepResult fitnessRepResult = null;
		try {
			fitnessRepResult = selectedFitnessAlg.replayLog(context, net, log, mapping, parametersFitnessAlg);
		} catch (AStarException e) {
			e.printStackTrace();
		}
		// PNRepResult replayResult = replay.replayLog(context, net, log, mapping, parametersFitnessAlg, parametersFitnessAlg);
		
		List<String> keyListFitness = new ArrayList<String>();
//		keyListFitness.add(PNRepResult.FITNESS); // Prefix Fitness
		keyListFitness.add(PNRepResult.TRACEFITNESS); // Trace Fitness
//		keyListFitness.add(PNRepResult.BEHAVIORAPPROPRIATENESS); // Behavioral Appropriateness
//		keyListFitness.add(PNRepResult.MOVELOGFITNESS); // Move-Log Fitness
//		keyListFitness.add(PNRepResult.MOVEMODELFITNESS); // Move-Model Fitness
//		keyListFitness.add(PNRepResult.RAWFITNESSCOST); // Raw Fitness Cost
//		keyListFitness.add(PNRepResult.NUMSTATEGENERATED); // Num. States
//		keyListFitness.add(PNRepResult.COMPLETEFITNESS); // Fitness
		
		return getFitnessFromReplayResult(fitnessRepResult, keyListFitness);
	}

	/**
	 * Extracts the fitness value from the replayResult object returned by the
	 * Replayer plugin
	 * 
	 * @author jbuijs
	 * @param replayResult
	 * @param keys
	 * @return
	 */
	private static double getFitnessFromReplayResult(PNRepResult replayResult, List<String> keys) {
		double returnValue = 0;

		if (replayResult != null) {
			List<Double> interimValues = new ArrayList<Double>();
			for (String key : keys) {
				interimValues.add(getFitnessFromReplayResult(replayResult, key));
			}
			returnValue = Utils.harmonicMean(interimValues);
		} else {
			//There was an error during replay which might indicate something bad for the PN, just return a low fitness value...
			//FIXME what could we do here? returning a 'random' fitness value seems bad
			returnValue = 0.25d;
		}

		//make sure the return value is positive (we encountered a negative behavioral appropriateness value)
		if (returnValue < 0) {
			//FIXME What should the new value be??? Small I guess
			returnValue = 0.25d;
		}

		return returnValue;
	}

	/**
	 * Helper function for getting the fitness value
	 * 
	 * @author jbuijs
	 * @param replayResult
	 * @param key
	 * @return
	 */
	private static double getFitnessFromReplayResult(PNRepResult replayResult, String key) {
		double totalFitness = 0.0000;
		int numCases = 0;
		for (SyncReplayResult repRes : replayResult) {
			int size = repRes.getTraceIndex().size();
			if (repRes.getInfo().containsKey(key)) {
				totalFitness += (size * repRes.getInfo().get(key));
				numCases += size;
			}
		}
		//We can now set the replay fitness value
		return totalFitness / numCases;
	}
	
	/**
	 * 
	 * @param originalLog
	 * @param logStartsEnd
	 * @return
	 */
	public static XLog traceSet2Log(ArrayBlockingQueue<XTrace> originalLog, Collection<Pair<String, String>> logStartsEnd) {
		
		HashMap<String, XTrace> tempLog = new HashMap<String, XTrace>();
		for (XTrace t : originalLog) {
			String caseId = XConceptExtension.instance().extractName(t);
			if (tempLog.keySet().contains(caseId)) {
				tempLog.get(caseId).add(t.get(0));
			} else {
				tempLog.put(caseId, (XTrace) t.clone());
			}
		}
		
		XLog log = XLogHelper.generateNewXLog("test");
		
		for (String s : tempLog.keySet()) {
			XTrace t = tempLog.get(s);
			// filter if starting and finishing activities are matched
			if (logStartsEnd == null || logStartsEnd.size() == 0) {
				FitnessHelper.injectArtificialStartEnd(t);
				log.add(t);
			} else {
				String firstEventName = XConceptExtension.instance().extractName(t.get(0));
				String lastEventName = XConceptExtension.instance().extractName(t.get(t.size() - 1));
				boolean match = false;
				for (Pair<String, String> p : logStartsEnd) {
					if (p.getFirst().equals(firstEventName) && p.getSecond().equals(lastEventName)) {
						match = true;
						break;
					}
				}
				if (match) {
					FitnessHelper.injectArtificialStartEnd(t);
					log.add(t);
				}
			}
		}
		
		return log;
	}
}
