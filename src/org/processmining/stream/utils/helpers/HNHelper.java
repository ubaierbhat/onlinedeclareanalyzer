package org.processmining.stream.utils.helpers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.models.heuristics.HeuristicsNet;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.HeuristicsMiner;
import org.processmining.plugins.heuristicsnet.miner.heuristics.miner.settings.HeuristicsMinerSettings;

public class HNHelper {

	public static HeuristicsNet getNet(PluginContext context, String logFile, Double dependencyThreshold) throws Exception {
		
		ByteArrayOutputStream pipeOut = new ByteArrayOutputStream();
		PrintStream old_out = System.out;
		System.setOut(new PrintStream(pipeOut));
		
		HeuristicsMinerSettings configuration = new HeuristicsMinerSettings();
		configuration.setDependencyThreshold(dependencyThreshold);
		configuration.setUseAllConnectedHeuristics(true);
		
		XesXmlParser parser = new XesXmlParser();
		XLog log = parser.parse(new File(logFile)).get(0);
		
		HeuristicsMiner hm = new HeuristicsMiner(context, log, configuration);
		HeuristicsNet net = hm.mine();
		
		System.setOut(old_out);
		
		return net;
	}
}
