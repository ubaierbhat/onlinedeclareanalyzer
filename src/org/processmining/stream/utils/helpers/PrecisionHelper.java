package org.processmining.stream.utils.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.util.ArrayUtils;
import org.processmining.models.connections.petrinets.EvClassLogPetrinetConnection;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.plugins.alignetc.AlignETCPlugin;
import org.processmining.plugins.alignetc.result.AlignETCResult;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;

import uk.ac.shef.wit.simmetrics.similaritymetrics.AbstractStringMetric;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;

/**
 * Class with static methods useful to simplify the computation of precision
 * metrics
 * 
 * @author Andrea Burattin
 */
public class PrecisionHelper {
	
	private static final String invisibleTransitionRegEx = "[a-z][0-9]+|(tr[0-9]+)|(silent)|(tau)|(skip)|(invi)";
	private static final Pattern pattern = Pattern.compile(invisibleTransitionRegEx);
	
	public final static XEventClass DUMMY = new XEventClass("DUMMY", -1) {
		public boolean equals(Object o) {
			return this == o;
		}
	};

	/**
	 *
	 * @param context the current plugin context
	 * @param net the process model
	 * @param log the log
	 * @return the precision value
	 */
	public static Double getPrecision(PluginContext context, Petrinet net, XLog log) {
		AlignETCResult result = null;
		try {
			EvClassLogPetrinetConnection con = getConnection(log, net);

			AlignETCPlugin etcPlugin = new AlignETCPlugin();
			result = etcPlugin.checkAlignETCSilent(context,
					log, net,
					FitnessHelper.getInitialMarking(net), // generate the initial marking
					FitnessHelper.getFinalMarking(net), // generate the final marking
					con, null, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.ap;
	}
	
	/**
	 * 
	 * @param log
	 * @param net
	 * @return
	 */
	private static EvClassLogPetrinetConnection getConnection(XLog log, Petrinet net) {
		TransEvClassMapping map = new TransEvClassMapping(XLogInfoImpl.STANDARD_CLASSIFIER, DUMMY);
		Object[] boxOptions = extractEventClasses(log, XLogInfoImpl.STANDARD_CLASSIFIER);
		List<Transition> listTrans = new ArrayList<Transition>(net.getTransitions());
		Collections.sort(listTrans, new Comparator<Transition>() {
			public int compare(Transition o1, Transition o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
		});
		for (Transition transition : listTrans) {
			if (transition.isInvisible()) {
				map.put(transition, DUMMY);
			} else {
				map.put(transition, (XEventClass) boxOptions[preSelectOption(transition.getLabel().toLowerCase(), boxOptions, pattern)]);
			}
		}
		
		EvClassLogPetrinetConnection con = new EvClassLogPetrinetConnection("Connection between " + net.getLabel() + " and "
				+ XConceptExtension.instance().extractName(log), net, log, XLogInfoImpl.STANDARD_CLASSIFIER, map);
		return con;
	}

	/**
	 * get all available event classes using the selected classifier, add with
	 * NONE
	 * 
	 * @author Arya Adriansyah
	 * @param log
	 * @param selectedItem
	 * @return
	 */
	private static Object[] extractEventClasses(XLog log, XEventClassifier selectedItem) {
		XLogInfo summary = XLogInfoFactory.createLogInfo(log, XLogInfoImpl.STANDARD_CLASSIFIER);
		XEventClasses eventClasses = summary.getEventClasses();

		// sort event class
		Collection<XEventClass> classes = eventClasses.getClasses();

		// create possible event classes
		Object[] arrEvClass = classes.toArray();
		Arrays.sort(arrEvClass);
		Object[] notMappedAct = { DUMMY };
		Object[] boxOptions = ArrayUtils.concatAll(notMappedAct, arrEvClass);

		return boxOptions;
	}

	/**
	 * Returns the Event Option Box index of the most similar event for the
	 * transition.
	 * 
	 * @author Arya Adriansyah
	 * @param transition
	 *            Name of the transitions, assuming low cases
	 * @param events
	 *            Array with the options for this transition
	 * @return Index of option more similar to the transition
	 */
	private static int preSelectOption(String transition, Object[] events, Pattern pattern) {
		// try to find precise match
		for (int i = 1; i < events.length; i++) {
			String event = ((XEventClass) events[i]).toString().toLowerCase();
			if (event.equalsIgnoreCase(transition)){
				return i;
			};
		}
		
		Matcher matcher = pattern.matcher(transition);
		if(matcher.find() && matcher.start()==0){
			return 0;
		}
		
		//The metric to get the similarity between strings
		AbstractStringMetric metric = new Levenshtein();

		int index = 0;
		float simOld = Float.MIN_VALUE;
		for (int i = 1; i < events.length; i++) {
			String event = ((XEventClass) events[i]).toString().toLowerCase();
			float sim = metric.getSimilarity(transition, event);

			if (simOld < sim) {
				simOld = sim;
				index = i;
			}
		}

		return index;
	}
}
