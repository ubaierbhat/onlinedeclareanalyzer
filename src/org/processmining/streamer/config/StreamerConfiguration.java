package org.processmining.streamer.config;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.processmining.stream.utils.GUIUtils;

import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * Panel for the configuration of the conversion of a "static" log file into a
 * stream source.
 * 
 * @author Andrea Burattin
 */
public class StreamerConfiguration extends JPanel {

	private static final long serialVersionUID = 1887757682375199378L;
	private JCheckBox randomizeStartTime;
	private JCheckBox tagBeginEndTrace;

	/**
	 * Basic class constructor
	 */
	public StreamerConfiguration() {
		initComponents();
	}
	
	/**
	 * Method to know if the user decided to have randomized times
	 * 
	 * @return the user decision
	 */
	public boolean getRandomizeStartTime() {
		return randomizeStartTime.isSelected();
	}
	
	/**
	 * Method to know if the user decided to tag the beginning and the end of
	 * traces
	 * 
	 * @return the user decision
	 */
	public boolean getTaggingBeginningEnding() {
		return tagBeginEndTrace.isSelected();
	}
	
	/*
	 * Graphical components initializer
	 */
	private void initComponents() {
		randomizeStartTime = SlickerFactory.instance().createCheckBox("Randomize trace beginning", false);
		tagBeginEndTrace = SlickerFactory.instance().createCheckBox("Tag beginning and end of traces (with a new attribute)", false);
		
		setOpaque(false);
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0; c.gridy = 0;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 15, 0);
		add(GUIUtils.prepareLabel("Use this form for the configuration of " +
				"the conversion of the log into a stream source."), c);

		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		add(randomizeStartTime, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 2;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		add(tagBeginEndTrace, c);
	}

}
