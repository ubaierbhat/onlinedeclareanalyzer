package org.processmining.streamer.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import java.util.Date;

import javax.swing.JPanel;

import org.deckfour.xes.info.XLogInfo;
import org.processmining.stream.utils.UIColors;
import org.processmining.stream.utils.Utils;

/**
 * This class contains the widget the is used to display a representation of the
 * two logs as rectangles. Each rectangle can be moved to the left or to the
 * right, in order to set the overlapping between the two logs
 * 
 * @author Andrea Burattin
 */
public class LogTimeSpanVisualizer extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = -3216119046311300305L;
	
	// general widget configuration
	private static Color backgroundColor = new Color(149, 149, 149);
	private static Color selectedTimeArea = new Color(0, 0, 200, 100);
	private int rectangleHeight = 25;
	private int distanceFromCenter = 20;
	private int arrowWidth = 10;
	private int horizontalPaddingScroller = 1;
	
	// internal fields
	private XLogInfo log1;
	private XLogInfo log2;
	
	private int offsetLog1 = -1;
	private int offsetLog2 = -1;
	private double pixelToMillisec = 0;
	
	private int widthLog1;
	private int widthLog2;
	private long durationLog1;
	private long durationLog2;
	private long totalDuration;
	private Date firstEvent;
	
	private FontMetrics messageFontMetric = null;
	protected SoftReference<BufferedImage> buffer = null;
	
	private boolean draggingLog1 = false;
	private boolean draggingLog2 = false;
	private int draggingX = 0;
	
	private int timeStringWidth = -1;
	private int widthAvailable = -1;
	private int scrollingAreaLeftMargin = -1;
	private int scrollingAreaRightMargin = -1;
	
	/**
	 * Widget constructor
	 * 
	 * @param log1 the first log
	 * @param log2 the second log
	 */
	public LogTimeSpanVisualizer(XLogInfo log1, XLogInfo log2) {
		this.log1 = log1;
		this.log2 = log2;
		
		addMouseMotionListener(this);
		addMouseListener(this);
		
		initData();
	}
	
	/**
	 * This method returns the offset to be summed to all the events of the
	 * first log. The value can be positive or negative
	 * 
	 * @return the time offset for the first log
	 */
	public long getTimeOffsetLog1() {
		long newBeginning = (long) (firstEvent.getTime() + (pixelToMillisec * (offsetLog1 - scrollingAreaLeftMargin)));
		return newBeginning - log1.getLogTimeBoundaries().getStartDate().getTime();
	}
	
	/**
	 * This method returns the offset to be summed to all the events of the
	 * second log. The value can be positive or negative
	 * 
	 * @return the time offset for the second log
	 */
	public long getTimeOffsetLog2() {
		long newBeginning = (long) (firstEvent.getTime() + (pixelToMillisec * (offsetLog2 - scrollingAreaLeftMargin)));
		return newBeginning - log2.getLogTimeBoundaries().getStartDate().getTime();
	}
	
	/*
	 * Logic data initializer
	 */
	private void initData() {
		durationLog1 = log1.getLogTimeBoundaries().getEndDate().getTime() - log1.getLogTimeBoundaries().getStartDate().getTime();
		durationLog2 = log2.getLogTimeBoundaries().getEndDate().getTime() - log2.getLogTimeBoundaries().getStartDate().getTime();
		totalDuration = durationLog1 + durationLog2;
		firstEvent = new Date(Math.min(log1.getLogTimeBoundaries().getStartDate().getTime(), log2.getLogTimeBoundaries().getStartDate().getTime()));
	}
	
	/*
	 * Method to check if the pointer is within the first log
	 */
	private boolean inLog1Area(int x, int y) {
		return Utils.inRectangle(new Point(x, y), new Rectangle(offsetLog1, (this.getHeight() / 2) - distanceFromCenter - rectangleHeight, widthLog1, rectangleHeight));
	}
	
	/*
	 * Method to check if the pointer is within the second log
	 */
	private boolean inLog2Area(int x, int y) {
		return Utils.inRectangle(new Point(x, y), new Rectangle(offsetLog2, (this.getHeight() / 2) + distanceFromCenter, widthLog2, rectangleHeight));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		int height = this.getHeight();
		int width = this.getWidth();
		
		// create new back buffer
		buffer = new SoftReference<BufferedImage>(new BufferedImage(width, height, BufferedImage.TRANSLUCENT));
		Graphics2D g2d = buffer.get().createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		if (messageFontMetric == null) {
			messageFontMetric = g2d.getFontMetrics(g2d.getFont());
		}

		if (timeStringWidth == -1) {
			timeStringWidth = messageFontMetric.stringWidth("Time");
		}

		// get the widths of the two rectangles
		if (widthAvailable == -1) {
			scrollingAreaLeftMargin = timeStringWidth + 5 + horizontalPaddingScroller;
			scrollingAreaRightMargin = arrowWidth + 5 + horizontalPaddingScroller;
			
			widthAvailable = width - scrollingAreaLeftMargin - scrollingAreaRightMargin;
			
			pixelToMillisec = (double)totalDuration / (double)widthAvailable;
			widthLog1 = (int) Math.ceil(((double)durationLog1 / (double)totalDuration) * widthAvailable);
			widthLog2 = (int) Math.floor(((double)durationLog2 / (double)totalDuration) * widthAvailable);
		}
		
		// background
		g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, width, height);
		
		// add basic text
		g2d.setColor(UIColors.darkGray);
		g2d.drawString("Move the two rectangles to define the time overlapping between the two processes:",
				0, (height/2) - distanceFromCenter - rectangleHeight - 20);
		
		// paint the time line in the middle and the arrow
		g2d.setColor(Color.WHITE);
		g2d.drawString("Time", 0, (height / 2) + 3);
//		g2d.drawLine(timeStringWidth + 5, height/2, width, height/2);
		g2d.fillRect(timeStringWidth + 6, (height/2) - 3, widthAvailable + 6, 7);
		Polygon p = new Polygon();
		p.addPoint(width-arrowWidth, (height/2) - 5);
		p.addPoint(width-arrowWidth, (height/2) + 6);
		p.addPoint(width, height/2);
		g2d.fillPolygon(p);
		
		if (offsetLog1 == -1) {
			offsetLog1 = scrollingAreaLeftMargin;
		}
		
		if (offsetLog2 == -1) {
			offsetLog2 = scrollingAreaLeftMargin;
		}
		
		// paint the two logs
		// ---------------------------------------------------------------------
		// log 1 ---------------------------------------------------------------
		// background of the scrollbar and vertical lines
		g2d.setColor(UIColors.lightLightGray);
		g2d.fillRoundRect(timeStringWidth + 5, (height / 2) - distanceFromCenter - rectangleHeight - 1, widthAvailable + (horizontalPaddingScroller*2), rectangleHeight + 2, 8, 8);
		g2d.drawLine(offsetLog1, (height/2) - distanceFromCenter, offsetLog1, (height / 2) + 3);
		g2d.drawLine(offsetLog1 + widthLog1, (height/2) - distanceFromCenter, offsetLog1 + widthLog1, (height / 2) + 3);
		// the blue area on the time line
		g2d.setColor(selectedTimeArea);
		g2d.fillRect(offsetLog1 + 1, (height/2) - 3, widthLog1 - 1, 7);
		// the actual log bar
		g2d.setColor(UIColors.gray);
		g2d.fillRoundRect(offsetLog1, (height / 2) - distanceFromCenter - rectangleHeight, widthLog1, rectangleHeight, 8, 8);
		// text over the bar
		g2d.setColor(UIColors.lightLightGray);
		g2d.drawString(log1.getLog().getAttributes().get("concept:name").toString(),
				offsetLog1 + 5,
				(height / 2) - distanceFromCenter - (rectangleHeight / 2) + 5);

		// log 2 ---------------------------------------------------------------
		// background of the scrollbar and vertical lines
		g2d.setColor(UIColors.lightLightGray);
		g2d.fillRoundRect(timeStringWidth + 5, (height / 2) + distanceFromCenter - 1, widthAvailable + (horizontalPaddingScroller*2), rectangleHeight + 2, 8, 8);
		g2d.drawLine(offsetLog2, (height/2) + distanceFromCenter, offsetLog2, (height / 2) - 3);
		g2d.drawLine(offsetLog2 + widthLog2, (height/2) + distanceFromCenter, offsetLog2 + widthLog2, (height / 2) - 3);
		// the blue area on the time line
		g2d.setColor(selectedTimeArea);
		g2d.fillRect(offsetLog2 + 1, (height/2) - 3, widthLog2 - 1, 7);
		// the actual log bar
		g2d.setColor(UIColors.gray);
		g2d.fillRoundRect(offsetLog2, (height / 2) + distanceFromCenter, widthLog2, rectangleHeight, 8, 8);
		// text over the bar
		g2d.setColor(UIColors.lightLightGray);
		g2d.drawString(log2.getLog().getAttributes().get("concept:name").toString(),
				offsetLog2 + 5,
				(height / 2) + distanceFromCenter + (rectangleHeight / 2) + 5);
		
		// final paint stuff
		g2d.dispose();
		Rectangle clip = g.getClipBounds();
		g.drawImage(buffer.get(), clip.x, clip.y, clip.x + clip.width, clip.y + clip.height,
				clip.x, clip.y, clip.x + clip.width, clip.y + clip.height, null);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if (inLog1Area(x, y)) {
			draggingLog1 = true;
			draggingX = x;
		} else if (inLog2Area(x, y)) {
			draggingLog2 = true;
			draggingX = x;
		}
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX();
		if (x < 0) x = 0;
		if (x > getWidth()) x = getWidth();
		
		if (draggingLog1) {
			offsetLog1 += (x - draggingX);
		} else if (draggingLog2) {
			offsetLog2 += (x - draggingX);
		}
		draggingX = x;
		
		if (offsetLog1 < scrollingAreaLeftMargin) offsetLog1 = scrollingAreaLeftMargin;
		if (offsetLog1 + widthLog1 > getWidth() - scrollingAreaRightMargin) offsetLog1 = getWidth() - widthLog1 - scrollingAreaRightMargin;
		if (offsetLog2 < scrollingAreaLeftMargin) offsetLog2 = scrollingAreaLeftMargin;
		if (offsetLog2 + widthLog2 > getWidth() - scrollingAreaRightMargin) offsetLog2 = getWidth() - widthLog2 - scrollingAreaRightMargin;
		
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		draggingLog1 = false;
		draggingLog2 = false;
	}

}
