package org.processmining.streamer.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import javax.swing.JPanel;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.processmining.framework.util.Pair;
import org.processmining.streamer.plugins.LogStream;

/**
 * This class is a widget for the representation of a stream of a log.
 * 
 * @author Andrea Burattin
 */
public class StreamRepresentation extends JPanel {
	
	public enum COLOR_POLICY {
		PER_EVENT,
		PER_CASE,
		NONE
	};

	private static final long serialVersionUID = -3304385807203690954L;
	private static Color spotColor = new Color(79, 108, 255, 200);
	private static Color spotColorPlayed = new Color(43, 127, 42, 200);
	private static Color topBottomBarColor = new Color(0, 0, 0, 180);
	private static int spotSize = 4;
	private static int internalMargin = 3;
	private static int verticalLines = 5;
	
	private static String streamingString = "Live streaming...";
	private static Color messageBackground = new Color(0, 0, 0, 150);
	private static Color messageForeground = new Color(255, 0, 0, 180);
	private static int messagePadding = 5;
	private Font defaultFont = null;
	private Font messageFont = null;
	private FontMetrics messageFontMetric = null;
	private int streamingStringWidth;
	private int messageHeight;
	private int messageDescent;
	private HashMap<String, Color> eventTypesToColor = new HashMap<String, Color>();
	private COLOR_POLICY colorPolicy = COLOR_POLICY.NONE;
	private static Random randomGenerator = new Random();
	private static ArrayList<Color> firstSpotColors = new ArrayList<Color>();
	static{
		firstSpotColors.add(Color.CYAN);
		firstSpotColors.add(Color.YELLOW);
		firstSpotColors.add(Color.GRAY);
		firstSpotColors.add(Color.MAGENTA);
		firstSpotColors.add(Color.ORANGE);
		firstSpotColors.add(Color.PINK);
		firstSpotColors.add(Color.RED);
		firstSpotColors.add(Color.LIGHT_GRAY);
		firstSpotColors.add(Color.WHITE);
		firstSpotColors.add(Color.GREEN);
		firstSpotColors.add(Color.BLUE);
	}
	
	protected SoftReference<BufferedImage> buffer = null;
	private LogStream stream = null;
	private HashSet<XTrace> playedTraces = null;
	private boolean streaming = false;
	private String message;

	/**
	 * Widget constructor
	 * 
	 * @param stream the stream to represent
	 */
	public StreamRepresentation(LogStream stream) {
		this.stream = stream;
		playedTraces = new HashSet<XTrace>();
	}
	
	/**
	 * Method to tell the widget if the streaming is ongoing
	 * 
	 * @param streaming true if the stream is ongoing, false otherwise
	 */
	public void setStreaming(boolean streaming) {
		this.streaming = streaming;
		repaint();
		invalidate();
	}
	
	/**
	 * Method to reset the status of the widget (for example, considering the
	 * instances already sent or the user messages)
	 */
	public void resetPlayed() {
		playedTraces = new HashSet<XTrace>();
		resetMessage();
	}
	
	/**
	 * Method to inform the widget that a particular trace has already been
	 * streamed (so the widget can represent it differently)
	 * 
	 * @param trace the trace already sent
	 */
	public void addPlayed(XTrace trace) {
		if (playedTraces != null) {
			playedTraces.add(trace);
		}
		repaint();
		invalidate();
	}
	
	/**
	 * Method to set a custom message into the widget. This message will be
	 * shown only if the streaming is not ongoing
	 * 
	 * @param message the message to display
	 */
	public void setMessage(String message) {
		this.message = message;
		repaint();
		invalidate();
	}
	
	/**
	 * Method to set if the color, for the events not yet played is supposed to
	 * be the same, or one color for each different event type
	 * 
	 * @param colorPolicy
	 */
	public void setColorPolicy(COLOR_POLICY colorPolicy) {
		this.colorPolicy = colorPolicy;
		repaint();
		invalidate();
	}
	
	/**
	 * Method to reset the custom message
	 */
	public void resetMessage() {
		this.message = null;
		repaint();
		invalidate();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		int height = this.getHeight();
		int width = this.getWidth();
		
		// create new back buffer
		buffer = new SoftReference<BufferedImage>(new BufferedImage(width, height, BufferedImage.TRANSLUCENT));
		Graphics2D g2d = buffer.get().createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		// setting up the "one-time font stuff"
		if (defaultFont == null) {
			defaultFont = g2d.getFont();
			messageFont = defaultFont.deriveFont(Font.BOLD, 30);
			messageFontMetric = g2d.getFontMetrics(messageFont);
			streamingStringWidth = messageFontMetric.stringWidth(streamingString);
			messageHeight = messageFontMetric.getHeight();
			messageDescent = messageFontMetric.getDescent();
		}
		
		// background
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, width, height);

		// vertical lines
		g2d.setColor(Color.DARK_GRAY);
		int spaceBetweenLines = width / (verticalLines+1);
		for (int i = 1; i <= verticalLines; i++) {
			g2d.drawLine(spaceBetweenLines*i, internalMargin, spaceBetweenLines*i, height-internalMargin);
		}
		
		// calculate the number of spot per pixel
		long first = stream.getFirstExecutionTime().getTime();
		long last = stream.getLastExecutionTime().getTime();
		double millisecRatio = (last - first) / (width - (internalMargin*2));
		
		/* Here we have two different approaches, if we want to color each spot
		 * with a different color we need to keep a list with the different
		 * event names, otherwise we can use a simpler list of sent and unsent
		 * spot.
		 */
		switch (colorPolicy) {

			case PER_EVENT: {
				
				/* Color based on the event name
				 * ============================================================
				 * 
				 * Hash map structure
				 *    - key: the pixel
				 *    - pair.first: number of pixels played
				 *    - pair.second: array list of events name not yet played 
				 */
				HashMap<Integer, Pair<Integer, ArrayList<String>>> spotDistribution = new HashMap<Integer, Pair<Integer, ArrayList<String>>>();
				for (XTrace trace : stream) {
					XEvent e = trace.get(0);
					
					long relativeMillisec = ((XAttributeTimestampImpl)e.getAttributes().get("time:timestamp")).getValueMillis() - first;
					Integer x = (int) (relativeMillisec / millisecRatio) + internalMargin;
					
					if (playedTraces.contains(trace)) {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, ArrayList<String>> p = new Pair<Integer, ArrayList<String>>(spotDistribution.get(x).getFirst() + 1, spotDistribution.get(x).getSecond());
							spotDistribution.put(x, p);
						} else {
							spotDistribution.put(x, new Pair<Integer, ArrayList<String>>(1, new ArrayList<String>()));
						}
					} else {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, ArrayList<String>> p = new Pair<Integer, ArrayList<String>>(spotDistribution.get(x).getFirst(), spotDistribution.get(x).getSecond());
							p.getSecond().add(e.getAttributes().get("concept:name").toString());
							spotDistribution.put(x, p);
						} else {
							ArrayList<String> temp = new ArrayList<String>();
							temp.add(e.getAttributes().get("concept:name").toString());
							spotDistribution.put(x, new Pair<Integer, ArrayList<String>>(0, temp));
						}
					}
				}
				
				// draw the actual spots
				for (Integer pixel : spotDistribution.keySet()) {
					int numberOfSpotsPlayed = spotDistribution.get(pixel).getFirst();
					int numberOfSpotsNotPlayed = spotDistribution.get(pixel).getSecond().size();
					int space = (height-(internalMargin*2)) / (numberOfSpotsNotPlayed + numberOfSpotsPlayed);
					
					int topSpace = internalMargin + ((height - (space*((numberOfSpotsNotPlayed + numberOfSpotsPlayed)-1)))/2);
					
					g2d.setColor(spotColorPlayed);
					for (int i = 0; i < numberOfSpotsPlayed; i++) {
						int centerY = topSpace+i*space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel+spotSize, centerY);
						g2d.fillPolygon(p);
					}
					int i = numberOfSpotsPlayed;
					for (String eventType : spotDistribution.get(pixel).getSecond()) {
						g2d.setColor(fromEventeventTypeToColor(eventType));
						int centerY = topSpace+i*space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel+spotSize, centerY);
						g2d.fillPolygon(p);
						i++;
					}
				}
				
				break;

			} case PER_CASE : {

				/*
				 * Color based on the process instance
				 * ============================================================
				 * 
				 * Hash map structure - key: the pixel - pair.first: number of
				 * pixels played - pair.second: array list of process instance
				 * names activities not yet played
				 */
				HashMap<Integer, Pair<Integer, ArrayList<String>>> spotDistribution = new HashMap<Integer, Pair<Integer, ArrayList<String>>>();
				for (XTrace trace : stream) {
					XEvent e = trace.get(0);

					long relativeMillisec = ((XAttributeTimestampImpl) e.getAttributes().get("time:timestamp"))
							.getValueMillis() - first;
					Integer x = (int) (relativeMillisec / millisecRatio) + internalMargin;

					if (playedTraces.contains(trace)) {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, ArrayList<String>> p = new Pair<Integer, ArrayList<String>>(spotDistribution
									.get(x).getFirst() + 1, spotDistribution.get(x).getSecond());
							spotDistribution.put(x, p);
						} else {
							spotDistribution.put(x, new Pair<Integer, ArrayList<String>>(1, new ArrayList<String>()));
						}
					} else {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, ArrayList<String>> p = new Pair<Integer, ArrayList<String>>(spotDistribution
									.get(x).getFirst(), spotDistribution.get(x).getSecond());
							p.getSecond().add(trace.getAttributes().get("concept:name").toString());
							spotDistribution.put(x, p);
						} else {
							ArrayList<String> temp = new ArrayList<String>();
							temp.add(trace.getAttributes().get("concept:name").toString());
							spotDistribution.put(x, new Pair<Integer, ArrayList<String>>(0, temp));
						}
					}
				}

				// draw the actual spots
				for (Integer pixel : spotDistribution.keySet()) {
					int numberOfSpotsPlayed = spotDistribution.get(pixel).getFirst();
					int numberOfSpotsNotPlayed = spotDistribution.get(pixel).getSecond().size();
					int space = (height - (internalMargin * 2)) / (numberOfSpotsNotPlayed + numberOfSpotsPlayed);

					int topSpace = internalMargin
							+ ((height - (space * ((numberOfSpotsNotPlayed + numberOfSpotsPlayed) - 1))) / 2);

					g2d.setColor(spotColorPlayed);
					for (int i = 0; i < numberOfSpotsPlayed; i++) {
						int centerY = topSpace + i * space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel + spotSize, centerY);
						g2d.fillPolygon(p);
					}
					int i = numberOfSpotsPlayed;
					for (String eventType : spotDistribution.get(pixel).getSecond()) {
						g2d.setColor(fromEventeventTypeToColor(eventType));
						int centerY = topSpace + i * space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel + spotSize, centerY);
						g2d.fillPolygon(p);
						i++;
					}
				}

				break;

			} case NONE : {
			
				/* No specific color policy
				 * ================================================================
				 * 
				 * Hash map structure
				 *    - key: the pixel
				 *    - pair.first: number of pixel not yet played
				 *    - pair.second: number of pixel already played
				 */ 
				HashMap<Integer, Pair<Integer, Integer>> spotDistribution = new HashMap<Integer, Pair<Integer, Integer>>();
				for (XTrace trace : stream) {
					XEvent e = trace.get(0);
					
					long relativeMillisec = ((XAttributeTimestampImpl)e.getAttributes().get("time:timestamp")).getValueMillis() - first;
					Integer x = (int) (relativeMillisec / millisecRatio) + internalMargin;
					
					if (playedTraces.contains(trace)) {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, Integer> p = new Pair<Integer, Integer>(spotDistribution.get(x).getFirst(), spotDistribution.get(x).getSecond() + 1);
							spotDistribution.put(x, p);
						} else {
							spotDistribution.put(x, new Pair<Integer, Integer>(0, 1));
						}
					} else {
						if (spotDistribution.containsKey(x)) {
							Pair<Integer, Integer> p = new Pair<Integer, Integer>(spotDistribution.get(x).getFirst() + 1, spotDistribution.get(x).getSecond());
							spotDistribution.put(x, p);
						} else {
							spotDistribution.put(x, new Pair<Integer, Integer>(1, 0));
						}
					}
				}
				
				// draw the actual spots
				for (Integer pixel : spotDistribution.keySet()) {
					
					int numberOfSpotsNotPlayed = spotDistribution.get(pixel).getFirst();
					int numberOfSpotsPlayed = spotDistribution.get(pixel).getSecond();
					int space = (height-(internalMargin*2)) / (numberOfSpotsNotPlayed + numberOfSpotsPlayed);
					
					int topSpace = internalMargin + ((height - (space*((numberOfSpotsNotPlayed + numberOfSpotsPlayed)-1)))/2);
					
					g2d.setColor(spotColorPlayed);
					for (int i = 0; i < numberOfSpotsPlayed; i++) {
						int centerY = topSpace+i*space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel+spotSize, centerY);
						g2d.fillPolygon(p);
					}
					g2d.setColor(spotColor);
					for (int i = numberOfSpotsPlayed; i < (numberOfSpotsNotPlayed + numberOfSpotsPlayed); i++) {
						int centerY = topSpace+i*space;
						Polygon p = new Polygon();
						p.addPoint(pixel, centerY - (spotSize));
						p.addPoint(pixel, centerY + (spotSize));
						p.addPoint(pixel+spotSize, centerY);
						g2d.fillPolygon(p);
					}
				}
			}
		}
		
		// draw the strings
		String firstTime = stream.getFirstExecutionTime().toString();
		String lastTime = stream.getLastExecutionTime().toString();
		FontMetrics fm = g2d.getFontMetrics();
		int lastTimeWidth = fm.stringWidth(lastTime);
		
		g2d.setColor(topBottomBarColor);
		g2d.fillRect(internalMargin, internalMargin, width, fm.getHeight() + 3);
		g2d.fillRect(internalMargin, height-fm.getHeight()-3, width, fm.getHeight() + 3);
		
		g2d.setColor(Color.GRAY);
		g2d.drawString(firstTime, internalMargin, fm.getHeight());
		g2d.drawString(firstTime, internalMargin, height - fm.getDescent());
		g2d.drawString(lastTime, width - internalMargin - lastTimeWidth, fm.getHeight());
		g2d.drawString(lastTime, width - internalMargin - lastTimeWidth, height - fm.getDescent());
		
		// draw the streaming string or the message string
		if (streaming) {
			g2d.setColor(messageBackground);
			g2d.fillRoundRect(
					(width/2) - (streamingStringWidth/2) - messagePadding, (height/2) - (messageHeight/2) - messagePadding,
					streamingStringWidth + (messagePadding*2), messageHeight + (messagePadding*2), 15, 15);
			
			g2d.setColor(messageForeground);
			g2d.setFont(messageFont);
			g2d.drawString(streamingString, (width/2) - (streamingStringWidth/2), (height/2) - messageDescent + (messageHeight/2));
			g2d.setFont(defaultFont);
		} else if (message != null) {
			int messageWidth = messageFontMetric.stringWidth(message);
			
			g2d.setColor(messageBackground);
			g2d.fillRoundRect(
					(width/2) - (messageWidth/2) - messagePadding, (height/2) - (messageHeight/2) - messagePadding,
					messageWidth + (messagePadding*2), messageHeight + (messagePadding*2), 15, 15);
			
			g2d.setColor(messageForeground);
			g2d.setFont(messageFont);
			g2d.drawString(message, (width/2) - (messageWidth/2), (height/2) - messageDescent + (messageHeight/2));
			g2d.setFont(defaultFont);
		}
		
		// final paint stuff
		g2d.dispose();
		Rectangle clip = g.getClipBounds();
		g.drawImage(buffer.get(), clip.x, clip.y, clip.x + clip.width, clip.y + clip.height,
				clip.x, clip.y, clip.x + clip.width, clip.y + clip.height, null);
	}
	
	/*
	 * Method based on org.processmining.plugins.log.dottedchartanalysis.ColorDispenser
	 */
	private Color fromEventeventTypeToColor(String eventType) {
		Color c = eventTypesToColor.get(eventType);
		if (c == null) {
			if (eventTypesToColor.size() < firstSpotColors.size()) {
				c = firstSpotColors.get(eventTypesToColor.size());
			} else {
				c = new Color(randomGenerator.nextInt(255), randomGenerator.nextInt(255), randomGenerator.nextInt(255), 180);
			}
			eventTypesToColor.put(eventType, c);
		}
		return c;
	}
}
