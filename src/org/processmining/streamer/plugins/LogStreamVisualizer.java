package org.processmining.streamer.plugins;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.operationalsupport.xml.OSXMLConverter;
import org.processmining.stream.utils.BasicPluginConfiguration;
import org.processmining.stream.utils.GUIUtils;
import org.processmining.stream.utils.UIColors;
import org.processmining.stream.utils.Utils;
import org.processmining.streamer.gui.StreamRepresentation;
import org.processmining.streamer.gui.StreamRepresentation.COLOR_POLICY;
import org.processmining.streamer.utils.BroadcastService;

import com.fluxicon.slickerbox.components.RoundedPanel;
import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * This class contains the visualizer of the log streamer
 * 
 * @author Andrea Burattin
 */
public class LogStreamVisualizer extends JPanel {

	private static final long serialVersionUID = 5616414631350956372L;
	private LogStream stream;
	
	private StreamRepresentation streamerProgress;
	private JProgressBar streamerProgressBar;
	private JTextField portField;
	private int timeBetweenEvents = 100;
	private JButton play;
	private JButton stop;
	private JButton dump;
//	private MemoryUsage memoryUsed;

	@Plugin(
		name = "Log Stream Visualizer",
		parameterLabels = { "A stream log" },
		returnLabels = { "Stream visualization" },
		returnTypes = { JComponent.class },
		userAccessible = false
	)
	@UITopiaVariant(
		author = BasicPluginConfiguration.AUTHOR,
		email = BasicPluginConfiguration.EMAIL,
		affiliation = BasicPluginConfiguration.AFFILIATION
	)
	@Visualizer(name = "Log Stream Visualizer")
	public JComponent visualize(UIPluginContext context, LogStream stream) {
		this.stream = stream;
		initComponents();
		return this;
	}
	
	/*
	 * Graphical components initializer
	 */
	private void initComponents() {
		setBackground(new Color(40, 40, 40));
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		streamerProgress = new StreamRepresentation(stream);
		
		streamerProgressBar = SlickerFactory.instance().createProgressBar(SwingConstants.HORIZONTAL);
		streamerProgressBar.setMinimum(0);
		streamerProgressBar.setMaximum(stream.size());
		
		
		// stream visualizer
		// ---------------------------------------------------------------------
		RoundedPanel distributionProgressViewer = new RoundedPanel(15, 5, 3);
		distributionProgressViewer.setLayout(new BorderLayout());
		distributionProgressViewer.setBackground(UIColors.lightGray);
		distributionProgressViewer.add(GUIUtils.prepareTitle("Stream progress visualizer"), BorderLayout.NORTH);
		
		RoundedPanel streamProgressRepresentationContainer = new RoundedPanel(15, 3, 5);
		streamProgressRepresentationContainer.setLayout(new BorderLayout());
		streamProgressRepresentationContainer.setBackground(Color.BLACK);
		streamProgressRepresentationContainer.add(streamerProgress, BorderLayout.CENTER);
		
		distributionProgressViewer.add(streamProgressRepresentationContainer, BorderLayout.CENTER);
		distributionProgressViewer.add(streamerProgressBar, BorderLayout.SOUTH);
		
		
		// key log information
		// ---------------------------------------------------------------------
		RoundedPanel keyLogInformation = new RoundedPanel(15, 5, 3);
		keyLogInformation.setLayout(new BorderLayout());
		keyLogInformation.setBackground(UIColors.lightGray);
		keyLogInformation.setMinimumSize(new Dimension(250, 230));
		keyLogInformation.setPreferredSize(new Dimension(250, 230));
		keyLogInformation.add(GUIUtils.prepareTitle("Key data"), BorderLayout.NORTH);
		
		JPanel keyLogStatsPanel = new JPanel();
		keyLogStatsPanel.setLayout(new BoxLayout(keyLogStatsPanel, BoxLayout.Y_AXIS));
		keyLogStatsPanel.setOpaque(false);
		keyLogStatsPanel.add(GUIUtils.prepareLabel("First event", SwingConstants.RIGHT, UIColors.gray));
		keyLogStatsPanel.add(GUIUtils.prepareLabel(stream.getFirstExecutionTime().toString(), SwingConstants.LEFT, UIColors.darkGray));
		keyLogStatsPanel.add(Box.createVerticalStrut(10));
		keyLogStatsPanel.add(GUIUtils.prepareLabel("Last event", SwingConstants.RIGHT, UIColors.gray));
		keyLogStatsPanel.add(GUIUtils.prepareLabel(stream.getLastExecutionTime().toString(), SwingConstants.LEFT, UIColors.darkGray));
		keyLogStatsPanel.add(Box.createVerticalStrut(10));
		keyLogStatsPanel.add(GUIUtils.prepareLabel("Number of events", SwingConstants.RIGHT, UIColors.gray));
		keyLogStatsPanel.add(GUIUtils.prepareLabel(stream.size(), SwingConstants.LEFT, UIColors.darkGray));
		keyLogStatsPanel.add(Box.createVerticalStrut(10));
		keyLogStatsPanel.add(GUIUtils.prepareLabel("Minimum time distance", SwingConstants.RIGHT, UIColors.gray));
		keyLogStatsPanel.add(GUIUtils.prepareLabel(Utils.fromMillisecToStringDuration(stream.getMinTimeBetweenActivities()), SwingConstants.LEFT, UIColors.darkGray));
		keyLogStatsPanel.add(Box.createVerticalStrut(10));
		keyLogStatsPanel.add(GUIUtils.prepareLabel("Maximum time distance", SwingConstants.RIGHT, UIColors.gray));
		keyLogStatsPanel.add(GUIUtils.prepareLabel(Utils.fromMillisecToStringDuration(stream.getMaxTimeBetweenActivities()), SwingConstants.LEFT, UIColors.darkGray));
		
		keyLogInformation.add(keyLogStatsPanel, BorderLayout.CENTER);
		
		
		// dump to file
		// ---------------------------------------------------------------------
		RoundedPanel dumpToFilePanel = new RoundedPanel(15, 5, 3);
		dumpToFilePanel.setLayout(new BorderLayout());
		dumpToFilePanel.setBackground(UIColors.lightGray);
		dumpToFilePanel.setMinimumSize(new Dimension(250, 100));
		dumpToFilePanel.setPreferredSize(new Dimension(250, 100));
		dumpToFilePanel.add(GUIUtils.prepareTitle("Dump to file"), BorderLayout.NORTH);
		
		dump = SlickerFactory.instance().createButton("Dump the stream to file");
		dump.addActionListener(new DumpListener());
		
		JPanel dumpButtonContainer = new JPanel(new FlowLayout(FlowLayout.CENTER));
		dumpButtonContainer.setOpaque(false);
		dumpButtonContainer.add(dump);
		
		dumpToFilePanel.add(dumpButtonContainer, BorderLayout.CENTER);
		
		
		// stream configuration
		// ---------------------------------------------------------------------
		RoundedPanel streamConfiguration = new RoundedPanel(15, 5, 3);
		streamConfiguration.setBackground(UIColors.lightGray);
		streamConfiguration.setMinimumSize(new Dimension(250, 300));
		streamConfiguration.setPreferredSize(new Dimension(250, 300));
		
		JRadioButton eventColorsNone = SlickerFactory.instance().createRadioButton("No colors");
		JRadioButton eventColorsEvent = SlickerFactory.instance().createRadioButton("Colors per event");
		JRadioButton eventColorsProcess = SlickerFactory.instance().createRadioButton("Colors per case id");
		eventColorsNone.setSelected(true);
		
		ActionListener colorListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				streamerProgress.setColorPolicy(COLOR_POLICY.valueOf(COLOR_POLICY.class, e.getActionCommand()));
			}
		};
		
		eventColorsNone.setActionCommand("NONE");
		eventColorsNone.addActionListener(colorListener);
		eventColorsEvent.setActionCommand("PER_EVENT");
		eventColorsEvent.addActionListener(colorListener);
		eventColorsProcess.setActionCommand("PER_CASE");
		eventColorsProcess.addActionListener(colorListener);
		
		ButtonGroup eventColorsGroup = new ButtonGroup();
		eventColorsGroup.add(eventColorsNone);
		eventColorsGroup.add(eventColorsEvent);
		eventColorsGroup.add(eventColorsProcess);
		
		JPanel eventColorPanel = new JPanel();
		eventColorPanel.setLayout(new BoxLayout(eventColorPanel, BoxLayout.Y_AXIS));
		eventColorPanel.setOpaque(false);
		eventColorPanel.add(eventColorsNone);
		eventColorPanel.add(eventColorsEvent);
		eventColorPanel.add(eventColorsProcess);
		
		portField = GUIUtils.prepareIntegerField(BasicPluginConfiguration.DEFAULT_NETWORK_PORT);
		final JLabel timeFieldLabel = GUIUtils.prepareLabel(timeBetweenEvents);
		final JSlider timeField = SlickerFactory.instance().createSlider(SwingConstants.HORIZONTAL);
		timeField.setMinimum(0);
		timeField.setMaximum(500);
		timeField.setValue(timeBetweenEvents);
		timeField.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				timeFieldLabel.setText("" + timeField.getValue());
				timeBetweenEvents = timeField.getValue();
			}
		});
		JPanel timeFieldContainer = new JPanel(new BorderLayout());
		timeFieldContainer.setOpaque(false);
		timeFieldContainer.add(timeField, BorderLayout.CENTER);
		timeFieldContainer.add(timeFieldLabel, BorderLayout.EAST);
		
		play = SlickerFactory.instance().createButton("Play");
		stop = SlickerFactory.instance().createButton("Stop");
		stop.setEnabled(false);
		play.addActionListener(new PlayExecutor());
		stop.addActionListener(new StopListener());
		JPanel buttonContainer = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonContainer.setOpaque(false);
		buttonContainer.add(play);
		buttonContainer.add(stop);
		
		GridBagConstraints c = new GridBagConstraints();
		JPanel streamConfigurationContainer = new JPanel();
		streamConfigurationContainer.setOpaque(false);
		streamConfigurationContainer.setLayout(new GridBagLayout());
		
		c.insets = new Insets(0, 0, 20, 0);
		streamConfigurationContainer.add(GUIUtils.prepareLabel("Network port:"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1; c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 20, 0);
		JPanel portFieldContainer = new RoundedPanel(10);
		portFieldContainer.setBackground(Color.DARK_GRAY);
		portFieldContainer.setLayout(new BorderLayout());
		portFieldContainer.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		portFieldContainer.add(portField, BorderLayout.CENTER);
		streamConfigurationContainer.add(portFieldContainer, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 1;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		streamConfigurationContainer.add(GUIUtils.prepareLabel("Time between events:"), c);

		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 2;
		c.gridwidth = 2;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 20, 0);
		streamConfigurationContainer.add(timeFieldContainer, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 3;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 5, 0);
		streamConfigurationContainer.add(GUIUtils.prepareLabel("Stream representation colors:"), c);

		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 4;
		c.gridwidth = 2;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 20, 0);
		streamConfigurationContainer.add(eventColorPanel, c);
		
		streamConfiguration.setLayout(new BorderLayout());
		streamConfiguration.add(GUIUtils.prepareTitle("Stream configuration"), BorderLayout.NORTH);
		streamConfiguration.add(streamConfigurationContainer, BorderLayout.CENTER);
		streamConfiguration.add(buttonContainer, BorderLayout.SOUTH);
		
		// memory visualizer
		// ---------------------------------------------------------------------
//		RoundedPanel memoryUsedContainer = new RoundedPanel(15, 5, 3);
//		memoryUsedContainer.setBackground(UIColors.lightGray);
//		memoryUsedContainer.setMinimumSize(new Dimension(250, 200));
//		memoryUsedContainer.setPreferredSize(new Dimension(250, 200));
//		memoryUsedContainer.setLayout(new BorderLayout());
//		memoryUsedContainer.add(GUIUtils.prepareLabel("Memory used", SwingConstants.LEFT, UIColors.gray), BorderLayout.NORTH);
//		
//		memoryUsed = new MemoryUsage(UIColors.lightGray);
//		memoryUsedContainer.add(memoryUsed, BorderLayout.CENTER);
		
		
		// add everything to the main panel
		// ---------------------------------------------------------------------
		JPanel sideContainer = new JPanel();
		sideContainer.setOpaque(false);
		sideContainer.setLayout(new GridBagLayout());
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 0;
		sideContainer.add(streamConfiguration, c);
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.VERTICAL;
		sideContainer.add(keyLogInformation, c);
		
		
		c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 2;
		c.weighty = 0;
		c.fill = GridBagConstraints.VERTICAL;
		sideContainer.add(dumpToFilePanel, c);
		

//		c = new GridBagConstraints();
//		c.gridx = 0;
//		c.gridy = 3;
//		sideContainer.add(memoryUsedContainer, c);
		
		setLayout(new BorderLayout());
		add(sideContainer, BorderLayout.WEST);
		add(distributionProgressViewer, BorderLayout.CENTER);
	}
	
	/*
	 * Listener for the play button
	 */
	private class PlayExecutor implements ActionListener {
		
		private boolean execute = true;
		private Thread player;
		
		public void stop() {
			execute = false;
			
			try {
				player.join();
			} catch (InterruptedException e) { }
			
			streamerProgress.setStreaming(false);
			streamerProgress.resetPlayed();
			stop.setEnabled(false);
			play.setEnabled(true);
			portField.setEditable(true);
		}
		
		public void resetPlayerThread() {
			player = new Thread(new Runnable() {
				public void run() {
					streamerProgress.setStreaming(true);
					BroadcastService bs = new BroadcastService(Integer.parseInt(portField.getText()));
					OSXMLConverter converter = new OSXMLConverter();
					int sent = 0;
					
					try {
						bs.open();
						for (XTrace t : stream) {
							
							String packet = converter.toXML(t).replace('\n', ' ') + "\n";
							bs.send(packet);
							streamerProgress.addPlayed(t);
							streamerProgressBar.setValue(++sent);
							
							try {
								Thread.sleep(timeBetweenEvents);
							} catch (InterruptedException e1) { }
							
							if (!execute) {
								bs.close();
								return;
							}
						}
						bs.close();
						
					} catch (IOException e) {
						JOptionPane.showMessageDialog(LogStreamVisualizer.this, e.getLocalizedMessage(), "Network Exception", JOptionPane.ERROR_MESSAGE);
					}
					
					streamerProgress.setStreaming(false);
					stop.setEnabled(false);
					play.setEnabled(true);
					portField.setEditable(true);
				}
			});
		}

		public void actionPerformed(ActionEvent e) {
			stop.setEnabled(true);
			play.setEnabled(false);
			portField.setEditable(false);
			
			
			streamerProgress.setMessage("Preparing...");
			
			resetPlayerThread();
			
			execute = true;
			streamerProgress.resetPlayed();
			player.start();
		}
	}
	
	/*
	 * Listener for the stop button
	 */
	private class StopListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			((PlayExecutor)play.getActionListeners()[0]).stop();
			
			stop.setEnabled(false);
			play.setEnabled(true);
			portField.setEditable(true);
		}
	}
	
	/*
	 * Listener for the dump of the stream
	 */
	private class DumpListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			dump.setEnabled(false);
			
			final JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter("Stream file (.stream)", ".stream"));
			if (fc.showSaveDialog(LogStreamVisualizer.this) == JFileChooser.APPROVE_OPTION) {
				// get correct destination file name
				final String file_name = fc.getSelectedFile().getAbsolutePath();
				
				new Thread(new Runnable() {
					public void run() {
						OSXMLConverter converter = new OSXMLConverter();
						streamerProgress.setMessage("Dumping to file...");
						PrintWriter writer = null;
						try {
							writer = new PrintWriter(file_name);
							int sent = 0;
							for (XTrace t : stream) {
								String packet = converter.toXML(t).replace('\n', ' ');
								writer.println(packet);
								streamerProgressBar.setValue(++sent);
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} finally {
							if (writer != null) {
								writer.close();
							}
						}
						streamerProgress.resetMessage();
						streamerProgressBar.setValue(0);
						dump.setEnabled(true);
					}
				}).start();;
			}
		}
	}
}
