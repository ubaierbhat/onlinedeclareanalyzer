package org.processmining.streamer.plugins;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.declareminer.visualizing.DeclareMap;
import org.processmining.stream.utils.BasicPluginConfiguration;
import org.processmining.streamer.config.StreamerConfiguration;

/**
 * This class contains the plugin for the generation of a configuration for the
 * stream source
 * 
 * @author Andrea Burattin
 */
public class LogStreamer {
	
	LogStream eventQueue;

	@Plugin(
		name = "Log Streamer",
		parameterLabels = { "The log to be streamed" , "Declare Model"},
		returnLabels = { "A stream representation of the log" },
		returnTypes = { LogStream.class },
		userAccessible = true
	)
	@UITopiaVariant(
		author = BasicPluginConfiguration.AUTHOR,
		email = BasicPluginConfiguration.EMAIL,
		affiliation = BasicPluginConfiguration.AFFILIATION
	)
	public LogStream logStreamer(final UIPluginContext context, final XLog log, DeclareMap model) {
		
		// ask information to user
		StreamerConfiguration streamerConfiguration = new StreamerConfiguration();
		InteractionResult r = context.showWizard("Streamer configuration", true, true, streamerConfiguration);
		boolean randomizeStart = false;
		boolean tagBeginningEnd = false;
		
		if (r.equals(InteractionResult.FINISHED)) {
			randomizeStart = streamerConfiguration.getRandomizeStartTime();
			tagBeginningEnd = streamerConfiguration.getTaggingBeginningEnding();
		} else {
			return null;
		}
		
		// set the progress bar so to work on the number of events
		context.log("Stream configuration");
		XLogInfo info = XLogInfoFactory.createLogInfo(log);
		context.getProgress().setMinimum(0);
		context.getProgress().setMaximum(info.getNumberOfEvents());
		context.getFutureResult(0).setLabel("Stream for " + log.getAttributes().get("concept:name"));
		
		// get the temporal window in which all activities are executed (this
		// will be useful in case of randomizing the beginning of the events)
		long temporalWindow = info.getLogTimeBoundaries().getEndDate().getTime() - info.getLogTimeBoundaries().getStartDate().getTime();
		
		// new queue setup
		eventQueue = new LogStream();
		
		// iterate through all the events of the log
		context.log("Stream creation");
		for (XTrace t : log) {
			// if we need to add a random time before the trace is executed, this is the moment
			long traceTimeIncrement = 0;
			if (randomizeStart) {
				// get a random time to be summed to all the events of the current trace
				traceTimeIncrement = (long) (Math.random()*temporalWindow);
			}
			// some general trace statistics
			int traceLength = t.size();
			int eventIndex = 0;
			for(XEvent e : t) {
				XTraceImpl t1 = new XTraceImpl(t.getAttributes());
				XAttributeTimestampImpl timestamp = (XAttributeTimestampImpl) e.getAttributes().get("time:timestamp");
				((XAttributeTimestampImpl) e.getAttributes().get("time:timestamp")).setValueMillis(timestamp.getValueMillis() + traceTimeIncrement);
				
				// event tagging
				if (tagBeginningEnd) {
					if (eventIndex == 0) {
						e.getAttributes().put("stream:lifecycle:trace-transition", new XAttributeLiteralImpl("stream:lifecycle:trace-transition", "start"));
					} else if (eventIndex == traceLength -1) {
						e.getAttributes().put("stream:lifecycle:trace-transition", new XAttributeLiteralImpl("stream:lifecycle:trace-transition", "complete"));
					}
				}
				
				t1.add(e);
				
				eventQueue.add(t1);
				context.getProgress().inc();
				eventIndex++;
			}
		}
		
		// calculate statistics over the stream
		context.log("Stream finalization");
		context.getProgress().setIndeterminate(true);
		eventQueue.calculateStatistics();
		
		return eventQueue;
	}
}
