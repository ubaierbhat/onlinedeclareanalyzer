package org.processmining.utils;

/**
 * Created by Ubaier on 26/03/2016.
 */
public class EquationBuilder {

    public static String invertSigns(String s){
        String output = s;

        if(output.contains("==")){
            output = output.replace("==","!==");
        } else if (output.contains("!=")){
            output = output.replace("!=","==");
        }

        if(output.contains(">")){
            output = output.replace(">","<");
        } else if (output.contains("<")){
            output = output.replace("<",">");
        }

        return output;
    }
}
